{$mode objfpc}
unit paswl_viewporter;

interface
uses wl_basic_types,wl_core,wl_libwl, wl_viewporter,
paswl_viewport, paswl_surface, paswl_bindable;


type

Tpaswl_viewporter=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;
  
  function get_viewport(surf : Tpaswl_surface; c : Tpaswl_viewport_cls) : Tpaswl_viewport;
  function get_viewport(surf : Tpaswl_surface) : Tpaswl_viewport;
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;
Tpaswl_viewporter_cls = class of Tpaswl_viewporter;








implementation
uses baseunix;







constructor Tpaswl_viewporter.create();
begin
  inherited create();
end;

destructor Tpaswl_viewporter.destroy();
begin
  wp_viewporter_destroy(wp_viewporter(wlo));
  wl_proxy_destroy(wlo);
  inherited destroy();
end;


function Tpaswl_viewporter.get_viewport(surf : Tpaswl_surface; c : Tpaswl_viewport_cls) : Tpaswl_viewport;
var wo : wp_viewport; o : Tpaswl_viewport;
begin
  wo := wp_viewporter_get_viewport(wlo,surf.wlo);
  o := Tpaswl_viewport(make_child(c,wo));
  get_viewport := o;
end;

function Tpaswl_viewporter.get_viewport(surf : Tpaswl_surface) : Tpaswl_viewport;
begin
  get_viewport := get_viewport(surf,Tpaswl_viewport);
end;


procedure Tpaswl_viewporter.onBind();
begin
  inherited;
  // no events to register for
end;

procedure Tpaswl_viewporter.onUnbind();
begin
  inherited;
end;




class function Tpaswl_viewporter.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wp_viewporter_interface;
end;

class function Tpaswl_viewporter.wl_ifver() : dword;
begin
  wl_ifver := 1;
end;


initialization
begin
end;

finalization
begin
end;

end.



