{$mode objfpc}
unit wl_libwl;

interface
uses wl_basic_types;

procedure libwl_cleanup();
procedure wl_proxy_destroy(p : pointer);


type
Twl_proxy_marshal_array_constructor_versioned = function(proxy : pointer; opcode : dword; args : pointer; intf : pointer; version : dword) : pointer; cdecl;
Twl_proxy_marshal_array_constructor = function(proxy : pointer; opcode : dword; args : pointer; intf : pointer) : pointer; cdecl;
Twl_proxy_marshal_array = function(proxy : pointer; opcode : dword; args : pointer) : pointer; cdecl;
Twl_proxy_add_listener = function(p,i,d : pointer) : Tcint; cdecl;
Twl_proxy_set_user_data = procedure(p : wl_object; d : pointer); cdecl;
Twl_proxy_get_user_data = function(p : wl_object) : pointer; cdecl;
Twl_display_connect = function(name : Pchar) : wl_display; cdecl;
Twl_display_disconnect = procedure(d : pointer); cdecl;
Twl_display_roundtrip = function(d : pointer) : Tcint; cdecl;
Twl_display_dispatch = function(d : pointer) : Tcint; cdecl;
Twl_display_get_fd = function(d : pointer) : Tcint; cdecl;
Twl_proxy_destroy = procedure(d : pointer); cdecl;

var
wl_proxy_marshal_array_constructor_versioned : Twl_proxy_marshal_array_constructor_versioned = nil;
wl_proxy_marshal_array_constructor : Twl_proxy_marshal_array_constructor = nil;
wl_proxy_marshal_array : Twl_proxy_marshal_array = nil;
wl_proxy_add_listener : Twl_proxy_add_listener = nil;
wl_proxy_set_user_data : Twl_proxy_set_user_data = nil;
wl_proxy_get_user_data : Twl_proxy_get_user_data = nil;
wl_display_connect : Twl_display_connect = nil;
wl_display_disconnect : Twl_display_disconnect = nil;
wl_display_roundtrip : Twl_display_roundtrip = nil;
wl_display_get_fd : Twl_display_get_fd = nil;
wl_display_dispatch : Twl_display_dispatch = nil;
_Twl_proxy_destroy : Twl_proxy_destroy = nil;


libwayland_client_avail : boolean = false;

function import_libwlc() : boolean;



implementation
uses dynlibs;




const
libwlcname = 'libwayland-client.so';
libwlcname2 = 'libwayland-client.so.0';

var
libwlc : TLibHandle = NilHandle;


function import_wl_funcs() : boolean;
var p : pointer;
begin
  import_wl_funcs := false;
  if libwlc<>NilHandle then begin
    p := GetProcedureAddress(libwlc, 'wl_proxy_marshal_array_constructor_versioned');
    wl_proxy_marshal_array_constructor_versioned := Twl_proxy_marshal_array_constructor_versioned(p);
    if p=nil then exit;

    p := GetProcedureAddress(libwlc, 'wl_proxy_marshal_array_constructor');
    wl_proxy_marshal_array_constructor := Twl_proxy_marshal_array_constructor(p);
    if p=nil then exit;

    p := GetProcedureAddress(libwlc, 'wl_proxy_marshal_array');
    wl_proxy_marshal_array := Twl_proxy_marshal_array(p);
    if p=nil then exit;

    p := GetProcedureAddress(libwlc, 'wl_proxy_add_listener');
    wl_proxy_add_listener := Twl_proxy_add_listener(p);
    if p=nil then exit;

    p := GetProcedureAddress(libwlc, 'wl_proxy_set_user_data');
    wl_proxy_set_user_data := Twl_proxy_set_user_data(p);
    if p=nil then exit;

    p := GetProcedureAddress(libwlc, 'wl_proxy_get_user_data');
    wl_proxy_get_user_data := Twl_proxy_get_user_data(p);
    if p=nil then exit;

    p := GetProcedureAddress(libwlc, 'wl_display_connect');
    wl_display_connect := Twl_display_connect(p);
    if p=nil then exit;

    p := GetProcedureAddress(libwlc, 'wl_display_disconnect');
    wl_display_disconnect := Twl_display_disconnect(p);
    if p=nil then exit;

    p := GetProcedureAddress(libwlc, 'wl_display_roundtrip');
    wl_display_roundtrip := Twl_display_roundtrip(p);
    if p=nil then exit;

    p := GetProcedureAddress(libwlc, 'wl_display_dispatch');
    wl_display_dispatch := Twl_display_dispatch(p);
    if p=nil then exit;

    p := GetProcedureAddress(libwlc, 'wl_display_get_fd');
    wl_display_get_fd := Twl_display_get_fd(p);
    if p=nil then exit;

    p := GetProcedureAddress(libwlc, 'wl_proxy_destroy');
    _Twl_proxy_destroy := Twl_proxy_destroy(p);


    import_wl_funcs := true;
  end;
end;




function import_libwlc() : boolean;
begin
  libwayland_client_avail := false;
  libwlc := LoadLibrary(libwlcname);
  if (libwlc=NilHandle) then libwlc := LoadLibrary(libwlcname2);
  if (libwlc<>NilHandle) then begin
    if import_wl_funcs() then libwayland_client_avail := true;
  end;
  import_libwlc := libwayland_client_avail;
end;

procedure wl_proxy_destroy(p : pointer);
begin
  if assigned(_Twl_proxy_destroy) then _Twl_proxy_destroy(p);
end;


procedure cleanup();
begin
  if (libwlc<>NilHandle) then begin
    UnloadLibrary(libwlc);
    libwlc:=NilHandle;
  end;
  libwayland_client_avail := false;
end;

procedure libwl_cleanup();
begin
  cleanup();
end;






initialization ;

finalization cleanup();

end.

