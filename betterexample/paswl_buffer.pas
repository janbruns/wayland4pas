{$mode objfpc}
unit paswl_buffer;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_bindable,mempool;


type

T_paswl_buffer=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  
  procedure onBind(); override;
  procedure onUnbind(); override;

  function getDataPtr() : pointer;

  {the functions assume format 0!}
  function getPixel(x,y : longint) : dword;
  procedure setPixel(x,y : longint; c : dword);
  procedure premul();
  procedure fillcolor(ctl,ctr,cbl,cbr : dword);
  procedure copyFrom(src : T_paswl_buffer; srcx,srcy,dx,dy,dstx,dsty : longint);
  
  function cb_buffer_release() : boolean; virtual;

  public
  width, height : longint;
  oset, bpl,bpp : int64; // bpl : bytes per line; bpp : bytes per pixel
  format : dword;
  pool : pointer;
  lastattach : Tpaswl_bindable;
  busy,release_on_release : boolean;
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
end;
T_paswl_buffer_cls = class of T_paswl_buffer;

Tpaswl_recropablebuffer =class;
Tpaswl_virtualbuffer = class(T_paswl_buffer)
  constructor create(); override;
  destructor destroy(); override;
  function get_recropable(cx,cy,cwidth,cheight : longint) : Tpaswl_recropablebuffer;
  function do_recrop(b : Tpaswl_recropablebuffer; cx,cy,cwidth,cheight : longint) : Tpaswl_recropablebuffer;
end;
Tpaswl_buffer = class(T_paswl_buffer)
  constructor create(); override;
  destructor destroy(); override;
end;
Tpaswl_recropablebuffer = class(T_paswl_buffer)
  constructor create(); override;
  destructor destroy(); override;
  public 
  function recrop(cx,cy,cwidth,cheight : longint) : Tpaswl_recropablebuffer;
  public
  vb : Tpaswl_virtualbuffer;
end;


Tpaswl_buffer_cls = class of Tpaswl_buffer;
Tpaswl_virtualbuffer_cls = class of Tpaswl_virtualbuffer;
Tpaswl_recropablebuffer_cls = class of Tpaswl_recropablebuffer;


function colinterpol(a,b : dword; t : single) : dword;





implementation
uses baseunix, math, paswl_shm_pool;


procedure buffer_release(dat:pointer; o:wl_buffer); cdecl;
var d : T_paswl_buffer;
begin
  if dat<>nil then begin
    d := T_paswl_buffer(dat);
    if d.wlo=o then begin
      if d.cb_buffer_release() then d.destroy();
    end else ;//
  end;
end;

var
imp_if_buffer_listener : wl_buffer_listener = (
  release : @buffer_release;
);



procedure T_paswl_buffer.onBind();
begin
  inherited;
  wl_buffer_add_listener(wl_buffer(wlo),imp_if_buffer_listener,self);
end;

procedure T_paswl_buffer.onUnbind();
begin
  inherited;
end;

function T_paswl_buffer.cb_buffer_release() : boolean;
begin
//write('*bufrelease',release_on_release,classname,'*');
  busy := false;
  cb_buffer_release := release_on_release;
end;

class function T_paswl_buffer.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_buffer_interface;
end;

class function T_paswl_buffer.wl_ifver() : dword;
begin
  wl_ifver := 1;
end;

function T_paswl_buffer.getPixel(x,y : longint) : dword;
var p : Pdword;
begin
  getPixel := 0;
  if (x<0)or(x>=width)  then exit;
  if (y<0)or(y>=height) then exit;
  p := T_paswl_shm_pool(pool).get_buffer_adress(oset+y*bpl+x*bpp);
  getPixel := p^;
end;

procedure T_paswl_buffer.setPixel(x,y : longint; c : dword);
var p : Pdword;
begin
  if (x<0)or(x>=width)  then exit;
  if (y<0)or(y>=height) then exit;
  //writeln('setpixel ',x,',',y,' bpl=',bpl,' bpp=',bpp,' oset=',oset);
  //p := T_paswl_shm_pool(pool).get_buffer_adress(0);
  //writeln(hexstr(ptruint(p),16));
  p := T_paswl_shm_pool(pool).get_buffer_adress(oset+y*bpl+x*bpp);
  //writeln(hexstr(ptruint(p),16));
  p^ := c;
end;

function colinterpol(a,b : dword; t : single) : dword;
var ra,rb,rr : byte; i : longint; r : dword;
begin
  r := 0;
  for i := 3 downto 0 do begin
    ra := (a shr (8*i)) and 255;
    rb := (b shr (8*i)) and 255;
    rr := round(ra*(1-t)+rb*t);
    r := r*256 + rr;
  end;
  colinterpol := r;
end;

procedure T_paswl_buffer.fillcolor(ctl,ctr,cbl,cbr : dword);
var cl,cr : dword; yf,xf : single; x,y : longint;
begin
  for y := 0 to height-1 do begin
    if (height=1) then yf := 0.5 else yf := y/(height-1);
    cl := colinterpol(ctl,cbl,yf);
    cr := colinterpol(ctr,cbr,yf);
    for x := 0 to width-1 do begin
      if (width=1) then xf := 0.5 else xf := x/(width-1);
      setPixel(x,y,colinterpol(cl,cr,xf));
    end;
  end;
end;

procedure T_paswl_buffer.premul();
var cl,cr,k : dword; yf : single; x,y : longint;
begin
  for y := 0 to height-1 do begin
    for x := 0 to width-1 do begin
      cl := getPixel(x,y);
      cr := (cl shr 24) and 255;
      yf := cr/255;
      k := (cl shr 16) and 255;
      k := round(k * yf);
      cr := cr*256+k;
      k := (cl shr 8) and 255;
      k := round(k * yf);
      cr := cr*256+k;
      k := (cl) and 255;
      k := round(k * yf);
      cr := cr*256+k;
      setPixel(x,y,cr);
    end;
  end;
end;

procedure T_paswl_buffer.copyFrom(src : T_paswl_buffer; srcx,srcy,dx,dy,dstx,dsty : longint);
var ps,pd : Pbyte; ps2,pd2 : Pdword; x,y,sx,sy : longint;
begin
//  writeln('CopyFrom ',srcx,' ',srcy,' ',dx,' ',dy,' ',dstx,' ',dsty,' bpl_src:',src.bpl,' bpl_dst:',bpl);
{ Reference implementation:

  for y := 0 to dy-1 do for x := 0 to dx-1 do begin
    setPixel(dstx+x,dsty+y,src.getPixel(srcx+x,srcy+y));
  end; 
}
  dx := min(dx,src.width -srcx);
  dy := min(dy,src.height-srcy);
  dx := min(dx,    width -dstx);
  dy := min(dy,    height-dsty);
  sx := max(max(0,-srcx),-dstx);
  sy := max(max(0,-srcy),-dsty);
  ps := src.getDataPtr();
  pd := getDataPtr();
  dx -= 1;
  dy -= 1;
  for y := sy to dy do begin
    for x := sx to dx do begin
      ps2 := @ps[src.bpl*(y+srcy) +src.bpp*(x+srcx)];
      pd2 := @pd[    bpl*(y+dsty) +    bpp*(x+dstx)];
      pd2^ := ps2^;
    end;
  end;
end;


function T_paswl_buffer.getDataPtr() : pointer;
begin
  getDataPtr := T_paswl_shm_pool(pool).get_buffer_adress(oset);
end;



function Tpaswl_virtualbuffer.get_recropable(cx,cy,cwidth,cheight : longint) : Tpaswl_recropablebuffer;
var o : Tpaswl_recropablebuffer;
begin
  o := Tpaswl_recropablebuffer( make_child(Tpaswl_recropablebuffer,nil,true) );
  o := do_recrop(o,cx,cy,cwidth,cheight);
  get_recropable := o;
end;

function Tpaswl_virtualbuffer.do_recrop(b : Tpaswl_recropablebuffer; cx,cy,cwidth,cheight : longint) : Tpaswl_recropablebuffer;
var p : Tpaswl_shm_pool;
begin
  if b.busy then begin
    b.release_on_release := true;
    do_recrop := get_recropable(cx,cy,cwidth,cheight);
  end else begin
    if (b.wlo<>nil) then begin
      wl_buffer_destroy(b.wlo);
      wl_proxy_destroy(b.wlo);
    end;
    p := Tpaswl_shm_pool(pool);
    cwidth := max(0,min(cwidth,width-cx));
    cheight := max(0,min(cheight,height-cy));
    b.oset := oset +bpp*cx +cy*bpl;
    b.wlo := wl_shm_pool_create_buffer(p.wlo, b.oset, cwidth, cheight, bpl, format); 
    b.width := cwidth;
    b.height := cheight;
    b.format := format;
    b.bpl := bpl;
    b.bpp := bpp;
    b.pool := pool;
    b.vb := self;
    b.onBind();
    do_recrop := b;
  end;
end;

function Tpaswl_recropablebuffer.recrop(cx,cy,cwidth,cheight : longint) : Tpaswl_recropablebuffer;
begin
  recrop := vb.do_recrop(self,cx,cy,cwidth,cheight);
end;




constructor T_paswl_buffer.create();
begin
  inherited;
end;

destructor T_paswl_buffer.destroy();
begin
  inherited;
end;




constructor Tpaswl_buffer.create();
begin
  inherited ;
end;

destructor Tpaswl_buffer.destroy();
var  p : T_paswl_shm_pool;
begin
  if (wlo<>nil) then begin
    wl_buffer_destroy(wlo);
    wl_proxy_destroy(wlo);
  end;
  p := T_paswl_shm_pool(pool);
  if (p<>nil)and(p is Tpaswl_shm_managedpool) then begin
    (p as Tpaswl_shm_managedpool).mempool.release_to_pool(oset,int64(bpl)*height);
  end;
  inherited destroy();
end;

constructor Tpaswl_recropablebuffer.create();
begin
  inherited ;
end;

destructor Tpaswl_recropablebuffer.destroy();
begin
//write('Tpaswl_recropablebuffer.destroy();',wlo=nil);
  if (wlo<>nil) then begin
    wl_buffer_destroy(wlo);
    wl_proxy_destroy(wlo);
  end;
  // leave the maybe-mempool-release up to vb
  inherited ;
end;

constructor Tpaswl_virtualbuffer.create();
begin
  inherited ;
end;

destructor Tpaswl_virtualbuffer.destroy();
var  p : T_paswl_shm_pool;
begin
  p := T_paswl_shm_pool(pool);
  if (p<>nil)and(p is Tpaswl_shm_managedpool) then begin
    (p as Tpaswl_shm_managedpool).mempool.release_to_pool(oset,int64(bpl)*height);
  end;
  inherited ;
end;






initialization
begin
end;

finalization
begin
end;

end.



