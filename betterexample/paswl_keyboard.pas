{$mode objfpc}
unit paswl_keyboard;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_bindable;


type

Tpaswl_keyboard=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  protected
  procedure cb_keyboard_keymap(format : dword; fd : Tcint; size : dword); virtual;
  procedure cb_keyboard_enter(serial : dword; surface : Tpaswl_bindable; keys : wl_array); virtual;
  procedure cb_keyboard_leave(serial : dword; surface : Tpaswl_bindable); virtual;
  procedure cb_keyboard_key(serial, time, key, state : dword); virtual;
  procedure cb_keyboard_modifiers(serial, mods_depressed, mods_latched, mods_locked, group : dword); virtual;
  procedure cb_keyboard_repeat_info(rate, delay : longint); virtual;
  
  public
  seat : Tpaswl_bindable;
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;
Tpaswl_keyboard_cls = class of Tpaswl_keyboard;





implementation
uses strings,math;




procedure keyboard_keymap(dat:pointer; o:wl_keyboard; format : dword; fd : Tcint; size : dword); cdecl;
var d : Tpaswl_keyboard;
begin
  if dat<>nil then begin
    d := Tpaswl_keyboard(dat);
    if d.wlo=o then d.cb_keyboard_keymap(format,fd,size)
    else ;//
  end;
end;

procedure keyboard_enter(dat:pointer; o:wl_keyboard; serial : dword; surface : wl_surface; keys : wl_array); cdecl;
var d : Tpaswl_keyboard;
begin
  if dat<>nil then begin
    d := Tpaswl_keyboard(dat);
    if d.wlo=o then d.cb_keyboard_enter(serial,Tpaswl_bindable(wl_proxy_get_user_data(surface)),keys)
    else ;//
  end;
end;

procedure keyboard_leave(dat:pointer; o:wl_keyboard; serial : dword; surface : wl_surface); cdecl;
var d : Tpaswl_keyboard;
begin
  if dat<>nil then begin
    d := Tpaswl_keyboard(dat);
    if d.wlo=o then d.cb_keyboard_leave(serial, Tpaswl_bindable(wl_proxy_get_user_data(surface)))
    else ;//
  end;
end;

procedure keyboard_key(dat:pointer; o:wl_keyboard; serial, time, key, state : dword); cdecl;
var d : Tpaswl_keyboard;
begin
  if dat<>nil then begin
    d := Tpaswl_keyboard(dat);
    if d.wlo=o then d.cb_keyboard_key(serial, time, key, state)
    else ;//
  end;
end;

procedure keyboard_modifiers(dat:pointer; o:wl_keyboard; serial, mods_depressed, mods_latched, mods_locked, group : dword); cdecl;
var d : Tpaswl_keyboard;
begin
  if dat<>nil then begin
    d := Tpaswl_keyboard(dat);
    if d.wlo=o then d.cb_keyboard_modifiers(serial, mods_depressed, mods_latched, mods_locked, group)
    else ;//
  end;
end;

procedure keyboard_repeat_info(dat:pointer; o:wl_keyboard; rate, delay : longint); cdecl;
var d : Tpaswl_keyboard;
begin
  if dat<>nil then begin
    d := Tpaswl_keyboard(dat);
    if d.wlo=o then d.cb_keyboard_repeat_info(rate, delay)
    else ;//
  end;
end;

var
imp_if_keyboard_listener : wl_keyboard_listener = (
  keymap : @keyboard_keymap;
  enter : @keyboard_enter;
  leave : @keyboard_leave;
  key : @keyboard_key;
  modifiers : @keyboard_modifiers;
  repeat_info : @keyboard_repeat_info;
);






procedure Tpaswl_keyboard.cb_keyboard_keymap(format : dword; fd : Tcint; size : dword);
begin
end;

procedure Tpaswl_keyboard.cb_keyboard_enter(serial : dword; surface : Tpaswl_bindable; keys : wl_array);
begin
end;

procedure Tpaswl_keyboard.cb_keyboard_leave(serial : dword; surface : Tpaswl_bindable);
begin
end;

procedure Tpaswl_keyboard.cb_keyboard_key(serial, time, key, state : dword);
begin
end;

procedure Tpaswl_keyboard.cb_keyboard_modifiers(serial, mods_depressed, mods_latched, mods_locked, group : dword);
begin
end;

procedure Tpaswl_keyboard.cb_keyboard_repeat_info(rate, delay : longint);
begin
end;



constructor Tpaswl_keyboard.create();
begin
  inherited create();
end;

destructor Tpaswl_keyboard.destroy();
begin
  wl_keyboard_release(wl_keyboard(wlo));
  inherited destroy();
end;

procedure Tpaswl_keyboard.onBind();
begin
  inherited;
  wl_keyboard_add_listener(wl_keyboard(wlo),imp_if_keyboard_listener,self);
end;

procedure Tpaswl_keyboard.onUnbind();
begin
  inherited;
end;






class function Tpaswl_keyboard.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_keyboard_interface;
end;

class function Tpaswl_keyboard.wl_ifver() : dword;
begin
  wl_ifver := 9;
end;


initialization
begin
end;

finalization
begin
end;

end.



