{$mode objfpc}
unit paswl_collection;

interface
uses wl_basic_types,wl_core,wl_libwl,
paswl_basebindable,paswl_bindable,
paswl_basecollection;


type
Tautobindfield = ^Tpaswl_bindable;
Tautobinddescr = record
  pascls : Tpaswl_bindable_cls;
  field : Tautobindfield;
  wlname : dword;
  required : boolean; { required: fail if missing on startup; essential: }
end;

Tpaswl_collection = class(Tpaswl_basecollection)
  public
  constructor create(); virtual;
  destructor destroy(); override;

  procedure addAutobind(pascls : Tpaswl_bindable_cls; field : Tautobindfield; required : boolean);
  function testAutobinds() : boolean;

  protected
  function bind_registry_obj(pascls : Tpaswl_bindable_cls; name : dword; var iface : Pchar; version : dword) : Tpaswl_bindable;
  function maybe_autobind(const abd : Tautobinddescr; name : dword; var iface : Pchar; version : dword) : boolean;
  procedure check_for_autobinds(name : dword; iface : Pchar; version : dword);
  procedure handle_registry_remove(n : dword);
  procedure cb_registry_global(name : dword; iface : Pchar; version : dword); override;
  procedure cb_registry_global_remove(name : dword); override;
  
  
  private
  autobind_req : array of Tautobinddescr;
end;

Tpaswl_collection_cls = class of Tpaswl_collection;




function startup_display(p : Pchar; c : Tpaswl_collection_cls) : Tpaswl_collection;


implementation
uses sysutils,math;



function startup_display(p : Pchar; c : Tpaswl_collection_cls) : Tpaswl_collection;
var o : Tpaswl_collection;
begin
  o := nil;
  if libwayland_client_avail then begin
    o := c.create();
    if (o<>nil) then begin
      if o.connect(p) then begin
        
      end else begin
        freeandnil(o);
      end;
    end else begin
    end;
  end;
  startup_display := o;
end;





constructor Tpaswl_collection.create();
begin
  inherited;
  setlength(autobind_req,0);
end;

destructor Tpaswl_collection.destroy();
begin
  inherited;
end;

procedure Tpaswl_collection.addAutobind(pascls : Tpaswl_bindable_cls; field : Tautobindfield; required : boolean);
var h : Tautobinddescr;
begin
  h.pascls := pascls;
  h.field := field;
  h.required := required;
  h.wlname := 0;
  setlength(autobind_req,1+length(autobind_req));
  autobind_req[high(autobind_req)] := h;
end;


function Tpaswl_collection.bind_registry_obj(pascls : Tpaswl_bindable_cls; name : dword; var iface : Pchar; version : dword) : Tpaswl_bindable;
var o : Tpaswl_bindable; wo : wl_object; v : dword; ifs : Pwl_interface;
begin
  o := nil;
  ifs := pascls.wl_ifdef();
  v := min(ifs^.version,version);
  v := min(v,pascls.wl_ifver());
  wo := wl_registry_bind(registry.wlo,ifs,v,name);
  if wo<>nil then begin
    o := pascls.create();
    o.wlo := wo;
    o.wlversion := v;
    o.collection := self;
    o.display := display;
  end;
  bind_registry_obj := o;
end;

function Tpaswl_collection.maybe_autobind(const abd : Tautobinddescr; name : dword; var iface : Pchar; version : dword) : boolean;
var ifs : Pwl_interface; o : Tpaswl_bindable;
begin
  maybe_autobind := false;
  if iface<>nil then begin
    if abd.field^=nil then begin
      ifs := abd.pascls.wl_ifdef();
      //write('check for ',iface,'=',ifs^.name,' ');
      if strcomp(iface,ifs^.name)=0 then begin
        //writeln('+');
        o := bind_registry_obj(abd.pascls,name,iface,version);
        if o<>nil then begin
          abd.field^ := o;
          maybe_autobind := true;
          iface := nil;
          o.onBind();
        end;
      end else ;//writeln('-');
    end;  
  end;
end;


procedure Tpaswl_collection.check_for_autobinds(name : dword; iface : Pchar; version : dword);
var i : longint;
begin
//writeln('have ',length(autobind_req),' autobind_req');
  for i := low(autobind_req) to high(autobind_req) do begin
    if maybe_autobind(autobind_req[i],name,iface,version) then begin
      autobind_req[i].wlname := name;
      exit;
    end;
  end;
end;

procedure Tpaswl_collection.handle_registry_remove(n : dword);
var i : longint;
begin
  for i := low(autobind_req) to high(autobind_req) do begin
    if autobind_req[i].field^ <> nil then begin
      if autobind_req[i].wlname=n then begin
        if autobind_req[i].field^.onRegistryRemove() then begin
          freeandnil(autobind_req[i].field^);
        end;
      end;
    end;
  end;
end;

function Tpaswl_collection.testAutobinds() : boolean;
var i : longint; b : boolean;
begin
  b := true;
  for i := low(autobind_req) to high(autobind_req) do begin
    if autobind_req[i].field^=nil then begin
      if autobind_req[i].required then begin
        writeln('failed autobind: ',autobind_req[i].pascls.wl_ifdef^.name);
        b := false;
      end;
    end;
  end;
  testAutobinds := b;
end;

procedure Tpaswl_collection.cb_registry_global(name : dword; iface : Pchar; version : dword);
begin
writeln('Tpaswl_collection.cb_registry_global: ',iface);
  check_for_autobinds(name, iface, version);
end;

procedure Tpaswl_collection.cb_registry_global_remove(name : dword);
begin
writeln('Tpaswl_collection.cb_registry_global_REMOVE: ',name);
  handle_registry_remove(name);
end;



initialization
begin

end;

finalization
begin
end;

end.



