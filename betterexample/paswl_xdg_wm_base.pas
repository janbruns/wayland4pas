{$mode objfpc}
unit paswl_xdg_wm_base;

interface
uses 
  wl_basic_types,
  wl_core,
  wl_libwl,
  wl_xdgshell,
  paswl_bindable,
  paswl_surface,
  paswl_xdg_surface,
  paswl_xdg_positioner;



type

Tpaswl_xdg_wm_base=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  function create_positioner(c : Tpaswl_xdg_positioner_cls) : Tpaswl_xdg_positioner;
  function get_xdg_surface(surf : Tpaswl_surface; c : Tpaswl_xdg_surface_cls) : Tpaswl_xdg_surface;

//function xdg_wm_base_get_xdg_surface(o : xdg_wm_base; surface : wl_surface) : xdg_surface;

  protected
  procedure cb_xdg_wm_base_ping(serial : dword); virtual;
  
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_xdg_wm_basecls = class of Tpaswl_xdg_wm_base;




implementation




procedure xdg_wm_base_ping(dat:pointer; o:xdg_wm_base; serial : dword); cdecl;
var d : Tpaswl_xdg_wm_base;
begin
  if dat<>nil then begin
    d := Tpaswl_xdg_wm_base(dat);
    if d.wlo=o then d.cb_xdg_wm_base_ping(serial)
    else ;//
  end;
end;


var
imp_if_xdg_wm_base_listener : xdg_wm_base_listener = (
  ping : @xdg_wm_base_ping;
);



procedure Tpaswl_xdg_wm_base.cb_xdg_wm_base_ping(serial : dword);
begin
  xdg_wm_base_pong(xdg_wm_base(wlo),serial);
end;


constructor Tpaswl_xdg_wm_base.create();
begin
  inherited create();
end;

destructor Tpaswl_xdg_wm_base.destroy();
begin
  xdg_wm_base_destroy(wlo);
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_xdg_wm_base.onBind();
begin
  inherited;
  xdg_wm_base_add_listener(xdg_wm_base(wlo),imp_if_xdg_wm_base_listener,self);
end;

procedure Tpaswl_xdg_wm_base.onUnbind();
begin
  inherited;
end;



function Tpaswl_xdg_wm_base.create_positioner(c : Tpaswl_xdg_positioner_cls) : Tpaswl_xdg_positioner;
var o : Tpaswl_xdg_positioner; wo : xdg_positioner; 
begin
  wo := xdg_wm_base_create_positioner(xdg_wm_base(wlo));
  o := Tpaswl_xdg_positioner(make_child(c,wo));
  create_positioner := o;
end;

function Tpaswl_xdg_wm_base.get_xdg_surface(surf : Tpaswl_surface; c : Tpaswl_xdg_surface_cls) : Tpaswl_xdg_surface;
var o : Tpaswl_xdg_surface; wo : xdg_surface; 
begin
  wo := xdg_wm_base_get_xdg_surface(xdg_wm_base(wlo),wl_surface(surf.wlo));
  o := Tpaswl_xdg_surface(make_child(c,wo));
  get_xdg_surface := o;
end;



class function Tpaswl_xdg_wm_base.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := xdg_wm_base_interface;
end;

class function Tpaswl_xdg_wm_base.wl_ifver() : dword;
begin
  wl_ifver := 6;
end;


initialization
begin
end;

finalization
begin
end;

end.



