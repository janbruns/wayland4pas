{$mode objfpc}
unit paswl_decor;

interface
uses 
  wl_basic_types,
  wl_core,
  wl_libwl,
  paswl_bindable,
  paswl_shm_pool,
  paswl_viewport,
  paswl_collection2,
  paswl_pointer,
  paswl_buffer,
  paswl_surface,
  paswl_subsurface,
  paswl_seat,
  paswl_decor_border,
  paswl_decor_knobs,
  minifont,
  math;

const
maxknobs = 4;
num_knobbordercols = 2;
num_knobstates = paswl_decor_knob_state_disabled+1;
invalid_serial = $100000000;
initial_maxwidth = 8000; // not relevant in resource allocation
initial_maxheight = 8000;
redraw_titlebar_bar = 1;
redraw_titlebar_but = 2;
redraw_titlebar_any = redraw_titlebar_bar or redraw_titlebar_but;
redraw_border = 4;
redraw_resize = redraw_titlebar_bar or redraw_border;
redraw_doneresize = 8;


type
Tdecor = class;

Tdecormouse = class(Tpaswl_pointer)
  public
  decor : Tdecor;
  lastenter : Tpaswl_bindable;
  enterx,entery,lastx,lasty : longint;
  protected
  procedure cb_pointer_enter(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed); override;
  procedure cb_pointer_leave(serial : dword; surface : Tpaswl_bindable); override;
  procedure cb_pointer_button(serial, time, button, state : dword); override;
  procedure cb_pointer_motion(time : dword; surface_x, surface_y : Twlfixed); override;
end;

Ttitlebutsurf = class(Tpaswl_surface)
  public
  procedure make_buf(c,s : dword; p : Tpaswl_surface);
  destructor destroy(); override;
  public
  which : longint;
  decor : Tdecor;
  buf,oldbuf : Tpaswl_buffer;
  sub : Tpaswl_subsurface;
  cur : Tpaswl_surface;
  curb : Tpaswl_buffer;
end;

TonEnter=procedure(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed) of object;
TonLeave=procedure(serial : dword; surface : Tpaswl_bindable) of object;
TonButton=procedure(serial, time, button, state : dword) of object;
TonMotion=procedure(time : dword; surface_x, surface_y : Twlfixed) of object;
{ b : background-buf to render to, might be nil
  x,y : move subsurf (relative to mainsurf) here, if any
  dx,dy : dims of the subsurf, if any
}
TonRender=function(b : T_paswl_buffer; x,y,dx,dy,reason : longint) : boolean of object;

Tdecor = class
  procedure init(); virtual; abstract;
  procedure prepare_new_size();
  procedure make_mainbuf();
  procedure initial_settings(); virtual;
  function makeknob(b : byte) : Tpaswl_decor_knob;
  procedure makeknobs();
  procedure printknobs();
  function check_knobs_button(x,y,b,s : longint) : boolean;
  procedure check_knobs_hl(x,y : longint);
  procedure make_fallbackcursor();
  procedure onCallbackDone(callback_data : dword);
  procedure request_draw_frame();
  procedure onFrame(d : dword);
  procedure make_border();
  procedure draw_titletext();
  procedure draw_titletext_using_titlesurf();
  procedure draw_titletext_using_appwinbuf();

  function point_in_titlebar(s : Tpaswl_surface; x,y : longint) : boolean;

  procedure onCloseButton(k : Tpaswl_decor_knob); virtual;
  procedure onMaximizeButton(k : Tpaswl_decor_knob); virtual; 

  procedure onAppButton(k : Tpaswl_decor_knob); virtual;

  procedure start_resize(seat : Tpaswl_seat; serial,where : dword); virtual; abstract;
  procedure start_move(seat : Tpaswl_seat; serial : dword); virtual; abstract;
  procedure onFrame(d : dword); virtual; abstract;
  
  procedure setCursorSurf(serial : dword; q : longint); virtual;
  
  procedure onEnter(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed); virtual;
  procedure onLeave(serial : dword; surface : Tpaswl_bindable); virtual;
  procedure onButton(serial, time, button, state : dword); virtual;
  procedure onMotion(time : dword; surface_x, surface_y : Twlfixed); virtual;
  
  procedure setgeo(); virtual; abstract;
  procedure doack(var q : qword); virtual; abstract;
  
  destructor destroy(); override;
  procedure redraw(); virtual;
  
  public
  bordercol : array of dword;
  decorborder : Tpaswl_decorborder;
  knobs : array[0..maxknobs-1] of Tpaswl_decor_knob;
  { 2 blend-colors per knob-state, drawn atop icon images to indicate the states,
    typically the two are the same, differing give checker-patterns }
  default_knobblend  : array[0..2*num_knobstates-1] of dword;
  { n bordercolors per knob, if knob-size requires more, the inner is repeated }
  default_knobborder : array[0..num_knobbordercols*maxknobs-1] of dword;
  initial_knobglyph : array[0..maxknobs-1] of byte;
  
  pool : Tpaswl_shm_managedpool;
  buf : Tpaswl_buffer;
  vbuf : Tpaswl_virtualbuffer;
  cbuf : T_paswl_buffer;
  clc : Tpaswl_collection2;
  mouse : Tdecormouse;
  mainsurf : Tpaswl_surface;
  cursrf :  Tpaswl_surface;
  curbuf : Tpaswl_buffer;
  
  
  reqserial,reqserial2 : qword;
  
  planwidth,planheight,
  minwidth,minheight,
  maxwidth,maxheight,
  titleheight,titlerot,
  iconsize,iconsizem,iconmargin,
  titlefontscalex,titlefontscaley,
  iconfontsize,
  mintxtspc : longint;
  
  { on maximize, store the old dimensions}
  stored_width,stored_height : longint;
  
  state_activated, state_maximized : boolean;
  state_resizeing, allow_nodraw_title_onresize : boolean;
  
  titlesurf : Tpaswl_surface;
  titlesub : Tpaswl_subsurface;
  titlebuf,oldtitlebuf : T_paswl_buffer;
  titlecols : array[false..true,0..3] of dword;
  txtcolor : array[false..true] of dword;

  redrawwhat : dword;
  
  name,appid : ansistring;
  
  { knob last pushed }
  lastdown : longint;
  
  framecbobj : wl_callback;
  cbufvp : Tpaswl_viewport;
  
  latestX, latestY : longint;
  latestenter : Tpaswl_bindable;

  HonEnter : TonEnter;
  HonLeave : TonLeave;
  HonButton : TonButton;
  HonMotion : TonMotion;
  HonRender : TonRender;
end;




implementation
uses sysutils;


procedure Tdecor.prepare_new_size();
var v : Tpaswl_virtualbuffer; w,h : longint; p : Pdword;
begin
  if (cbuf is Tpaswl_recropablebuffer)and((cbuf as Tpaswl_recropablebuffer).vb=vbuf) then begin
    if (planwidth<=vbuf.width)and(planheight<=vbuf.height) then begin
      cbuf := Tpaswl_recropablebuffer(cbuf).recrop(0,0,planwidth,planheight);
    end else begin
      writeln('resize by vbuf-grow');
      w := vbuf.width;
      h := vbuf.height;
      while (w<planwidth) do w*=2;
      while (h<planheight) do h*=2;
      v := pool.create_buffer( w, h, 4*w, wl_shm_format_argb8888, Tpaswl_virtualbuffer) as Tpaswl_virtualbuffer;
      p := v.getDataPtr();
      filldword(p^,w*h,0);
      Tpaswl_recropablebuffer(cbuf).vb := v;
      cbuf := Tpaswl_recropablebuffer(cbuf).recrop(0,0,planwidth,planheight);
      vbuf.destroy();
      vbuf := v;
    end;
  end else begin
    if (cbuf.width=1)and(cbuf.height=1)and(assigned(cbufvp)) then begin
      cbufvp.set_destination(planwidth,planheight);
    end else begin
      // shouldn't happen
    end;
  end;
end;


procedure Tdecor.make_mainbuf();
begin
  { using the 1x1 pixel background way requires vieporter, of course,
    but it doesn't make no sense with titlesurf=nil 
    (assuming that only happens with subcompositor missing)
  }
  if assigned(titlesurf) and assigned(clc.viewporter) then begin
    cbuf := pool.create_buffer(1,1,4,wl_shm_format_argb8888,Tpaswl_buffer);
    cbuf.setPixel(0,0,$00000000);
    cbufvp := clc.viewporter.get_viewport(mainsurf);
    cbufvp.set_Source(0,0,256,256);
    cbufvp.set_Destination(planwidth,planheight);
  end else begin
    vbuf := pool.create_buffer(planwidth,planheight,4*planwidth,wl_shm_format_argb8888,Tpaswl_virtualbuffer) as Tpaswl_virtualbuffer;
    vbuf.fillcolor(0,0,0,0);
    cbuf := vbuf.get_recropable(0,0,planwidth,planheight);
  end;
end;


procedure Tdecor.initial_settings();
begin
  reqserial  := invalid_serial;
  reqserial2 := invalid_serial;
  name := '';
  appid := '';
  iconsize := 22;
  iconmargin := 2;
  iconsizem := iconsize+iconmargin;
  titleheight := 26;
  titlerot := 1;
  titlefontscalex := 1;
  titlefontscaley := 2;
  mintxtspc := 3;
  state_activated := true;
  planwidth := 500;
  planheight := 300;
  minwidth := 8*iconsize;
  minheight := minwidth;
  maxwidth := initial_maxwidth;
  maxheight := initial_maxheight;

  allow_nodraw_title_onresize := false;

  txtcolor[false] := $FFc0c0c0;
  txtcolor[true]  := $FFc0c0c0;
  
  setlength(bordercol,7);
  bordercol[0] := $80808080;
  bordercol[1] := $c0c0c0c0;
  bordercol[2] := $ff808080;
  bordercol[3] := $ff808080;
  bordercol[4] := $ff808080;
  bordercol[5] := $ffc0c0c0;
  bordercol[6] := $FF404040;
  titlecols[false][0] := $FF404040;
  titlecols[false][1] := $FF808080;
  titlecols[false][2] := $FF404040;
  titlecols[false][3] := $FF808080;
  titlecols[true][0] := $FF0080FF;
  titlecols[true][1] := $FF00FFFF;
  titlecols[true][2] := $FF0040FF;
  titlecols[true][3] := $FF00FF00;

  default_knobblend[0] := 0;
  default_knobblend[1] := 0;
  default_knobblend[2] := $20ffffff;
  default_knobblend[3] := $20ffffff;
  default_knobblend[4] := $80404040;
  default_knobblend[5] := $80404040;
  default_knobblend[6] := $c0808080;
  default_knobblend[7] := $c0404040;
  
  default_knobborder[0] := $ff808080;
  default_knobborder[1] := $ffc0c0c0;
  default_knobborder[2] := $ffffffff;
  default_knobborder[3] := $ffc0c0c0;
  default_knobborder[4] := $ff808080;
  default_knobborder[5] := $ffc0c0c0;
  default_knobborder[6] := $ff404040;
  default_knobborder[7] := $ff808080;

  iconfontsize := 2;
  initial_knobglyph[0] := $58;
  initial_knobglyph[1] := $71;
  initial_knobglyph[2] := $3f;
  initial_knobglyph[3] := $00;
end;

function Tdecor.makeknob(b : byte) : Tpaswl_decor_knob;
var k : Tpaswl_decor_knob; buf1 : Tpaswl_virtualbuffer; brd : array of dword; ks,i,j,brdsiz : longint;
begin
  ks := iconfontsize*8;
  buf1 := pool.create_buffer(ks,ks,4*ks,wl_shm_format_argb8888,Tpaswl_virtualbuffer) as Tpaswl_virtualbuffer;
  buf1.fillcolor($ffa0a0a0,$ffa0a080,$ffa0a0a0,$ffa0a0a0);
  paint_glyph_to_buffer(buf1,0,0,iconfontsize,iconfontsize,b,$ff000000);
  k := Tpaswl_decor_knob.create();
  k.pool := pool;
  brdsiz := (iconsize-ks) DIV 2;
  setlength(brd,brdsiz*4);
  for j:= 0 to 3 do begin
    for i:=0 to brdsiz-1 do begin
      brd[j*brdsiz +i] := default_knobborder[j*2+min(i,1)];
    end;
  end;
  k.setBorder(brdsiz,@brd[0]);
  k.setBlend(@default_knobblend[0]);
  k.load(buf1,ks,ks);
  buf1.destroy(); 
  makeknob := k;
end;

procedure Tdecor.onCloseButton(k : Tpaswl_decor_knob);
begin
  clc.alldone := true;
end;

procedure Tdecor.onMaximizeButton(k : Tpaswl_decor_knob);
begin
end;

procedure Tdecor.onAppButton(k : Tpaswl_decor_knob);
begin
  titlerot += 1;
  if titlerot>2 then titlerot := 0;
  redrawwhat := redraw_doneresize or redraw_resize;
  request_draw_frame();
end;



procedure Tdecor.makeknobs();
var i : longint;
begin
  for i := 0 to high(knobs) do knobs[i] := makeknob(initial_knobglyph[i]);
  knobs[0].onclick := @self.onCloseButton;
  knobs[1].onclick := @self.onMaximizeButton;
  knobs[3].onclick := @self.onAppButton;
  knobs[2].setState(3);
end;


procedure paintknob(k : Tpaswl_decor_knob; x,y : longint; b : T_paswl_buffer; s : Tpaswl_surface);
begin
  if assigned(k) then begin
    k.paint(b,x,y);
    if assigned(k.curbuf) 
    then s.damage(x,y,k.curbuf.width,k.curbuf.height);
  end;
end;

procedure Tdecor.printknobs();
var txtv,hbc,dx,dy,bw : longint; b : T_paswl_buffer; s : Tpaswl_surface;
begin
  hbc := high(bordercol);
  bw := hbc+1;
  dx := 0;
  dy := 0;
  b := vbuf;
  s := mainsurf;
  if titlesurf<>nil then begin
    s := titlesurf;
    b := titlebuf;
  end else begin
    dx := bw;
    dy := bw;
    if (titlerot=1) then begin
      dx := planwidth -titleheight -bw;
    end;
  end;
  s.attach(b);
  txtv := (titleheight -iconsize) div 2;
  if titlerot=0 then begin
    paintknob(knobs[0],dx+planwidth-2*bw-1*(iconsizem),dy+txtv,b,s);
    paintknob(knobs[1],dx+planwidth-2*bw-2*(iconsizem),dy+txtv,b,s);
    paintknob(knobs[2],dx+planwidth-2*bw-3*(iconsizem),dy+txtv,b,s);
    paintknob(knobs[3],dx+iconmargin,dy+txtv,b,s);
  end else begin
    paintknob(knobs[0],dx+txtv,dy+iconmargin,b,s);
    paintknob(knobs[1],dx+txtv,dy+1*iconsizem+iconmargin,b,s);
    paintknob(knobs[2],dx+txtv,dy+2*iconsizem+iconmargin,b,s);
    paintknob(knobs[3],dx+txtv,dy+planheight-2*bw-1*iconsizem,b,s);
  end;
end;

function Tdecor.point_in_titlebar(s : Tpaswl_surface; x,y : longint) : boolean;
var bw : longint;
begin
  if assigned(titlesurf) then begin
    point_in_titlebar := (s=titlesurf);
  end else begin
    if (s=mainsurf) then begin
      bw := high(bordercol)+1;
      if (titlerot=0) then begin
        if (y>=bw)and(y<(bw+titleheight)) then begin
          if (x>=bw)and(x<(planwidth-bw)) 
          then point_in_titlebar := true
          else point_in_titlebar := false;
        end else point_in_titlebar := false;
      end else if (titlerot=2) then begin
        if (x>=bw)and(x<(bw+titleheight)) then begin
          if (y>=bw)and(y<(planheight-bw)) 
          then point_in_titlebar := true
          else point_in_titlebar := false;
        end else point_in_titlebar := false;
      end else begin
        if (x>=(planwidth-bw-titleheight))and(x<(planwidth-bw)) then begin
          if (y>=bw)and(y<(planheight-bw)) 
          then point_in_titlebar := true
          else point_in_titlebar := false;
        end else point_in_titlebar := false;
      end;
    end else point_in_titlebar := false;
  end;
end;

procedure Tdecor.check_knobs_hl(x,y : longint);
var i : longint;
begin
  for i := 0 to high(knobs) do begin
    if assigned(knobs[i]) then begin
      if knobs[i].pointWithinLastPaint(x,y) then begin
//writeln(i,' chkdknobs ',x,'/',y, ' state:', knobs[i].currentstate);
        if (knobs[i].currentstate=0) then begin
          knobs[i].setState(1);
          request_draw_frame();
          redrawwhat := redrawwhat or redraw_titlebar_but;
        end;
      end else begin
        if (knobs[i].currentstate=1) then begin
          knobs[i].setState(0);
          request_draw_frame();
          redrawwhat := redrawwhat or redraw_titlebar_but;
        end;
      end;
    end;
  end;
end;



function Tdecor.check_knobs_button(x,y,b,s : longint) : boolean;
var i : longint;
begin
  check_knobs_button := false;
  if s=wl_pointer_button_state_pressed then begin
    for i := 0 to high(knobs) do begin
      if assigned(knobs[i]) then begin
        
        if knobs[i].pointWithinLastPaint(x,y) then begin
          check_knobs_button := true;
          if (knobs[i].currentstate<2) then begin
            knobs[i].setState(2);
            request_draw_frame();
            redrawwhat := redrawwhat or redraw_titlebar_but;
            if (lastdown>=0)and(lastdown<>i)and(lastdown<=high(knobs)) then begin
              knobs[lastdown].setState(0);
            end;
            lastdown := i;
          end;
        end else begin
        end;
      end;
    end;
  end else if s=wl_pointer_button_state_released then begin
    if (lastdown>=0) and (lastdown<=high(knobs)) then begin
      if assigned(knobs[lastdown]) then begin
        if knobs[lastdown].pointWithinLastPaint(x,y) then begin
          check_knobs_button := true;
          writeln('FIRE');
          knobs[lastdown].onclick(knobs[lastdown]);
          knobs[lastdown].setState(0);
          request_draw_frame();
          redrawwhat := redrawwhat or redraw_titlebar_but;
          lastdown := -1;
        end else begin
          writeln('RELEASE');
          knobs[lastdown].setState(0);
          request_draw_frame();
          redrawwhat := redrawwhat or redraw_titlebar_but;
          lastdown := -1;
        end;
      end;
    end;
  end;
end;

procedure Tdecor.make_fallbackcursor();
var dw : dword; i,x,y : longint;
begin
  curbuf := pool.create_buffer(fallbackcursorsize,fallbackcursorsize,4*fallbackcursorsize,wl_shm_format_argb8888,Tpaswl_buffer) as Tpaswl_buffer;
  i := 0;
  for y := 0 to fallbackcursorsize-1 do begin
    for x := 0 to fallbackcursorsize-1 do begin
      dw := fallbackcursor[i] and 15;
      dw := dw*16+dw;
      dw := (dw shl 16) or (dw shl 8) or dw;
      dw := dw or ((fallbackcursor[i] and $f0) shl 24);
      dw := dw or ((fallbackcursor[i] and $f0) shl 20);
      curbuf.setPixel(x,y,dw);
      inc(i);
    end;
  end;
  cursrf := clc.compositor.create_surface(Tpaswl_surface) as Tpaswl_surface;
  cursrf.attach(curbuf);
  cursrf.commit();
end;

procedure Tdecor.make_border();
begin
  decorborder := Tpaswl_decorborder.create();
  decorborder.take_compositor(clc.compositor,clc.subcompositor);
  decorborder.take_viewporter(clc.viewporter);
  decorborder.take_pool(pool);
  decorborder.take_surface(mainsurf);
  decorborder.take_colors(high(bordercol)+1,@bordercol[0]);
  decorborder.make();
  decorborder.onResize(cbuf,planwidth,planheight);
end;

destructor Tdecor.destroy();
var i : longint;
begin
  if assigned(framecbobj) then wl_proxy_destroy(framecbobj);
  freeandnil(titlesub);
  freeandnil(titlesurf);
  freeandnil(titlebuf);
  freeandnil(oldtitlebuf);
  freeandnil(decorborder);
  
  for i := 0 to maxknobs-1 do freeandnil(knobs[i]);
  freeandnil(mainsurf);
  freeandnil(cbufvp);
  freeandnil(cursrf);
  freeandnil(curbuf);
  freeandnil(buf);
  freeandnil(cbuf);
  freeandnil(vbuf);
  freeandnil(pool);
  inherited;
end;




procedure Tdecor.onCallbackDone(callback_data : dword);
begin
  framecbobj := nil;
  onFrame(callback_data);
end;


procedure wl_callback_done(dat:pointer; o:wl_callback; callback_data : dword); cdecl;
begin
  Tdecor(dat).onCallbackDone(callback_data);
  wl_proxy_destroy(o);
end;

var framecblisten : wl_callback_listener = (
  done : @wl_callback_done;
);

procedure Tdecor.request_draw_frame();
begin
  if not assigned(framecbobj) then begin
    framecbobj := mainsurf.frame();
    wl_callback_add_listener(framecbobj,framecblisten,self);
    mainsurf.commit();
  end else ;//writeln('already waiting for framecb');
end;

procedure Tdecor.draw_titletext();
begin
  if assigned(titlesurf) 
  then draw_titletext_using_titlesurf()
  else draw_titletext_using_appwinbuf();
end;

procedure Tdecor.draw_titletext_using_appwinbuf();
var txtv,txtcenter,hbc,bw : longint; sa : boolean; c : Pdword;
begin
  sa := state_activated;
  c := @titlecols[sa][0];
  if assigned(oldtitlebuf) then freeandnil(oldtitlebuf);
  oldtitlebuf := titlebuf;
  titlebuf := nil;
  if assigned(oldtitlebuf) and not oldtitlebuf.busy then freeandnil(oldtitlebuf);

  hbc := high(bordercol);
  bw := hbc+1;
  txtv := (titleheight -titlefontscaley*8) div 2;
  txtcenter := -2*bw;
  txtcenter -= 4*iconsizem +iconmargin +mintxtspc;
  if titlerot=0 then begin
    txtcenter -= 8*length(name)*titlefontscalex;
    txtcenter += planwidth;
    txtcenter := max(0,txtcenter div 2);
    txtcenter += iconsizem;
    titlebuf := vbuf.get_recropable(bw,bw,planwidth-2*bw,titleheight);
    titlebuf.fillcolor(c[0],c[1],c[2],c[3]);
    paint_string_to_buffer(titlebuf,txtcenter+mintxtspc,txtv,titlefontscalex,titlefontscaley,titlerot,name,txtcolor[sa]);
    mainsurf.attach(cbuf);
    mainsurf.damage(bw,bw,(planwidth-2*bw),titleheight);
  end else begin
    txtcenter -= 8*length(name)*titlefontscalex;
    txtcenter += planheight;
    txtcenter := max(0,txtcenter div 2);

    if (titlerot=1) 
    then titlebuf := vbuf.get_recropable(planwidth -titleheight -bw,bw,titleheight,planheight-2*bw)
    else titlebuf := vbuf.get_recropable(bw,bw,titleheight,planheight-2*bw);
    if (titlerot=1) 
    then titlebuf.fillcolor(c[2],c[0],c[3],c[1])
    else titlebuf.fillcolor(c[1],c[3],c[0],c[2]);
    if (titlerot=1) 
    then paint_string_to_buffer(titlebuf,txtv,txtcenter+3*iconsizem+mintxtspc,titlefontscalex,titlefontscaley,titlerot,name,txtcolor[sa])
    else paint_string_to_buffer(titlebuf,txtv,-txtcenter-iconsizem-mintxtspc+planheight-2*bw,titlefontscalex,titlefontscaley,titlerot,name,txtcolor[sa]);
    mainsurf.attach(cbuf);
    if (titlerot=1) 
    then mainsurf.damage(planwidth -titleheight -bw,bw,(titleheight),(planheight-2*bw))
    else mainsurf.damage(bw,bw,(titleheight),(planheight-2*bw));
  end;
end;


procedure Tdecor.draw_titletext_using_titlesurf();
var txtv,txtcenter,hbc,bw : longint; sa : boolean; c : Pdword;
begin
  sa := state_activated;
  c := @titlecols[sa][0];
  if assigned(oldtitlebuf) then freeandnil(oldtitlebuf);
  oldtitlebuf := titlebuf;
  titlebuf := nil;
  if assigned(oldtitlebuf) and not oldtitlebuf.busy then freeandnil(oldtitlebuf);

  if state_resizeing and allow_nodraw_title_onresize then begin
    titlebuf := pool.create_buffer(1,1,4,wl_shm_format_argb8888,Tpaswl_buffer) as Tpaswl_buffer;
    titlebuf.setPixel(0,0,0);
    titlesurf.attach(titlebuf);
    exit;
  end;
  hbc := high(bordercol);
  bw := hbc+1;
  txtv := (titleheight -titlefontscaley*8) div 2;
  txtcenter := -2*bw;
  txtcenter -= 4*iconsizem +iconmargin +mintxtspc;
  if titlerot=0 then begin
    txtcenter -= 8*length(name)*titlefontscalex;
    txtcenter += planwidth;
    txtcenter := max(0,txtcenter div 2);
    txtcenter += iconsizem;
    titlesub.set_position(bw,bw);
    titlebuf := pool.create_buffer(planwidth-2*bw,titleheight,4*(planwidth-2*bw),wl_shm_format_argb8888,Tpaswl_buffer) as Tpaswl_buffer;
    titlebuf.fillcolor(c[0],c[1],c[2],c[3]);
    paint_string_to_buffer(titlebuf,txtcenter+mintxtspc,txtv,titlefontscalex,titlefontscaley,titlerot,name,txtcolor[sa]);
    titlesurf.attach(titlebuf);
    titlesurf.damage(0,0,(planwidth-2*bw),titleheight);
  end else begin
    txtcenter -= 8*length(name)*titlefontscalex;
    txtcenter += planheight;
    txtcenter := max(0,txtcenter div 2);
    
    if (titlerot=1) then titlesub.set_position(planwidth -titleheight -bw,bw)
    else titlesub.set_position(bw ,bw);
    
    titlebuf := pool.create_buffer(titleheight,planheight-2*bw,4*titleheight,wl_shm_format_argb8888,Tpaswl_buffer) as Tpaswl_buffer;
    if (titlerot=1) 
    then titlebuf.fillcolor(c[2],c[0],c[3],c[1])
    else titlebuf.fillcolor(c[1],c[3],c[0],c[2]);
    if (titlerot=1) 
    then paint_string_to_buffer(titlebuf,txtv,txtcenter+3*iconsizem+mintxtspc,titlefontscalex,titlefontscaley,titlerot,name,txtcolor[sa])
    else paint_string_to_buffer(titlebuf,txtv,-txtcenter-iconsizem-mintxtspc+planheight-2*bw,titlefontscalex,titlefontscaley,titlerot,name,txtcolor[sa]);
    titlesurf.attach(titlebuf);
    titlesurf.damage(0,0,(titleheight),(planheight-2*bw));
  end;
end;


procedure Ttitlebutsurf.make_buf(c,s : dword; p : Tpaswl_surface);
begin
  freeandnil(oldbuf);
  oldbuf := buf;
  buf := nil;
  if assigned(oldbuf) and not(oldbuf.busy) then freeandnil(oldbuf);
  buf := decor.pool.create_buffer( s, s, 4*s, wl_shm_format_argb8888, Tpaswl_buffer) as Tpaswl_buffer;
  buf.fillcolor(c,c,c,c);
  if not assigned(sub) then begin
    sub := decor.clc.subcompositor.get_subsurface(self,p,Tpaswl_subsurface);
  end;
  attach(buf);
  damage(0,0,s,s);
end;

destructor Ttitlebutsurf.destroy();
begin
  freeandnil(oldbuf);
  freeandnil(sub);
  freeandnil(buf);
  inherited;
end;

procedure Tdecor.redraw();
var clientarea : Tpaswl_recropablebuffer; bw ,x,y,dx,dy: longint;
begin
write('R',redrawwhat);

  if (redrawwhat and redraw_doneresize)>0 then begin
    bw := high(bordercol)+1;
    case titlerot of
      0 : begin x := bw; y := bw+titleheight; dx := planwidth-2*bw; dy := planheight-2*bw-titleheight; end;
      1 : begin x := bw; y := bw; dx := planwidth-2*bw-titleheight; dy := planheight-2*bw; end;
      2 : begin x := bw+titleheight; y := bw; dx := planwidth-2*bw-titleheight; dy := planheight-2*bw; end;
    end;
    if assigned(vbuf) and (cbuf is Tpaswl_recropablebuffer) then begin
      writeln('vbuf-based inner render');
      clientarea := vbuf.get_recropable(x,y,dx,dy);
      clientarea.fillcolor($00000000,$00000000,$00000000,$00000000);
      if assigned(HonRender) then begin
        HonRender(clientarea,x,y,dx,dy,redrawwhat);
      end;
      clientarea.destroy();
      mainsurf.attach(cbuf);
      mainsurf.damage(0,0,planwidth,planheight);
    end else begin
      writeln('subsurf-based client inner render');
      if assigned(HonRender) then begin
        HonRender(nil,x,y,dx,dy,redrawwhat);
      end;
    end;
  end;
  
  if (redrawwhat and redraw_titlebar_any)>0 then begin
    if (redrawwhat and redraw_titlebar_bar)>0 then begin
      draw_titletext();
      printknobs();
    end else if (redrawwhat and redraw_titlebar_but)>0 then begin
      printknobs();
    end;
  end;
  
  if (redrawwhat and redraw_border)>0 then begin
    decorborder.onResize(cbuf,planwidth,planheight);
  end;

  setgeo();

  doack(reqserial2);

  if assigned(titlesurf) then titlesurf.commit();
  mainsurf.attach(cbuf);
  mainsurf.commit();
end;


procedure Tdecor.setCursorSurf(serial : dword; q : longint);
begin
  mouse.set_cursor(serial,cursrf,0,0);
end;

procedure Tdecor.onEnter(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed);
var q,bw : dword;
begin
  latestenter := surface;
  latestX := longint(surface_x) div 256;
  latestY := longint(surface_y) div 256;
  if (surface=mainsurf) then begin
    bw := high(bordercol)+1;
    q := 0;
    if (latestX<bw) then q += 4;
    if (latesty<bw) then q += 1;
    if (latestx>=planwidth-bw) then q += 8;
    if (latesty>=planheight-bw) then q +=2;
    if q=0 then begin
      setCursorSurf(serial,q);
    end else begin
      if assigned(HonEnter) 
      then HonEnter(serial, surface, surface_x, surface_y)
      else setCursorSurf(serial,q);
    end;
  end else if (surface=titlesurf) then begin
    setCursorSurf(serial,-1);
  end else if assigned(HonEnter) then HonEnter(serial, surface, surface_x, surface_y);
end;

procedure Tdecor.onLeave(serial : dword; surface : Tpaswl_bindable);
begin
  if assigned(HonLeave) then HonLeave(serial,surface);
end;

var reorderwhich : array[0..7] of byte = (8,1,4,2,5,9,6,10);

procedure Tdecor.onButton(serial, time, button, state : dword);
var bw,q : longint;
begin
  if latestenter is Tborderpartsurf then begin
    start_resize(clc.seat, serial, reorderwhich[Tborderpartsurf(latestenter).which and 7]);
  end else begin
    if point_in_titlebar(Tpaswl_surface(latestenter),latestx,latesty) then begin
      if check_knobs_button(latestx,latesty,button,state) then exit
      else start_move(clc.seat, serial);
    end else begin
      bw := high(bordercol)+1;
      q := 0;
      if (latestenter=mainsurf) then begin
        if (latestX<bw) then q += 4;
        if (latesty<bw) then q += 1;
        if (latestx>=planwidth-bw) then q += 8;
        if (latesty>=planheight-bw) then q +=2;
        if q>0 then begin
          start_resize(clc.seat, serial, q );
        end;
      end;
      if (latestenter=titlesurf) and assigned(titlesurf) then begin
        start_move(clc.seat,serial);
      end;
      if assigned(HonButton) then HonButton(serial, time, button, state);
    end;
  end;
end;

procedure Tdecor.onMotion(time : dword; surface_x, surface_y : Twlfixed);
begin
  latestX := longint(surface_x) div 256;
  latestY := longint(surface_y) div 256;
  if (latestenter=titlesurf) then begin
    check_knobs_hl(latestx,latesty);
  end else begin
    if (latestenter=mainsurf) and not(assigned(titlesurf)) then begin
      check_knobs_hl(latestx,latesty);
    end;
  end;
  if assigned(HonMotion) then HonMotion(time, surface_x, surface_y);
end;

procedure Tdecormouse.cb_pointer_leave(serial : dword; surface : Tpaswl_bindable);
begin
  if assigned(decor) then decor.onLeave(serial, surface);
end;

procedure Tdecormouse.cb_pointer_enter(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed);
begin
  lastenter := surface;
  enterx := surface_x;
  entery := surface_y;
  lastx := surface_x div 256;
  lasty := surface_y div 256;
  if assigned(decor) then decor.onEnter(serial, surface, surface_x,surface_y);
end;

procedure Tdecormouse.cb_pointer_motion(time : dword; surface_x, surface_y : Twlfixed);
begin
  lastx := surface_x div 256;
  lasty := surface_y div 256;
  if assigned(decor) then decor.onMotion(time, surface_x, surface_y);
end;

procedure Tdecormouse.cb_pointer_button(serial, time, button, state : dword);
begin
  if assigned(decor) then decor.onButton(serial, time, button, state);
end;


begin
end.
