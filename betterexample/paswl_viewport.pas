{$mode objfpc}
unit paswl_viewport;

interface
uses wl_basic_types,wl_core,wl_libwl, wl_viewporter, paswl_bindable;


type

Tpaswl_viewport=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  procedure set_source(x, y, width, height : Twlfixed);
  procedure set_destination(width, height : longint);
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;
Tpaswl_viewport_cls = class of Tpaswl_viewport;








implementation
uses baseunix;







constructor Tpaswl_viewport.create();
begin
  inherited create();
end;

destructor Tpaswl_viewport.destroy();
begin
  wp_viewport_destroy(wp_viewport(wlo));
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_viewport.set_source(x, y, width, height : Twlfixed);
begin
  wp_viewport_set_source(wlo, x, y, width, height);
end;

procedure Tpaswl_viewport.set_destination(width, height : longint);
begin
  wp_viewport_set_destination(wlo, width, height);
end;




procedure Tpaswl_viewport.onBind();
begin
  inherited;
  // no events to register for
end;

procedure Tpaswl_viewport.onUnbind();
begin
  inherited;
end;




class function Tpaswl_viewport.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wp_viewport_interface;
end;

class function Tpaswl_viewport.wl_ifver() : dword;
begin
  wl_ifver := 1;
end;


initialization
begin
end;

finalization
begin
end;

end.



