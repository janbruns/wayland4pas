{$mode objfpc}
unit paswl_shell_surface;

interface
uses 
  wl_basic_types,
  wl_core,
  wl_libwl,
  paswl_bindable,
  paswl_seat,
  paswl_surface,
  paswl_output;



type

Tpaswl_shell_surface=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  procedure pong(serial : dword);
  procedure move(seat : Tpaswl_seat; serial : dword);
  procedure resize(seat : Tpaswl_seat; serial, edges : dword);
  procedure set_toplevel();
  procedure set_transient(parent : Tpaswl_surface; x, y : longint; flags : dword);
  procedure set_fullscreen(method, framerate : dword; output : wl_output);
  procedure set_popup(seat : Tpaswl_seat; serial : dword; parent : Tpaswl_surface; x, y : longint; flags : dword);
  procedure set_maximized(output : Tpaswl_output);
  procedure set_title(title : Pchar);
  procedure set_class(class_ : Pchar);

  protected
  procedure cb_shell_surface_ping(serial : dword); virtual;
  procedure cb_shell_surface_configure(edges : dword; width, height : longint); virtual;
  procedure cb_shell_surface_popup_done(); virtual;
  
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_shell_surface_cls = class of Tpaswl_shell_surface;




implementation

procedure shell_surface_ping(dat:pointer; o:wl_shell_surface; serial : dword); cdecl;
var d : Tpaswl_shell_surface;
begin
  if dat<>nil then begin
    d := Tpaswl_shell_surface(dat);
    if d.wlo=o then d.cb_shell_surface_ping(serial)
    else ;//
  end;
end;

procedure shell_surface_configure(dat:pointer; o:wl_shell_surface; edges : dword; width, height : longint); cdecl;
var d : Tpaswl_shell_surface;
begin
  if dat<>nil then begin
    d := Tpaswl_shell_surface(dat);
    if d.wlo=o then d.cb_shell_surface_configure(edges, width, height)
    else ;//
  end;
end;

procedure shell_surface_popup_done(dat:pointer; o:wl_shell_surface); cdecl;
var d : Tpaswl_shell_surface;
begin
  if dat<>nil then begin
    d := Tpaswl_shell_surface(dat);
    if d.wlo=o then d.cb_shell_surface_popup_done()
    else ;//
  end;
end;

var
imp_if_shell_surface_listener : wl_shell_surface_listener =(
  ping : @shell_surface_ping;
  configure : @shell_surface_configure;
  popup_done : @shell_surface_popup_done;
);

procedure Tpaswl_shell_surface.cb_shell_surface_ping(serial : dword);
begin
  writeln('Tpaswl_shell_surface.cb_shell_surface_ping ',serial);
  pong(serial);
end;

procedure Tpaswl_shell_surface.cb_shell_surface_configure(edges : dword; width, height : longint); 
begin
writeln('Tpaswl_shell_surface.cb_shell_surface_configure');
end;

procedure Tpaswl_shell_surface.cb_shell_surface_popup_done();
begin
end;

constructor Tpaswl_shell_surface.create();
begin
  inherited create();
end;

destructor Tpaswl_shell_surface.destroy();
begin
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_shell_surface.onBind();
begin
writeln('Tpaswl_shell_surface.onBind();');
  inherited;
  wl_shell_surface_add_listener(wlo,imp_if_shell_surface_listener,self);
end;

procedure Tpaswl_shell_surface.onUnbind();
begin
  inherited;
end;


procedure Tpaswl_shell_surface.pong(serial : dword);
begin
  wl_shell_surface_pong(wlo, serial);
end;

procedure Tpaswl_shell_surface.move(seat : Tpaswl_seat; serial : dword);
begin
  wl_shell_surface_move(wlo, seat.wlo, serial);
end;

procedure Tpaswl_shell_surface.resize(seat : Tpaswl_seat; serial, edges : dword);
begin
  wl_shell_surface_resize(wlo, seat.wlo, serial, edges);
end;

procedure Tpaswl_shell_surface.set_toplevel();
begin
  wl_shell_surface_set_toplevel(wlo);
end;

procedure Tpaswl_shell_surface.set_transient(parent : Tpaswl_surface; x, y : longint; flags : dword);
begin
  wl_shell_surface_set_transient(wlo, parent.wlo, x, y, flags);
end;

procedure Tpaswl_shell_surface.set_fullscreen(method, framerate : dword; output : wl_output);
begin
  wl_shell_surface_set_fullscreen(wlo, method, framerate, output);
end;

procedure Tpaswl_shell_surface.set_popup(seat : Tpaswl_seat; serial : dword; parent : Tpaswl_surface; x, y : longint; flags : dword);
begin
  wl_shell_surface_set_popup(wlo, seat.wlo, serial, parent.wlo, x, y, flags);
end;

procedure Tpaswl_shell_surface.set_maximized(output : Tpaswl_output);
begin
  if assigned(output) 
  then wl_shell_surface_set_maximized(wlo, output.wlo)
  else wl_shell_surface_set_maximized(wlo, nil);
end;

procedure Tpaswl_shell_surface.set_title(title : Pchar);
begin
  wl_shell_surface_set_title(wlo, title);
end;

procedure Tpaswl_shell_surface.set_class(class_ : Pchar);
begin
  wl_shell_surface_set_class(wlo, class_);
end;





class function Tpaswl_shell_surface.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_shell_surface_interface;
end;

class function Tpaswl_shell_surface.wl_ifver() : dword;
begin
  wl_ifver := 1;
end;


initialization
begin
end;

finalization
begin
end;

end.



