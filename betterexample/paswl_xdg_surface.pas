{$mode objfpc}
unit paswl_xdg_surface;

interface
uses 
  wl_basic_types,
  wl_core,
  wl_libwl,
  wl_xdgshell,
  paswl_bindable,
  paswl_xdg_toplevel,
  paswl_xdg_popup,
  paswl_xdg_positioner;


type

Tpaswl_xdg_surface=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  function get_toplevel(c : Tpaswl_xdg_toplevel_cls) : Tpaswl_xdg_toplevel;
  function get_popup(parent : Tpaswl_xdg_surface; positioner : Tpaswl_xdg_positioner; c : Tpaswl_xdg_popup_cls) : Tpaswl_xdg_popup;

  procedure set_window_geometry(x, y, width, height : longint);
  procedure ack_configure(serial : dword);

  function seen_configure() : boolean;
  function consume_configure() : dword;


  protected
  procedure cb_xdg_surface_configure(serial : dword); virtual;

  protected
  latestconfigserial : qword;
  
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_xdg_surface_cls = class of Tpaswl_xdg_surface;







implementation




procedure xdg_surface_configure(dat:pointer; o:xdg_surface; serial : dword); cdecl;
var d : Tpaswl_xdg_surface;
begin
  if dat<>nil then begin
    d := Tpaswl_xdg_surface(dat);
    if d.wlo=o then d.cb_xdg_surface_configure(serial)
    else ;//
  end;
end;


var
imp_if_xdg_surface_listener : xdg_surface_listener = (
  configure : @xdg_surface_configure;
);



procedure Tpaswl_xdg_surface.cb_xdg_surface_configure(serial : dword);
begin
  //ack_configure(serial);
  latestconfigserial := serial;
  writeln('xdg_surf received serial: ',serial);
end;

const invalid_serial = $0000000100000000;

function Tpaswl_xdg_surface.seen_configure() : boolean;
begin
  seen_configure := latestconfigserial < invalid_serial;
end;

function Tpaswl_xdg_surface.consume_configure() : dword;
begin
  consume_configure := latestconfigserial;
  latestconfigserial := invalid_serial;
end;

constructor Tpaswl_xdg_surface.create();
begin
  inherited create();
  latestconfigserial := invalid_serial;
end;

destructor Tpaswl_xdg_surface.destroy();
begin
  xdg_surface_destroy(xdg_surface(wlo));
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_xdg_surface.onBind();
begin
  inherited;
  xdg_surface_add_listener(xdg_surface(wlo),imp_if_xdg_surface_listener,self);
end;

procedure Tpaswl_xdg_surface.onUnbind();
begin
  inherited;
end;


function Tpaswl_xdg_surface.get_toplevel(c : Tpaswl_xdg_toplevel_cls) : Tpaswl_xdg_toplevel;
var o : Tpaswl_xdg_toplevel; wo : xdg_toplevel; 
begin
  o := nil;
  wo := xdg_surface_get_toplevel(xdg_surface(wlo));
  if wo<>nil then begin
    o := Tpaswl_xdg_toplevel(make_child(c,wo));
  end;
  get_toplevel := o;
end;

function Tpaswl_xdg_surface.get_popup(parent : Tpaswl_xdg_surface; positioner : Tpaswl_xdg_positioner; c : Tpaswl_xdg_popup_cls) : Tpaswl_xdg_popup;
var o : Tpaswl_xdg_popup; wo : xdg_popup; wpo : xdg_surface;
begin
  o := nil;
  wpo := nil;
  if parent<>nil then wpo := xdg_surface(parent.wlo);
  wo := xdg_surface_get_popup(xdg_surface(wlo), wpo, xdg_positioner(positioner.wlo) );
  if wo<>nil then begin
    o := Tpaswl_xdg_popup(make_child(c,wo));
  end;
  get_popup := o;
end;


procedure Tpaswl_xdg_surface.set_window_geometry(x, y, width, height : longint);
begin
  xdg_surface_set_window_geometry(xdg_surface(wlo),x, y, width, height);
end;

procedure Tpaswl_xdg_surface.ack_configure(serial : dword);
begin
  xdg_surface_ack_configure(xdg_surface(wlo),serial);
end;



class function Tpaswl_xdg_surface.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_surface_interface;
end;

class function Tpaswl_xdg_surface.wl_ifver() : dword;
begin
  wl_ifver := 6;
end;


initialization
begin
end;

finalization
begin
end;

end.



