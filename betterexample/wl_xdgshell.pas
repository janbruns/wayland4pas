{$mode objfpc}
unit wl_xdgshell; //auto-generated


{$PACKRECORDS C}


interface
uses wl_basic_types,wl_libwl,wl_core;


type
xdg_wm_base = wl_object;
xdg_positioner = wl_object;
xdg_surface = wl_object;
xdg_toplevel = wl_object;
xdg_popup = wl_object;



var
xdg_wm_base_interface : Pwl_interface;
xdg_positioner_interface : Pwl_interface;
xdg_surface_interface : Pwl_interface;
xdg_toplevel_interface : Pwl_interface;
xdg_popup_interface : Pwl_interface;

procedure xdg_wm_base_destroy(o : xdg_wm_base);
function xdg_wm_base_create_positioner(o : xdg_wm_base) : xdg_positioner;
function xdg_wm_base_get_xdg_surface(o : xdg_wm_base; surface : wl_surface) : xdg_surface;
procedure xdg_wm_base_pong(o : xdg_wm_base; serial : dword);
procedure xdg_positioner_destroy(o : xdg_positioner);
procedure xdg_positioner_set_size(o : xdg_positioner; width, height : longint);
procedure xdg_positioner_set_anchor_rect(o : xdg_positioner; x, y, width, height : longint);
procedure xdg_positioner_set_anchor(o : xdg_positioner; anchor : dword);
procedure xdg_positioner_set_gravity(o : xdg_positioner; gravity : dword);
procedure xdg_positioner_set_constraint_adjustment(o : xdg_positioner; constraint_adjustment : dword);
procedure xdg_positioner_set_offset(o : xdg_positioner; x, y : longint);
procedure xdg_positioner_set_reactive(o : xdg_positioner);
procedure xdg_positioner_set_parent_size(o : xdg_positioner; parent_width, parent_height : longint);
procedure xdg_positioner_set_parent_configure(o : xdg_positioner; serial : dword);
procedure xdg_surface_destroy(o : xdg_surface);
function xdg_surface_get_toplevel(o : xdg_surface) : xdg_toplevel;
function xdg_surface_get_popup(o : xdg_surface; parent : xdg_surface; positioner : xdg_positioner) : xdg_popup;
procedure xdg_surface_set_window_geometry(o : xdg_surface; x, y, width, height : longint);
procedure xdg_surface_ack_configure(o : xdg_surface; serial : dword);
procedure xdg_toplevel_destroy(o : xdg_toplevel);
procedure xdg_toplevel_set_parent(o : xdg_toplevel; parent : xdg_toplevel);
procedure xdg_toplevel_set_title(o : xdg_toplevel; title : Pchar);
procedure xdg_toplevel_set_app_id(o : xdg_toplevel; app_id : Pchar);
procedure xdg_toplevel_show_window_menu(o : xdg_toplevel; seat : wl_seat; serial : dword; x, y : longint);
procedure xdg_toplevel_move(o : xdg_toplevel; seat : wl_seat; serial : dword);
procedure xdg_toplevel_resize(o : xdg_toplevel; seat : wl_seat; serial, edges : dword);
procedure xdg_toplevel_set_max_size(o : xdg_toplevel; width, height : longint);
procedure xdg_toplevel_set_min_size(o : xdg_toplevel; width, height : longint);
procedure xdg_toplevel_set_maximized(o : xdg_toplevel);
procedure xdg_toplevel_unset_maximized(o : xdg_toplevel);
procedure xdg_toplevel_set_fullscreen(o : xdg_toplevel; output : wl_output);
procedure xdg_toplevel_unset_fullscreen(o : xdg_toplevel);
procedure xdg_toplevel_set_minimized(o : xdg_toplevel);
procedure xdg_popup_destroy(o : xdg_popup);
procedure xdg_popup_grab(o : xdg_popup; seat : wl_seat; serial : dword);
procedure xdg_popup_reposition(o : xdg_popup; positioner : xdg_positioner; token : dword);

type
xdg_wm_base_listener = record
  ping : procedure(dat:pointer; o:xdg_wm_base; serial : dword); cdecl;
end;

xdg_positioner_listener = record
end;

xdg_surface_listener = record
  configure : procedure(dat:pointer; o:xdg_surface; serial : dword); cdecl;
end;

xdg_toplevel_listener = record
  configure : procedure(dat:pointer; o:xdg_toplevel; width, height : longint; states : wl_array); cdecl;
  close : procedure(dat:pointer; o:xdg_toplevel); cdecl;
  configure_bounds : procedure(dat:pointer; o:xdg_toplevel; width, height : longint); cdecl;
  wm_capabilities : procedure(dat:pointer; o:xdg_toplevel; capabilities : wl_array); cdecl;
end;

xdg_popup_listener = record
  configure : procedure(dat:pointer; o:xdg_popup; x, y, width, height : longint); cdecl;
  popup_done : procedure(dat:pointer; o:xdg_popup); cdecl;
  repositioned : procedure(dat:pointer; o:xdg_popup; token : dword); cdecl;
end;



procedure xdg_wm_base_add_listener(o:xdg_wm_base; var l:xdg_wm_base_listener; dat:pointer);
procedure xdg_positioner_add_listener(o:xdg_positioner; var l:xdg_positioner_listener; dat:pointer);
procedure xdg_surface_add_listener(o:xdg_surface; var l:xdg_surface_listener; dat:pointer);
procedure xdg_toplevel_add_listener(o:xdg_toplevel; var l:xdg_toplevel_listener; dat:pointer);
procedure xdg_popup_add_listener(o:xdg_popup; var l:xdg_popup_listener; dat:pointer);


const
xdg_wm_base_error_role = $00000000;
xdg_wm_base_error_defunct_surfaces = $00000001;
xdg_wm_base_error_not_the_topmost_popup = $00000002;
xdg_wm_base_error_invalid_popup_parent = $00000003;
xdg_wm_base_error_invalid_surface_state = $00000004;
xdg_wm_base_error_invalid_positioner = $00000005;
xdg_wm_base_error_unresponsive = $00000006;
xdg_positioner_error_invalid_input = $00000000;
xdg_positioner_anchor_none = $00000000;
xdg_positioner_anchor_top = $00000001;
xdg_positioner_anchor_bottom = $00000002;
xdg_positioner_anchor_left = $00000003;
xdg_positioner_anchor_right = $00000004;
xdg_positioner_anchor_top_left = $00000005;
xdg_positioner_anchor_bottom_left = $00000006;
xdg_positioner_anchor_top_right = $00000007;
xdg_positioner_anchor_bottom_right = $00000008;
xdg_positioner_gravity_none = $00000000;
xdg_positioner_gravity_top = $00000001;
xdg_positioner_gravity_bottom = $00000002;
xdg_positioner_gravity_left = $00000003;
xdg_positioner_gravity_right = $00000004;
xdg_positioner_gravity_top_left = $00000005;
xdg_positioner_gravity_bottom_left = $00000006;
xdg_positioner_gravity_top_right = $00000007;
xdg_positioner_gravity_bottom_right = $00000008;
xdg_positioner_constraint_adjustment_none = $00000000;
xdg_positioner_constraint_adjustment_slide_x = $00000001;
xdg_positioner_constraint_adjustment_slide_y = $00000002;
xdg_positioner_constraint_adjustment_flip_x = $00000004;
xdg_positioner_constraint_adjustment_flip_y = $00000008;
xdg_positioner_constraint_adjustment_resize_x = $00000010;
xdg_positioner_constraint_adjustment_resize_y = $00000020;
xdg_surface_error_not_constructed = $00000001;
xdg_surface_error_already_constructed = $00000002;
xdg_surface_error_unconfigured_buffer = $00000003;
xdg_surface_error_invalid_serial = $00000004;
xdg_surface_error_invalid_size = $00000005;
xdg_surface_error_defunct_role_object = $00000006;
xdg_toplevel_error_invalid_resize_edge = $00000000;
xdg_toplevel_error_invalid_parent = $00000001;
xdg_toplevel_error_invalid_size = $00000002;
xdg_toplevel_resize_edge_none = $00000000;
xdg_toplevel_resize_edge_top = $00000001;
xdg_toplevel_resize_edge_bottom = $00000002;
xdg_toplevel_resize_edge_left = $00000004;
xdg_toplevel_resize_edge_top_left = $00000005;
xdg_toplevel_resize_edge_bottom_left = $00000006;
xdg_toplevel_resize_edge_right = $00000008;
xdg_toplevel_resize_edge_top_right = $00000009;
xdg_toplevel_resize_edge_bottom_right = $0000000A;
xdg_toplevel_state_maximized = $00000001;
xdg_toplevel_state_fullscreen = $00000002;
xdg_toplevel_state_resizing = $00000003;
xdg_toplevel_state_activated = $00000004;
xdg_toplevel_state_tiled_left = $00000005;
xdg_toplevel_state_tiled_right = $00000006;
xdg_toplevel_state_tiled_top = $00000007;
xdg_toplevel_state_tiled_bottom = $00000008;
xdg_toplevel_state_suspended = $00000009;
xdg_toplevel_wm_capabilities_window_menu = $00000001;
xdg_toplevel_wm_capabilities_maximize = $00000002;
xdg_toplevel_wm_capabilities_fullscreen = $00000003;
xdg_toplevel_wm_capabilities_minimize = $00000004;
xdg_popup_error_invalid_grab = $00000000;


var
xdg_wm_base_ifacedat : Twl_interface = (name:'xdg_wm_base'; version:6; method_count:4; methods:nil; event_count:1; events:nil); 
xdg_positioner_ifacedat : Twl_interface = (name:'xdg_positioner'; version:6; method_count:10; methods:nil; event_count:0; events:nil); 
xdg_surface_ifacedat : Twl_interface = (name:'xdg_surface'; version:6; method_count:5; methods:nil; event_count:1; events:nil); 
xdg_toplevel_ifacedat : Twl_interface = (name:'xdg_toplevel'; version:6; method_count:14; methods:nil; event_count:4; events:nil); 
xdg_popup_ifacedat : Twl_interface = (name:'xdg_popup'; version:6; method_count:3; methods:nil; event_count:3; events:nil); 


implementation



procedure xdg_wm_base_add_listener(o:xdg_wm_base; var l:xdg_wm_base_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure xdg_positioner_add_listener(o:xdg_positioner; var l:xdg_positioner_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure xdg_surface_add_listener(o:xdg_surface; var l:xdg_surface_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure xdg_toplevel_add_listener(o:xdg_toplevel; var l:xdg_toplevel_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure xdg_popup_add_listener(o:xdg_popup; var l:xdg_popup_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;



procedure xdg_wm_base_destroy(o : xdg_wm_base);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

function xdg_wm_base_create_positioner(o : xdg_wm_base) : xdg_positioner;
var parm : Tparamarr1;
begin
  parm[0].o := nil;
  xdg_wm_base_create_positioner := xdg_positioner(wl_proxy_marshal_array_constructor(o,1,@parm[0],xdg_positioner_interface));
end;

function xdg_wm_base_get_xdg_surface(o : xdg_wm_base; surface : wl_surface) : xdg_surface;
var parm : Tparamarr2;
begin
  parm[0].o := nil;
  parm[1].o := surface;
  xdg_wm_base_get_xdg_surface := xdg_surface(wl_proxy_marshal_array_constructor(o,2,@parm[0],xdg_surface_interface));
end;

procedure xdg_wm_base_pong(o : xdg_wm_base; serial : dword);
var parm : Tparamarr1;
begin
  parm[0].u := serial;
  wl_proxy_marshal_array(o,3,@parm[0]);
end;

procedure xdg_positioner_destroy(o : xdg_positioner);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

procedure xdg_positioner_set_size(o : xdg_positioner; width, height : longint);
var parm : Tparamarr2;
begin
  parm[0].i := width;
  parm[1].i := height;
  wl_proxy_marshal_array(o,1,@parm[0]);
end;

procedure xdg_positioner_set_anchor_rect(o : xdg_positioner; x, y, width, height : longint);
var parm : Tparamarr4;
begin
  parm[0].i := x;
  parm[1].i := y;
  parm[2].i := width;
  parm[3].i := height;
  wl_proxy_marshal_array(o,2,@parm[0]);
end;

procedure xdg_positioner_set_anchor(o : xdg_positioner; anchor : dword);
var parm : Tparamarr1;
begin
  parm[0].u := anchor;
  wl_proxy_marshal_array(o,3,@parm[0]);
end;

procedure xdg_positioner_set_gravity(o : xdg_positioner; gravity : dword);
var parm : Tparamarr1;
begin
  parm[0].u := gravity;
  wl_proxy_marshal_array(o,4,@parm[0]);
end;

procedure xdg_positioner_set_constraint_adjustment(o : xdg_positioner; constraint_adjustment : dword);
var parm : Tparamarr1;
begin
  parm[0].u := constraint_adjustment;
  wl_proxy_marshal_array(o,5,@parm[0]);
end;

procedure xdg_positioner_set_offset(o : xdg_positioner; x, y : longint);
var parm : Tparamarr2;
begin
  parm[0].i := x;
  parm[1].i := y;
  wl_proxy_marshal_array(o,6,@parm[0]);
end;

procedure xdg_positioner_set_reactive(o : xdg_positioner);
begin
  wl_proxy_marshal_array(o,7,nil);
end;

procedure xdg_positioner_set_parent_size(o : xdg_positioner; parent_width, parent_height : longint);
var parm : Tparamarr2;
begin
  parm[0].i := parent_width;
  parm[1].i := parent_height;
  wl_proxy_marshal_array(o,8,@parm[0]);
end;

procedure xdg_positioner_set_parent_configure(o : xdg_positioner; serial : dword);
var parm : Tparamarr1;
begin
  parm[0].u := serial;
  wl_proxy_marshal_array(o,9,@parm[0]);
end;

procedure xdg_surface_destroy(o : xdg_surface);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

function xdg_surface_get_toplevel(o : xdg_surface) : xdg_toplevel;
var parm : Tparamarr1;
begin
  parm[0].o := nil;
  xdg_surface_get_toplevel := xdg_toplevel(wl_proxy_marshal_array_constructor(o,1,@parm[0],xdg_toplevel_interface));
end;

function xdg_surface_get_popup(o : xdg_surface; parent : xdg_surface; positioner : xdg_positioner) : xdg_popup;
var parm : Tparamarr3;
begin
  parm[0].o := nil;
  parm[1].o := parent;
  parm[2].o := positioner;
  xdg_surface_get_popup := xdg_popup(wl_proxy_marshal_array_constructor(o,2,@parm[0],xdg_popup_interface));
end;

procedure xdg_surface_set_window_geometry(o : xdg_surface; x, y, width, height : longint);
var parm : Tparamarr4;
begin
  parm[0].i := x;
  parm[1].i := y;
  parm[2].i := width;
  parm[3].i := height;
  wl_proxy_marshal_array(o,3,@parm[0]);
end;

procedure xdg_surface_ack_configure(o : xdg_surface; serial : dword);
var parm : Tparamarr1;
begin
  parm[0].u := serial;
  wl_proxy_marshal_array(o,4,@parm[0]);
end;

procedure xdg_toplevel_destroy(o : xdg_toplevel);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

procedure xdg_toplevel_set_parent(o : xdg_toplevel; parent : xdg_toplevel);
var parm : Tparamarr1;
begin
  parm[0].o := parent;
  wl_proxy_marshal_array(o,1,@parm[0]);
end;

procedure xdg_toplevel_set_title(o : xdg_toplevel; title : Pchar);
var parm : Tparamarr1;
begin
  parm[0].s := title;
  wl_proxy_marshal_array(o,2,@parm[0]);
end;

procedure xdg_toplevel_set_app_id(o : xdg_toplevel; app_id : Pchar);
var parm : Tparamarr1;
begin
  parm[0].s := app_id;
  wl_proxy_marshal_array(o,3,@parm[0]);
end;

procedure xdg_toplevel_show_window_menu(o : xdg_toplevel; seat : wl_seat; serial : dword; x, y : longint);
var parm : Tparamarr4;
begin
  parm[0].o := seat;
  parm[1].u := serial;
  parm[2].i := x;
  parm[3].i := y;
  wl_proxy_marshal_array(o,4,@parm[0]);
end;

procedure xdg_toplevel_move(o : xdg_toplevel; seat : wl_seat; serial : dword);
var parm : Tparamarr2;
begin
  parm[0].o := seat;
  parm[1].u := serial;
  wl_proxy_marshal_array(o,5,@parm[0]);
end;

procedure xdg_toplevel_resize(o : xdg_toplevel; seat : wl_seat; serial, edges : dword);
var parm : Tparamarr3;
begin
  parm[0].o := seat;
  parm[1].u := serial;
  parm[2].u := edges;
  wl_proxy_marshal_array(o,6,@parm[0]);
end;

procedure xdg_toplevel_set_max_size(o : xdg_toplevel; width, height : longint);
var parm : Tparamarr2;
begin
  parm[0].i := width;
  parm[1].i := height;
  wl_proxy_marshal_array(o,7,@parm[0]);
end;

procedure xdg_toplevel_set_min_size(o : xdg_toplevel; width, height : longint);
var parm : Tparamarr2;
begin
  parm[0].i := width;
  parm[1].i := height;
  wl_proxy_marshal_array(o,8,@parm[0]);
end;

procedure xdg_toplevel_set_maximized(o : xdg_toplevel);
begin
  wl_proxy_marshal_array(o,9,nil);
end;

procedure xdg_toplevel_unset_maximized(o : xdg_toplevel);
begin
  wl_proxy_marshal_array(o,10,nil);
end;

procedure xdg_toplevel_set_fullscreen(o : xdg_toplevel; output : wl_output);
var parm : Tparamarr1;
begin
  parm[0].o := output;
  wl_proxy_marshal_array(o,11,@parm[0]);
end;

procedure xdg_toplevel_unset_fullscreen(o : xdg_toplevel);
begin
  wl_proxy_marshal_array(o,12,nil);
end;

procedure xdg_toplevel_set_minimized(o : xdg_toplevel);
begin
  wl_proxy_marshal_array(o,13,nil);
end;

procedure xdg_popup_destroy(o : xdg_popup);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

procedure xdg_popup_grab(o : xdg_popup; seat : wl_seat; serial : dword);
var parm : Tparamarr2;
begin
  parm[0].o := seat;
  parm[1].u := serial;
  wl_proxy_marshal_array(o,1,@parm[0]);
end;

procedure xdg_popup_reposition(o : xdg_popup; positioner : xdg_positioner; token : dword);
var parm : Tparamarr2;
begin
  parm[0].o := positioner;
  parm[1].u := token;
  wl_proxy_marshal_array(o,2,@parm[0]);
end;


var
reqtypes : array[0..47] of Pwl_interface = (
  // 0:xdg_wm_base.destroy
  @xdg_positioner_ifacedat, // 0:xdg_wm_base.create_positioner
  @xdg_surface_ifacedat, @wl_surface_ifacedat, // 1:xdg_wm_base.get_xdg_surface
  nil, // 3:xdg_wm_base.pong
  // 4:xdg_positioner.destroy
  nil, nil, // 4:xdg_positioner.set_size
  nil, nil, nil, nil, // 6:xdg_positioner.set_anchor_rect
  nil, // 10:xdg_positioner.set_anchor
  nil, // 11:xdg_positioner.set_gravity
  nil, // 12:xdg_positioner.set_constraint_adjustment
  nil, nil, // 13:xdg_positioner.set_offset
  // 15:xdg_positioner.set_reactive
  nil, nil, // 15:xdg_positioner.set_parent_size
  nil, // 17:xdg_positioner.set_parent_configure
  // 18:xdg_surface.destroy
  @xdg_toplevel_ifacedat, // 18:xdg_surface.get_toplevel
  @xdg_popup_ifacedat, @xdg_surface_ifacedat, @xdg_positioner_ifacedat, // 19:xdg_surface.get_popup
  nil, nil, nil, nil, // 22:xdg_surface.set_window_geometry
  nil, // 26:xdg_surface.ack_configure
  // 27:xdg_toplevel.destroy
  @xdg_toplevel_ifacedat, // 27:xdg_toplevel.set_parent
  nil, // 28:xdg_toplevel.set_title
  nil, // 29:xdg_toplevel.set_app_id
  @wl_seat_ifacedat, nil, nil, nil, // 30:xdg_toplevel.show_window_menu
  @wl_seat_ifacedat, nil, // 34:xdg_toplevel.move
  @wl_seat_ifacedat, nil, nil, // 36:xdg_toplevel.resize
  nil, nil, // 39:xdg_toplevel.set_max_size
  nil, nil, // 41:xdg_toplevel.set_min_size
  // 43:xdg_toplevel.set_maximized
  // 43:xdg_toplevel.unset_maximized
  @wl_output_ifacedat, // 43:xdg_toplevel.set_fullscreen
  // 44:xdg_toplevel.unset_fullscreen
  // 44:xdg_toplevel.set_minimized
  // 44:xdg_popup.destroy
  @wl_seat_ifacedat, nil, // 44:xdg_popup.grab
  @xdg_positioner_ifacedat, nil// 46:xdg_popup.reposition
);

argtypes : array[0..12] of Pwl_interface = (
  nil, // 0:xdg_wm_base.ping
  nil, // 1:xdg_surface.configure
  nil, nil, nil, // 2:xdg_toplevel.configure
  // 5:xdg_toplevel.close
  nil, nil, // 5:xdg_toplevel.configure_bounds
  nil, // 7:xdg_toplevel.wm_capabilities
  nil, nil, nil, nil, // 8:xdg_popup.configure
  // 12:xdg_popup.popup_done
  nil// 12:xdg_popup.repositioned
);

requests : array[0..35] of Twl_message = (
  (name:'destroy'; signature:''; types:nil),// xdg_wm_base
  (name:'create_positioner'; signature:'n'; types:@reqtypes[0]),// xdg_wm_base
  (name:'get_xdg_surface'; signature:'no'; types:@reqtypes[1]),// xdg_wm_base
  (name:'pong'; signature:'u'; types:@reqtypes[3]),// xdg_wm_base
  (name:'destroy'; signature:''; types:nil),// xdg_positioner
  (name:'set_size'; signature:'ii'; types:@reqtypes[4]),// xdg_positioner
  (name:'set_anchor_rect'; signature:'iiii'; types:@reqtypes[6]),// xdg_positioner
  (name:'set_anchor'; signature:'u'; types:@reqtypes[10]),// xdg_positioner
  (name:'set_gravity'; signature:'u'; types:@reqtypes[11]),// xdg_positioner
  (name:'set_constraint_adjustment'; signature:'u'; types:@reqtypes[12]),// xdg_positioner
  (name:'set_offset'; signature:'ii'; types:@reqtypes[13]),// xdg_positioner
  (name:'set_reactive'; signature:''; types:nil),// xdg_positioner
  (name:'set_parent_size'; signature:'ii'; types:@reqtypes[15]),// xdg_positioner
  (name:'set_parent_configure'; signature:'u'; types:@reqtypes[17]),// xdg_positioner
  (name:'destroy'; signature:''; types:nil),// xdg_surface
  (name:'get_toplevel'; signature:'n'; types:@reqtypes[18]),// xdg_surface
  (name:'get_popup'; signature:'n?oo'; types:@reqtypes[19]),// xdg_surface
  (name:'set_window_geometry'; signature:'iiii'; types:@reqtypes[22]),// xdg_surface
  (name:'ack_configure'; signature:'u'; types:@reqtypes[26]),// xdg_surface
  (name:'destroy'; signature:''; types:nil),// xdg_toplevel
  (name:'set_parent'; signature:'?o'; types:@reqtypes[27]),// xdg_toplevel
  (name:'set_title'; signature:'s'; types:@reqtypes[28]),// xdg_toplevel
  (name:'set_app_id'; signature:'s'; types:@reqtypes[29]),// xdg_toplevel
  (name:'show_window_menu'; signature:'ouii'; types:@reqtypes[30]),// xdg_toplevel
  (name:'move'; signature:'ou'; types:@reqtypes[34]),// xdg_toplevel
  (name:'resize'; signature:'ouu'; types:@reqtypes[36]),// xdg_toplevel
  (name:'set_max_size'; signature:'ii'; types:@reqtypes[39]),// xdg_toplevel
  (name:'set_min_size'; signature:'ii'; types:@reqtypes[41]),// xdg_toplevel
  (name:'set_maximized'; signature:''; types:nil),// xdg_toplevel
  (name:'unset_maximized'; signature:''; types:nil),// xdg_toplevel
  (name:'set_fullscreen'; signature:'?o'; types:@reqtypes[43]),// xdg_toplevel
  (name:'unset_fullscreen'; signature:''; types:nil),// xdg_toplevel
  (name:'set_minimized'; signature:''; types:nil),// xdg_toplevel
  (name:'destroy'; signature:''; types:nil),// xdg_popup
  (name:'grab'; signature:'ou'; types:@reqtypes[44]),// xdg_popup
  (name:'reposition'; signature:'ou'; types:@reqtypes[46])// xdg_popup
);

events : array[0..8] of Twl_message = (
  (name:'ping'; signature:'u'; types:@argtypes[0]),// xdg_wm_base
  (name:'configure'; signature:'u'; types:@argtypes[1]),// xdg_surface
  (name:'configure'; signature:'iia'; types:@argtypes[2]),// xdg_toplevel
  (name:'close'; signature:''; types:nil),// xdg_toplevel
  (name:'configure_bounds'; signature:'ii'; types:@argtypes[5]),// xdg_toplevel
  (name:'wm_capabilities'; signature:'a'; types:@argtypes[7]),// xdg_toplevel
  (name:'configure'; signature:'iiii'; types:@argtypes[8]),// xdg_popup
  (name:'popup_done'; signature:''; types:nil),// xdg_popup
  (name:'repositioned'; signature:'u'; types:@argtypes[12])// xdg_popup
);

procedure initifs();
begin
  xdg_wm_base_interface := @xdg_wm_base_ifacedat;
  xdg_positioner_interface := @xdg_positioner_ifacedat;
  xdg_surface_interface := @xdg_surface_ifacedat;
  xdg_toplevel_interface := @xdg_toplevel_ifacedat;
  xdg_popup_interface := @xdg_popup_ifacedat;
  xdg_wm_base_ifacedat.methods := @requests[0];
  xdg_positioner_ifacedat.methods := @requests[4];
  xdg_surface_ifacedat.methods := @requests[14];
  xdg_toplevel_ifacedat.methods := @requests[19];
  xdg_popup_ifacedat.methods := @requests[33];
  xdg_wm_base_ifacedat.events := @events[0];
  xdg_positioner_ifacedat.events := nil;
  xdg_surface_ifacedat.events := @events[1];
  xdg_toplevel_ifacedat.events := @events[2];
  xdg_popup_ifacedat.events := @events[6];
end;


begin
  initifs();
end.

