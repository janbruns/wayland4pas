{$mode objfpc}
unit wl_basic_types;

{$PACKRECORDS C}

interface
uses ctypes;

type
Tcint = cint;
PPwl_interface = ^Pwl_interface;
Pwl_interface = ^Twl_interface;
Pwl_message = ^Twl_message;
Twl_message = record
  name,signature : Pchar;
  types : PPwl_interface;
end;
Twl_interface = record
  name : Pchar;
  version : Tcint;
  method_count : Tcint;
  methods : Pwl_message;
  event_count : Tcint;
  events : Pwl_message;
end;
wl_object = class end;
wl_display = wl_object;
Twl_array = record
  size,alloc : SizeInt;
  data : Pbyte;
end;
wl_array = ^Twl_array;
Twlfixed = longint;

Tparm = record
  case byte of
    1 : (i : longint);
    2 : (u : dword);
    3 : (o : wl_object);
    4 : (d : Tcint);
    5 : (f : dword);
    6 : (s : Pchar);
    7 : (a : wl_array);
end;

Tparamarr1 = array[0..0] of Tparm;
Tparamarr2 = array[0..1] of Tparm;
Tparamarr3 = array[0..2] of Tparm;
Tparamarr4 = array[0..3] of Tparm;
Tparamarr5 = array[0..4] of Tparm;
Tparamarr6 = array[0..5] of Tparm;
Tparamarr7 = array[0..6] of Tparm;
Tparamarr8 = array[0..7] of Tparm;





implementation



finalization ;

end.

