{$mode objfpc}
unit paswl_shell;

interface
uses 
  wl_basic_types,
  wl_core,
  wl_libwl,
  paswl_bindable,
  paswl_surface,
  paswl_shell_surface;



type

Tpaswl_shell=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  function get_shell_surface(surface : Tpaswl_surface; c : Tpaswl_shell_surface_cls) : Tpaswl_shell_surface;
  function get_shell_surface(surface : Tpaswl_surface) : Tpaswl_shell_surface;

  protected
  
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_shell_cls = class of Tpaswl_shell;




implementation




constructor Tpaswl_shell.create();
begin
  inherited create();
end;

destructor Tpaswl_shell.destroy();
begin
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_shell.onBind();
begin
  inherited;
  //wl_shell_add_listener(wlo,xxx,self);
end;

procedure Tpaswl_shell.onUnbind();
begin
  inherited;
end;


function Tpaswl_shell.get_shell_surface(surface : Tpaswl_surface; c : Tpaswl_shell_surface_cls) : Tpaswl_shell_surface;
var wo : wl_shell_surface;
begin
  wo := wl_shell_get_shell_surface(wlo, surface.wlo);
  get_shell_surface := Tpaswl_shell_surface(make_child(c,wo));
end;

function Tpaswl_shell.get_shell_surface(surface : Tpaswl_surface) : Tpaswl_shell_surface;
begin
  get_shell_surface := get_shell_surface(surface, Tpaswl_shell_surface);
end;


class function Tpaswl_shell.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_shell_interface;
end;

class function Tpaswl_shell.wl_ifver() : dword;
begin
  wl_ifver := 1;
end;


initialization
begin
end;

finalization
begin
end;

end.



