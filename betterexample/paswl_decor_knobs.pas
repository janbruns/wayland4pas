{$mode objfpc}
unit paswl_decor_knobs;

interface
uses 
  wl_basic_types,
  wl_core,
  wl_libwl,
  paswl_decor_border,
  paswl_shm_pool,
  paswl_buffer;

const
  paswl_decor_knob_state_normal = 0;
  paswl_decor_knob_state_hilited = 1;
  paswl_decor_knob_state_pushed = 2;
  paswl_decor_knob_state_disabled = 3;

type
Tpaswl_decor_knob = class;
Tknobcb = procedure(k : Tpaswl_decor_knob) of object;

Tcolarrarr = array[0..3] of Pdword;

Tpaswl_decor_knob = class
  public
  procedure paint(dst : T_paswl_buffer; x,y : longint); virtual;
  procedure load(src : T_paswl_buffer; dx,dy : longint);
  procedure setState(s : longint);
  procedure setBorder(bordersize_ : longint; ca : Pdword);
  procedure setBlend(ca : Pdword);
  destructor destroy(); override;
  constructor create();
  function pointWithinLastPaint(x,y : longint) : boolean;

  private
  procedure dontdoanything(what : Tpaswl_decor_knob);
  
  public
  onclick : Tknobcb;
  bp : Tborderpainter;
  buf,curbuf : Tpaswl_virtualbuffer;
  pool : Tpaswl_shm_managedpool;
  colarr : Pdword;
  colcnt : longint;
  blendcol : array[0..7] of dword;
  currentstate,lastx,lasty : longint;
end;



implementation
uses math,sysutils;


function Tpaswl_decor_knob.pointWithinLastPaint(x,y : longint) : boolean;
begin
  pointWithinLastPaint := false;
  if assigned(curbuf)and(x>=lastx)and(y>=lasty) then begin
    if (x<(lastx+curbuf.width))and(y<(lasty+curbuf.height))
    then pointWithinLastPaint := true;
  end;
end;

procedure Tpaswl_decor_knob.paint(dst : T_paswl_buffer; x,y : longint);
begin
  if assigned(curbuf) then begin
    dst.copyFrom(curbuf,0,0,curbuf.width,curbuf.height,x,y);
    lastx := x;
    lasty := y;
  end;
end;

procedure Tpaswl_decor_knob.load(src : T_paswl_buffer; dx,dy : longint);
begin
  if assigned(buf) then freeandnil(buf);
  buf := pool.create_buffer(dx,dy,4*dx,wl_shm_format_argb8888,Tpaswl_virtualbuffer) as Tpaswl_virtualbuffer;
  buf.copyFrom(src,0,0,dx,dy,0,0);
  if assigned(curbuf) then freeandnil(curbuf);
  setState(currentstate);
end;

procedure Tpaswl_decor_knob.setBorder(bordersize_ : longint; ca : Pdword);
begin
  if (colarr<>nil) then freemem(colarr,colcnt*4*sizeof(dword));
  colcnt := bordersize_;
  getmem(colarr,colcnt*4*sizeof(dword));
  move(ca[0],colarr[0],4*sizeof(dword)*colcnt);
end;

procedure Tpaswl_decor_knob.setBlend(ca : Pdword);
var i : longint;
begin
  for i := 0 to 7 do blendcol[i] := ca[i];
end;

procedure Tpaswl_decor_knob.setState(s : longint);
var x,y,dx,dy : longint; c,d : dword;
begin
  s := s and 3;
  currentstate := s;
  if not assigned(buf) then exit;
  dx := buf.width+2*colcnt;
  dy := buf.height+2*colcnt;
  if assigned(curbuf) then begin
    if (curbuf.width <>dx)
    or (curbuf.height<>dy)
    then freeandnil(curbuf);
  end;
  if not assigned(curbuf) then begin
    curbuf := pool.create_buffer(dx,dy,4*dx,wl_shm_format_argb8888,Tpaswl_virtualbuffer) as Tpaswl_virtualbuffer;
  end;
  bp.make(colcnt,@colarr[s*colcnt]);
  bp.repaint(curbuf,dx,dy);
  for y := 0 to buf.height-1 do begin
    for x := 0 to buf.width-1 do begin
      c := buf.getPixel(x,y);
      d := blendcol[s*2+((x+y) and 1)];
      c := colinterpol(c,d,(d shr 24)/255);  // from paswl_buffer.pas
      c := c or $FF000000;
      curbuf.setPixel(x+colcnt,y+colcnt,c);
    end;
  end;
end;

destructor Tpaswl_decor_knob.destroy(); 
begin
  bp.destroy;
  freemem(colarr,4*sizeof(dword)*colcnt);
  freeandnil(buf);
  freeandnil(curbuf);
end;

constructor Tpaswl_decor_knob.create();
begin
  colcnt := 0;
  bp := Tborderpainter.create;
  onclick := @self.dontdoanything;
end;

procedure Tpaswl_decor_knob.dontdoanything(what : Tpaswl_decor_knob);
begin
end;



begin
end.
