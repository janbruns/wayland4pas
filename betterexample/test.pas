{$mode objfpc}
uses minifont,paswl_collection, paswl_collection2, wl_libwl, wl_core,wl_basic_types,

paswl_shm_pool,
paswl_buffer,
sysutils,
paswl_bindable,
paswl_seat,
paswl_pointer,
paswl_surface,
paswl_subsurface,
paswl_xdg_wm_base,
paswl_xdg_surface,
paswl_xdg_toplevel,
paswl_xdg_ext1,
paswl_shell,
paswl_shell_surface,
paswl_decor_border,
paswl_decor_knobs,
paswl_decor,
math;




type

Txdgdecor=class;
Twlshelldecor=class;

Tdecorxdgmainsurf = class(Tpaswl_xdg_surface)
  protected procedure cb_xdg_surface_configure(serial : dword); override;
  public decor : Txdgdecor;
end;
Txdgdecor=class(Tdecor)
  public
  procedure initial_settings(); override;
  procedure init(); override;
  procedure check_states();
  procedure onConfigure(c : Tpaswl_xdg_surface; serial : dword);
  procedure setgeo(); override;
  procedure doack(var q : qword); override;
  destructor destroy(); override;
  procedure onMaximizeButton(k : Tpaswl_decor_knob); override; 
  procedure addStatereq(serial : dword; var st : TXDGstatereq);
  procedure start_resize(seat : Tpaswl_seat; serial,where : dword); override;
  procedure start_move(seat : Tpaswl_seat; serial : dword); override;
  procedure onFrame(d : dword); override;

  public
  maintoplevel : Txdgtoplevel_with_states;
  mainxdg : Tdecorxdgmainsurf;
  reqstate : TXDGstatereq;
end;

Twlshellmainsurf = class(Tpaswl_shell_surface)
  protected procedure cb_shell_surface_configure(edges : dword; width, height : longint); override;
  public decor : Twlshelldecor;
end;
Twlshelldecor=class(Tdecor)
  public
  procedure initial_settings(); override;
  procedure init(); override;
  procedure setgeo(); override;
  procedure doack(var q : qword); override;
  destructor destroy(); override;

  procedure onMaximizeButton(k : Tpaswl_decor_knob); override; 
  procedure onConfigure(edges : dword; width, height : longint);
  procedure start_resize(seat : Tpaswl_seat; serial,where : dword); override;
  procedure start_move(seat : Tpaswl_seat; serial : dword); override;
  procedure onFrame(d : dword); override;
  procedure onEnter(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed); override;

  public
  mainshellsurf : Twlshellmainsurf;
  use_wlshell_resizeworkaround : boolean;
end;




procedure Txdgdecor.check_states();
var didresize : boolean;
begin
  didresize := false;
  
  if reqstate.close then clc.alldone := true;
  if reqstate.resize <>state_resizeing then begin
    state_resizeing := not state_resizeing;
    redrawwhat := redrawwhat or redraw_titlebar_bar;
    if not state_resizeing then redrawwhat := redrawwhat or redraw_doneresize;
  end;
  if reqstate.activated<>state_activated then begin
    redrawwhat := redrawwhat or redraw_titlebar_bar;
    state_activated := not state_activated;
  end;
  if reqstate.maximized<>state_maximized then begin
    state_maximized := not state_maximized;
    redrawwhat := redrawwhat or redraw_resize or redraw_doneresize;;
    if not state_maximized then begin
      reqstate.width := stored_width;
      reqstate.height := stored_height;
    end else begin
      stored_width := planwidth;
      stored_height := planheight;
    end;
  end else;

  if reqstate.width<>0 then begin
    if planwidth<>reqstate.width then begin
      redrawwhat := redrawwhat or redraw_resize;
      planwidth := min(maxwidth,max(minwidth,reqstate.width));
      reqstate.width := 0;
      didresize := true;
    end;
  end;
  if reqstate.height<>0 then begin
    if planheight<>reqstate.height then begin
      redrawwhat := redrawwhat or redraw_resize;
      planheight := min(maxheight,max(minheight,reqstate.height));
      reqstate.height := 0;
      didresize := true;
    end;
  end;
  if didresize then prepare_new_size();
end;

procedure Txdgdecor.addStatereq(serial : dword; var st : TXDGstatereq);
begin
  reqserial := serial;
  reqstate.close := reqstate.close or st.close;
  reqstate.maximized := st.maximized;
  reqstate.fullscreen:= st.fullscreen;
  reqstate.resize    := st.resize;
  reqstate.activated := st.activated;
  reqstate.suspended := st.suspended;
  if st.width<>0  then reqstate.width  := st.width;
  if st.height<>0 then reqstate.height := st.height;
  st.width := 0;
  st.height := 0;
end;



procedure Txdgdecor.onConfigure(c : Tpaswl_xdg_surface; serial : dword);
begin
  if c=mainxdg then begin
  //writeln('onconfigure',serial);
    addStatereq(serial, maintoplevel.reqstate);
    request_draw_frame();
  end;
end;

procedure Txdgdecor.onMaximizeButton(k : Tpaswl_decor_knob);
begin
  writeln('Txdgdecor.onMaximizeButton');
  if state_maximized 
  then maintoplevel.unset_maximized()
  else maintoplevel.set_maximized();
end;

procedure Txdgdecor.onFrame(d : dword);
begin
  if reqserial<>invalid_serial then begin
    mainxdg.ack_configure(reqserial);
    reqserial := invalid_serial;
    check_states();
  end;
  redraw();
  redrawwhat := 0;
  init_XDGreq_states(reqstate);
  //request_draw_frame();
end;


procedure Txdgdecor.initial_settings();
begin
  inherited;
  
  name := 'Nothing as emptyness';
  appid := 'app.example.gui.wlxdg';
end;









procedure Txdgdecor.init();
begin
//  freeandnil(clc.subcompositor);
//  freeandnil(clc.viewporter);
  initial_settings();

  mainsurf := clc.compositor.create_surface(Tpaswl_surface) as Tpaswl_surface;
  mainxdg := clc.xdg_wm_base.get_xdg_surface(mainsurf,Tdecorxdgmainsurf) as Tdecorxdgmainsurf;
  maintoplevel := mainxdg.get_toplevel(Txdgtoplevel_with_states) as Txdgtoplevel_with_states;
  mainsurf.commit();
  while not mainxdg.seen_configure do clc.display.display_roundtrip();
  

  if assigned(clc.subcompositor) then begin
    titlesurf := clc.compositor.create_surface(Tpaswl_surface) as Tpaswl_surface;
    titlesub  := clc.subcompositor.get_subsurface(titlesurf,mainsurf,Tpaswl_subsurface);
  end;
  pool := clc.shm.create_managed_pool(1024);
  make_fallbackcursor();
  mouse := clc.seat.get_pointer(Tdecormouse) as Tdecormouse;
  if assigned(mouse) then mouse.decor := self;

  make_mainbuf();
  mainsurf.attach(cbuf);
  
  draw_titletext();
  if assigned(titlesurf) then titlesurf.commit();
  make_border();
  makeknobs();

  while not mainxdg.seen_configure do clc.display.display_roundtrip();
  mainxdg.set_window_geometry(0,0,planwidth,planheight);
  maintoplevel.set_min_size(minwidth,minheight);
  maintoplevel.set_max_size(maxwidth,maxheight);
  maintoplevel.set_title(@name[1]);
  maintoplevel.set_app_id(@appid[1]);
  mainxdg.ack_configure(mainxdg.consume_configure());
  request_draw_frame();
  mainsurf.commit();
  mainxdg.decor := self;
  redrawwhat := redrawwhat or redraw_titlebar_but or redraw_resize or redraw_doneresize;
end;



procedure Txdgdecor.setgeo();
begin
  mainxdg.set_window_geometry(0,0,planwidth,planheight);
end;

procedure Txdgdecor.doack(var q : qword);
begin
  if (q<>invalid_serial) then mainxdg.ack_configure(q);
end;



procedure Txdgdecor.start_resize(seat : Tpaswl_seat; serial,where : dword);
begin
  maintoplevel.resize(seat, serial, where);
end;

procedure Txdgdecor.start_move(seat : Tpaswl_seat; serial : dword);
begin
  maintoplevel.move(seat, serial);
end;



destructor Txdgdecor.destroy();
begin
  inherited;
  freeandnil(maintoplevel);
  freeandnil(mainxdg);
end;



procedure Tdecorxdgmainsurf.cb_xdg_surface_configure(serial : dword);
begin
  if assigned(decor) then decor.onConfigure(self,serial) else inherited;
end;








procedure Twlshelldecor.initial_settings();
begin
  inherited;
  name := 'Nothing as emptyness';
  appid := 'app.example.gui.wlshell';
  use_wlshell_resizeworkaround := true; {weston}
end;

procedure Twlshelldecor.init();
begin
//  freeandnil(clc.subcompositor);
//  freeandnil(clc.viewporter);
  initial_settings();
  mainsurf := clc.compositor.create_surface(Tpaswl_surface) as Tpaswl_surface;
  mainshellsurf := clc.coreshell.get_shell_surface(mainsurf,Twlshellmainsurf) as Twlshellmainsurf;
  mainshellsurf.decor := self;
  writeln('mainshellsurf=',hexstr(ptruint(mainshellsurf),16));
  writeln('mainshellsurf.wlo=',hexstr(ptruint(mainshellsurf.wlo),16));

  mainsurf.commit();
  clc.display.display_roundtrip();  

  if assigned(clc.subcompositor) then begin
    titlesurf := clc.compositor.create_surface(Tpaswl_surface) as Tpaswl_surface;
    titlesub  := clc.subcompositor.get_subsurface(titlesurf,mainsurf,Tpaswl_subsurface);
  end;
  pool := clc.shm.create_managed_pool(1024);
  make_fallbackcursor();
  mouse := clc.seat.get_pointer(Tdecormouse) as Tdecormouse;
  if assigned(mouse) then mouse.decor := self;

  make_mainbuf();
  mainsurf.attach(cbuf);
  
  draw_titletext();
  if assigned(titlesurf) then titlesurf.commit();
  make_border();
  makeknobs();

  clc.display.display_roundtrip();
  mainshellsurf.set_title(@name[1]);
  mainshellsurf.set_class(@appid[1]);
  mainshellsurf.set_toplevel();
  
  request_draw_frame();
  redrawwhat := redrawwhat or redraw_titlebar_but or redraw_resize or redraw_doneresize;
  mainsurf.commit();
end;

procedure Twlshelldecor.setgeo();
begin
end;

procedure Twlshelldecor.doack(var q : qword);
begin
end;

destructor Twlshelldecor.destroy();
begin
  freeandnil(mainshellsurf);
  inherited;
end;

procedure Twlshelldecor.onMaximizeButton(k : Tpaswl_decor_knob);
begin
  writeln('Twlshelldecor.onMaximizeButton');
  if state_maximized then begin
    mainshellsurf.set_toplevel();
    planwidth := stored_width;
    planheight := stored_height;
    state_maximized := false;
    prepare_new_size();
    redrawwhat := redrawwhat or redraw_resize or redraw_doneresize;
    request_draw_frame();
    mainsurf.commit();
  end else begin
    stored_width := planwidth;
    stored_height := planheight;
    mainshellsurf.set_maximized(nil);
    state_maximized := true;
    redrawwhat := redrawwhat or redraw_resize or redraw_doneresize;
  end;
end;

procedure Twlshelldecor.start_resize(seat : Tpaswl_seat; serial,where : dword);
begin
  state_resizeing := true;
  writeln('Start resize');
  mainshellsurf.resize(seat, serial, where);
end;

procedure Twlshelldecor.start_move(seat : Tpaswl_seat; serial : dword);
begin
  mainshellsurf.move(seat, serial);
end;

procedure Twlshelldecor.onFrame(d : dword);
begin
  redraw();
  redrawwhat := 0;
end;

procedure Twlshelldecor.onEnter(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed);
begin
  inherited;
  if state_resizeing and use_wlshell_resizeworkaround then begin
    state_resizeing := false;
    redrawwhat := redrawwhat or redraw_resize or redraw_doneresize;
    request_draw_frame();
    mainsurf.commit();
    writeln('Done resize');
  end;
end;

procedure Twlshelldecor.onConfigure(edges : dword; width, height : longint);
var sc : boolean;
begin
  sc := false;
  if (width>0)and(width<>planwidth) then begin
    planwidth := min(maxwidth,max(minwidth,width));
    sc := true;
  end;
  if (height>0)and(height<>planheight) then begin
    planheight := min(maxheight,max(minheight,height));
    sc := true;
  end;
  if sc or state_resizeing then begin
    prepare_new_size();
    redrawwhat := redrawwhat or redraw_resize;
    if (edges=0)and(not(use_wlshell_resizeworkaround)) then begin
      state_resizeing := false;
      redrawwhat := redrawwhat or redraw_doneresize;
      writeln('Done resize');
    end;
    request_draw_frame();
    mainsurf.commit();
  end;
end;

procedure Twlshellmainsurf.cb_shell_surface_configure(edges : dword; width, height : longint);
begin
  if assigned(decor) then decor.onConfigure(edges, width, height) else inherited;
end;



type
TinnerExampleApp = class
  public
  pool : Tpaswl_shm_managedpool;
  buf : Tpaswl_buffer;
  surf : Tpaswl_surface;
  subs : Tpaswl_subsurface;
  decor : Tdecor;
  cnt : longint;
  public
  procedure take_decor(d : Tdecor);
  function Render(b : T_paswl_buffer; x,y,dx,dy,reason : longint) : boolean;
  destructor destroy(); override;
end;
procedure TinnerExampleApp.take_decor(d : Tdecor);
begin
  decor := d;
  pool := d.clc.shm.create_managed_pool(512);
  buf := pool.create_buffer(288,32,4*288,0,Tpaswl_buffer) as Tpaswl_buffer;
  if assigned(d.clc.subcompositor) then begin
    surf := d.clc.compositor.create_surface(Tpaswl_surface);
    subs := d.clc.subcompositor.get_subsurface(surf,d.mainsurf);
  end;
  d.HonRender := @self.Render;
end;
function TinnerExampleApp.Render(b : T_paswl_buffer; x,y,dx,dy,reason : longint) : boolean;
begin
  Render := true;  cnt+=1;
  buf.fillColor($ffffffff,$ff8040ff,$ff404040,$00000000);
  paint_string_to_buffer(buf,0,0,4,4,0,hexstr(dx,4)+'_'+hexstr(cnt,4),$ffc0c0c0);
  if assigned(surf) then begin
    subs.set_position(x,y);
    surf.attach(buf);
    surf.damage(0,0,288,32);
    surf.commit();
  end else begin
    b.copyFrom(buf,0,0,288,32,0,0);
  end;
end;
destructor TinnerExampleApp.destroy();
begin
  freeandnil(subs);
  freeandnil(surf);
  freeandnil(buf);
  freeandnil(pool);
end;

var
o : Tpaswl_collection2;
deco : Tdecor;
ieapp : TinnerExampleApp;

begin 
  import_libwlc();
  o := startup_display(argv[1], Tpaswl_collection2) as Tpaswl_collection2;
  if (o<>nil) then begin
    o.display.display_roundtrip();
    if o.testAutobinds() then begin
      deco := nil;
//freeandnil(o.xdg_wm_base);
      if assigned(o.xdg_wm_base) then begin
        writeln('using xdg-shell');
        deco := Txdgdecor.create()
      end else if assigned(o.coreshell) then begin
        writeln('using wl-shell');
        deco := Twlshelldecor.create();
      end;
      if assigned(deco) then begin
        deco.clc := o;
        deco.init();
        o.display.display_roundtrip();
        writeln('enter msgloop');
        ieapp := TinnerExampleApp.create();
        ieapp.take_decor(deco);
        o.msgloop();
        deco.destroy();
        ieapp.destroy();
        writeln;
      end else writeln('Sorry, no supported shell (currently: core wl_shell + xdg-shell) found.');
    end else begin
      writeln('Insufficient set of available wayland interfaces, sorry.');
    end;
    o.destroy();
  end else writeln('no connected Tpaswl');
end.
