{$mode objfpc}
unit paswl_surface;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_bindable,paswl_buffer;


type

Tpaswl_surface=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;


  procedure set_opaque_region(region : Tpaswl_bindable);
  procedure set_input_region(region : Tpaswl_bindable);
  procedure set_buffer_transform(transform : longint);
  procedure set_buffer_scale(s : longint);
  procedure damage_buffer(x,y,w,h : longint);
  procedure attach(b : T_paswl_buffer; x : longint = 0; y : longint = 0);
  procedure damage(x,y,w,h : longint);
  function frame() : wl_callback;
  procedure commit();

  protected
  procedure cb_surface_enter(); virtual;
  procedure cb_surface_leave(); virtual;
  procedure cb_surface_preferred_buffer_scale(factor : longint); virtual;
  procedure cb_surface_preferred_buffer_transform(transform : dword); virtual;

  public
  parent : Tpaswl_surface;
  lastattached : T_paswl_buffer;
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
  
end;

Tpaswl_surface_cls = class of Tpaswl_surface;





implementation
uses sysutils;


procedure surface_enter(dat:pointer; o:wl_surface; ot : wl_output); cdecl;
var d : Tpaswl_surface;
begin
  if dat<>nil then begin
    d := Tpaswl_surface(dat);
    if d.wlo=o then d.cb_surface_enter()
    else ;//
  end;
end;

procedure surface_leave(dat:pointer; o:wl_surface; ot : wl_output); cdecl;
var d : Tpaswl_surface;
begin
  if dat<>nil then begin
    d := Tpaswl_surface(dat);
    if d.wlo=o then d.cb_surface_enter()
    else ;//
  end;
end;

procedure surface_preferred_buffer_scale(dat:pointer; o:wl_surface; factor : longint); cdecl;
var d : Tpaswl_surface;
begin
  if dat<>nil then begin
    d := Tpaswl_surface(dat);
    if d.wlo=o then d.cb_surface_preferred_buffer_scale(factor)
    else ;//
  end;
end;

procedure surface_preferred_buffer_transform(dat:pointer; o:wl_surface; transform : dword); cdecl;
var d : Tpaswl_surface;
begin
  if dat<>nil then begin
    d := Tpaswl_surface(dat);
    if d.wlo=o then d.cb_surface_preferred_buffer_transform(transform)
    else ;//
  end;
end;



var
imp_if_surface_listener : wl_surface_listener = (
  enter : @surface_enter;
  leave : @surface_leave;
  preferred_buffer_scale : @surface_preferred_buffer_scale;
  preferred_buffer_transform : @surface_preferred_buffer_transform;
);





procedure Tpaswl_surface.cb_surface_enter();
begin
end;

procedure Tpaswl_surface.cb_surface_leave();
begin
end;

procedure Tpaswl_surface.cb_surface_preferred_buffer_scale(factor : longint);
begin
end;

procedure Tpaswl_surface.cb_surface_preferred_buffer_transform(transform : dword);
begin
end;



constructor Tpaswl_surface.create();
begin
  inherited create();
end;

destructor Tpaswl_surface.destroy();
begin
  wl_surface_destroy(wlo);
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_surface.onBind();
begin
  inherited;
  wl_surface_add_listener(wl_surface(wlo),imp_if_surface_listener,self);
end;

procedure Tpaswl_surface.onUnbind();
begin
  inherited;
end;





procedure Tpaswl_surface.set_opaque_region(region : Tpaswl_bindable);
begin
  wl_surface_set_opaque_region(wl_surface(wlo), wl_region(region.wlo) );
end;

procedure Tpaswl_surface.set_input_region(region : Tpaswl_bindable);
begin
  wl_surface_set_input_region(wl_surface(wlo), wl_region(region.wlo) );
end;

procedure Tpaswl_surface.set_buffer_transform(transform : longint);
begin
  wl_surface_set_buffer_transform(wl_surface(wlo), transform);
end;

procedure Tpaswl_surface.set_buffer_scale(s : longint);
begin
  wl_surface_set_buffer_scale(wl_surface(wlo), s);
end;

procedure Tpaswl_surface.damage_buffer(x,y,w,h : longint);
begin
  wl_surface_damage_buffer(wl_surface(wlo), x,y,w,h);
end;



procedure Tpaswl_surface.attach(b : T_paswl_buffer;  x : longint = 0; y : longint = 0);
begin

  if assigned(b) then begin
  { additional b.lastattach<>self test typically not needed.
    Might be necessary to add for compositors that release buffers lately.
    On the other hand, it's sometimes an indication of misuse, if the stronger
    test fails, so I leave the final test commented out here for now.
  } // or maybe opted other way round
    if (b<>lastattached) and b.busy 
{$ifndef debug}
      and (b.lastattach<>self)
{$endif}
    then begin 
      raise Exception.create('attempt to attach a busy buffer');
    end;
    if assigned(lastattached) then begin
      if lastattached.cb_buffer_release() then lastattached.destroy();
    end;
    lastattached := b;
    if assigned(b) then begin
      b.busy := true;
      b.lastattach := self;
    end;
    wl_surface_attach(wl_surface(wlo), wl_buffer(b.wlo), x,y );
  end else wl_surface_attach(wl_surface(wlo), nil, x,y );;
  
end;

procedure Tpaswl_surface.damage(x,y,w,h : longint);
begin
  wl_surface_damage(wl_surface(wlo), x,y,w,h );
end;

function Tpaswl_surface.frame() : wl_callback;
begin
  frame := wl_surface_frame(wl_surface(wlo));
end;

procedure Tpaswl_surface.commit();
begin
  lastattached := nil;
  wl_surface_commit(wl_surface(wlo));
end;




class function Tpaswl_surface.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_surface_interface;
end;

class function Tpaswl_surface.wl_ifver() : dword;
begin
  wl_ifver := 6;
end;


initialization
begin
end;

finalization
begin
end;

end.



