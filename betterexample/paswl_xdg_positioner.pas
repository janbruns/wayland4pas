{$mode objfpc}
unit paswl_xdg_positioner;

interface
uses 
  wl_basic_types,
  wl_core,
  wl_libwl,
  wl_xdgshell,
  paswl_bindable,
  paswl_seat;


type

Tpaswl_xdg_positioner=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  procedure set_size(width, height : longint);
  procedure set_anchor_rect(x, y, width, height : longint);
  procedure set_anchor(anchor : dword);
  procedure set_gravity(gravity : dword);
  procedure set_constraint_adjustment(constraint_adjustment : dword);
  procedure set_offset(x, y : longint);
  procedure set_reactive();
  procedure set_parent_size(parent_width, parent_height : longint);
  procedure set_parent_configure(serial : dword);



  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_xdg_positioner_cls = class of Tpaswl_xdg_positioner;


Tpaswl_xdg_positioner_dbg = class(Tpaswl_xdg_positioner)
end;




implementation






procedure Tpaswl_xdg_positioner.set_size(width, height : longint);
begin
  xdg_positioner_set_size(xdg_positioner(wlo),width, height);
end;

procedure Tpaswl_xdg_positioner.set_anchor_rect(x, y, width, height : longint);
begin
  xdg_positioner_set_anchor_rect(xdg_positioner(wlo),x,y,width,height);
end;

procedure Tpaswl_xdg_positioner.set_anchor(anchor : dword);
begin
  xdg_positioner_set_anchor(xdg_positioner(wlo),anchor);
end;

procedure Tpaswl_xdg_positioner.set_gravity(gravity : dword);
begin
  xdg_positioner_set_gravity(xdg_positioner(wlo),gravity);
end;

procedure Tpaswl_xdg_positioner.set_constraint_adjustment(constraint_adjustment : dword);
begin
  xdg_positioner_set_constraint_adjustment(xdg_positioner(wlo),constraint_adjustment);
end;

procedure Tpaswl_xdg_positioner.set_offset(x, y : longint);
begin
  xdg_positioner_set_offset(xdg_positioner(wlo),x,y);
end;

procedure Tpaswl_xdg_positioner.set_reactive();
begin
  xdg_positioner_set_reactive(xdg_positioner(wlo));
end;

procedure Tpaswl_xdg_positioner.set_parent_size(parent_width, parent_height : longint);
begin
  xdg_positioner_set_parent_size(xdg_positioner(wlo),parent_width, parent_height);
end;

procedure Tpaswl_xdg_positioner.set_parent_configure(serial : dword);
begin
  xdg_positioner_set_parent_configure(xdg_positioner(wlo),serial);
end;






constructor Tpaswl_xdg_positioner.create();
begin
  inherited create();
end;

destructor Tpaswl_xdg_positioner.destroy();
begin
  xdg_positioner_destroy(xdg_positioner(wlo));
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_xdg_positioner.onBind();
begin
  inherited;
end;

procedure Tpaswl_xdg_positioner.onUnbind();
begin
  inherited;
end;

class function Tpaswl_xdg_positioner.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := xdg_positioner_interface;
end;

class function Tpaswl_xdg_positioner.wl_ifver() : dword;
begin
  wl_ifver := 6;
end;


initialization
begin
end;

finalization
begin
end;

end.



