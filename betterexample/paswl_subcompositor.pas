{$mode objfpc}
unit paswl_subcompositor;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_surface,paswl_subsurface, paswl_bindable;


type

Tpaswl_subcompositor=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  function get_subsurface(surf,par : Tpaswl_surface) : Tpaswl_subsurface;
  function get_subsurface(surf,par : Tpaswl_surface; c : Tpaswl_subsurface_cls) : Tpaswl_subsurface;
  
 
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_subcompositor_cls = class of Tpaswl_subcompositor;







implementation



constructor Tpaswl_subcompositor.create();
begin
  inherited create();
end;

destructor Tpaswl_subcompositor.destroy();
begin
  wl_subcompositor_destroy(wlo);
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_subcompositor.onBind();
begin
  inherited;
end;

procedure Tpaswl_subcompositor.onUnbind();
begin
  inherited;
end;

function Tpaswl_subcompositor.get_subsurface(surf,par : Tpaswl_surface) : Tpaswl_subsurface;
begin
  get_subsurface := get_subsurface(surf,par,Tpaswl_subsurface);
end;

function Tpaswl_subcompositor.get_subsurface(surf,par : Tpaswl_surface; c : Tpaswl_subsurface_cls) : Tpaswl_subsurface;
var wo : wl_subsurface; o : Tpaswl_subsurface;
begin  
  wo := wl_subcompositor_get_subsurface(wlo, surf.wlo, par.wlo);
  o := Tpaswl_subsurface(make_child(c,wo));
  if o<>nil then begin
    o.surface := surf;
    surf.parent := par;
  end;
  get_subsurface := o;
end;


class function Tpaswl_subcompositor.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_subcompositor_interface;
end;

class function Tpaswl_subcompositor.wl_ifver() : dword;
begin
  wl_ifver := 1;
end;


initialization
begin
end;

finalization
begin
end;

end.



