{$mode objfpc}
unit paswl_xdg_toplevel;

interface
uses 
  wl_basic_types,
  wl_core,
  wl_libwl,
  wl_xdgshell,
  paswl_bindable,
  paswl_output,
  paswl_seat;




type

Tpaswl_xdg_toplevel=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;


  procedure set_parent(parent : Tpaswl_xdg_toplevel);
  procedure set_title(title : Pchar);
  procedure set_app_id(app_id : Pchar);
  procedure show_window_menu(seat : Tpaswl_seat; serial : dword; x, y : longint);
  procedure move(seat : Tpaswl_seat; serial : dword);
  procedure resize(seat : Tpaswl_seat; serial, edges : dword);
  procedure set_max_size(width, height : longint);
  procedure set_min_size(width, height : longint);
  procedure set_maximized();
  procedure unset_maximized();
  procedure set_fullscreen(o : Tpaswl_output);
  procedure unset_fullscreen();
  procedure set_minimized();


  protected
  procedure cb_xdg_toplevel_configure(width, height : longint; states : wl_array); virtual;
  procedure cb_xdg_toplevel_close(); virtual;
  procedure cb_xdg_toplevel_configure_bounds(width, height : longint); virtual;
  procedure cb_xdg_toplevel_wm_capabilities(capabilities : wl_array); virtual;

  
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_xdg_toplevel_cls = class of Tpaswl_xdg_toplevel;






implementation




procedure xdg_toplevel_configure(dat:pointer; o:xdg_toplevel; width, height : longint; states : wl_array); cdecl;
var d : Tpaswl_xdg_toplevel;
begin
  if dat<>nil then begin
    d := Tpaswl_xdg_toplevel(dat);
    if d.wlo=o then d.cb_xdg_toplevel_configure(width, height, states)
    else ;//
  end;
end;

procedure xdg_toplevel_close(dat:pointer; o:xdg_toplevel); cdecl;
var d : Tpaswl_xdg_toplevel;
begin
  if dat<>nil then begin
    d := Tpaswl_xdg_toplevel(dat);
    if d.wlo=o then d.cb_xdg_toplevel_close()
    else ;//
  end;
end;

procedure xdg_toplevel_configure_bounds(dat:pointer; o:xdg_toplevel; width, height : longint); cdecl;
var d : Tpaswl_xdg_toplevel;
begin
  if dat<>nil then begin
    d := Tpaswl_xdg_toplevel(dat);
    if d.wlo=o then d.cb_xdg_toplevel_configure_bounds(width, height)
    else ;//
  end;
end;

procedure xdg_toplevel_wm_capabilities(dat:pointer; o:xdg_toplevel; capabilities : wl_array); cdecl;
var d : Tpaswl_xdg_toplevel;
begin
  if dat<>nil then begin
    d := Tpaswl_xdg_toplevel(dat);
    if d.wlo=o then d.cb_xdg_toplevel_wm_capabilities(capabilities)
    else ;//
  end;
end;

var
imp_if_xdg_toplevel_listener : xdg_toplevel_listener = (
  configure : @xdg_toplevel_configure;
  close : @xdg_toplevel_close;
  configure_bounds : @xdg_toplevel_configure_bounds;
  wm_capabilities : @xdg_toplevel_wm_capabilities;
);






procedure Tpaswl_xdg_toplevel.cb_xdg_toplevel_configure(width, height : longint; states : wl_array);
begin
writeln('TOPLEVEL: configure');
end;
procedure Tpaswl_xdg_toplevel.cb_xdg_toplevel_close();
begin
writeln('TOPLEVEL: close');
end;
procedure Tpaswl_xdg_toplevel.cb_xdg_toplevel_configure_bounds(width, height : longint);
begin
writeln('TOPLEVEL: configure bounds:',width,'/',height);
end;
procedure Tpaswl_xdg_toplevel.cb_xdg_toplevel_wm_capabilities(capabilities : wl_array);
begin
writeln('TOPLEVEL: caps');
end;








procedure Tpaswl_xdg_toplevel.set_parent(parent : Tpaswl_xdg_toplevel);
var wo : xdg_toplevel;
begin
  wo := nil;
  if parent<>nil then wo := xdg_toplevel(parent.wlo);
  xdg_toplevel_set_parent(xdg_toplevel(wlo),wo);
end;

procedure Tpaswl_xdg_toplevel.set_title(title : Pchar);
begin
  xdg_toplevel_set_title(xdg_toplevel(wlo),title);
end;

procedure Tpaswl_xdg_toplevel.set_app_id(app_id : Pchar);
begin
  xdg_toplevel_set_app_id(xdg_toplevel(wlo),app_id);
end;

procedure Tpaswl_xdg_toplevel.show_window_menu(seat : Tpaswl_seat; serial : dword; x, y : longint);
begin
  xdg_toplevel_show_window_menu(xdg_toplevel(wlo),wl_seat(seat.wlo),serial,x,y);
end;

procedure Tpaswl_xdg_toplevel.move(seat : Tpaswl_seat; serial : dword);
begin
  xdg_toplevel_move(xdg_toplevel(wlo),wl_seat(seat.wlo),serial);
end;

procedure Tpaswl_xdg_toplevel.resize(seat : Tpaswl_seat; serial, edges : dword);
begin
  xdg_toplevel_resize(xdg_toplevel(wlo),wl_seat(seat.wlo),serial,edges);
end;

procedure Tpaswl_xdg_toplevel.set_max_size(width, height : longint);
begin
  xdg_toplevel_set_max_size(xdg_toplevel(wlo),width, height);
end;

procedure Tpaswl_xdg_toplevel.set_min_size(width, height : longint);
begin
  xdg_toplevel_set_min_size(xdg_toplevel(wlo),width, height);
end;

procedure Tpaswl_xdg_toplevel.set_maximized();
begin
  xdg_toplevel_set_maximized(xdg_toplevel(wlo));
end;

procedure Tpaswl_xdg_toplevel.unset_maximized();
begin
  xdg_toplevel_unset_maximized(xdg_toplevel(wlo));
end;

procedure Tpaswl_xdg_toplevel.set_fullscreen(o : Tpaswl_output);
begin
  xdg_toplevel_set_fullscreen(xdg_toplevel(wlo),wl_output(o.wlo));
end;

procedure Tpaswl_xdg_toplevel.unset_fullscreen();
begin
  xdg_toplevel_unset_fullscreen(xdg_toplevel(wlo));
end;

procedure Tpaswl_xdg_toplevel.set_minimized();
begin
  xdg_toplevel_set_minimized(xdg_toplevel(wlo));
end;









constructor Tpaswl_xdg_toplevel.create();
begin
  inherited create();
end;

destructor Tpaswl_xdg_toplevel.destroy();
begin
  xdg_toplevel_destroy(xdg_toplevel(wlo));
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_xdg_toplevel.onBind();
begin
  inherited;
  xdg_toplevel_add_listener(xdg_toplevel(wlo),imp_if_xdg_toplevel_listener,self);
end;

procedure Tpaswl_xdg_toplevel.onUnbind();
begin
  inherited;
end;

class function Tpaswl_xdg_toplevel.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := xdg_toplevel_interface;
end;

class function Tpaswl_xdg_toplevel.wl_ifver() : dword;
begin
  wl_ifver := 6;
end;


initialization
begin
end;

finalization
begin
end;

end.



