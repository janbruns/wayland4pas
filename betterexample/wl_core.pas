{$mode objfpc}
unit wl_core; 
{ modified version:
  use less type-safe wl_object instead of specific classes for wl_objects
  to make the pas obj wrapping handle that same thing easier
}


{$PACKRECORDS C}


interface
uses wl_basic_types,wl_libwl;


type
//wl_display = class(wl_object) end;
wl_registry = wl_object;
wl_callback = wl_object;
wl_compositor = wl_object;
wl_shm_pool = wl_object;
wl_shm = wl_object;
wl_buffer = wl_object;
wl_data_offer = wl_object;
wl_data_source = wl_object;
wl_data_device = wl_object;
wl_data_device_manager = wl_object;
wl_shell = wl_object;
wl_shell_surface = wl_object;
wl_surface = wl_object;
wl_seat = wl_object;
wl_pointer = wl_object;
wl_keyboard = wl_object;
wl_touch = wl_object;
wl_output = wl_object;
wl_region = wl_object;
wl_subcompositor = wl_object;
wl_subsurface = wl_object;



var
wl_display_interface : Pwl_interface;
wl_registry_interface : Pwl_interface;
wl_callback_interface : Pwl_interface;
wl_compositor_interface : Pwl_interface;
wl_shm_pool_interface : Pwl_interface;
wl_shm_interface : Pwl_interface;
wl_buffer_interface : Pwl_interface;
wl_data_offer_interface : Pwl_interface;
wl_data_source_interface : Pwl_interface;
wl_data_device_interface : Pwl_interface;
wl_data_device_manager_interface : Pwl_interface;
wl_shell_interface : Pwl_interface;
wl_shell_surface_interface : Pwl_interface;
wl_surface_interface : Pwl_interface;
wl_seat_interface : Pwl_interface;
wl_pointer_interface : Pwl_interface;
wl_keyboard_interface : Pwl_interface;
wl_touch_interface : Pwl_interface;
wl_output_interface : Pwl_interface;
wl_region_interface : Pwl_interface;
wl_subcompositor_interface : Pwl_interface;
wl_subsurface_interface : Pwl_interface;

function wl_display_sync(o : wl_display) : wl_callback;
function wl_display_get_registry(o : wl_display) : wl_registry;
function wl_registry_bind(o : wl_registry; intf : Pwl_interface; version : dword; name : dword) : wl_object;
function wl_compositor_create_surface(o : wl_compositor) : wl_surface;
function wl_compositor_create_region(o : wl_compositor) : wl_region;
function wl_shm_pool_create_buffer(o : wl_shm_pool; offset, width, height, stride : longint; format : dword) : wl_buffer;
procedure wl_shm_pool_destroy(o : wl_shm_pool);
procedure wl_shm_pool_resize(o : wl_shm_pool; size : longint);
function wl_shm_create_pool(o : wl_shm; fd : Tcint; size : longint) : wl_shm_pool;
procedure wl_buffer_destroy(o : wl_buffer);
procedure wl_data_offer_accept(o : wl_data_offer; serial : dword; mime_type : Pchar);
procedure wl_data_offer_receive(o : wl_data_offer; mime_type : Pchar; fd : Tcint);
procedure wl_data_offer_destroy(o : wl_data_offer);
procedure wl_data_offer_finish(o : wl_data_offer);
procedure wl_data_offer_set_actions(o : wl_data_offer; dnd_actions, preferred_action : dword);
procedure wl_data_source_offer(o : wl_data_source; mime_type : Pchar);
procedure wl_data_source_destroy(o : wl_data_source);
procedure wl_data_source_set_actions(o : wl_data_source; dnd_actions : dword);
procedure wl_data_device_start_drag(o : wl_data_device; source : wl_data_source; origin, icon : wl_surface; serial : dword);
procedure wl_data_device_set_selection(o : wl_data_device; source : wl_data_source; serial : dword);
procedure wl_data_device_release(o : wl_data_device);
function wl_data_device_manager_create_data_source(o : wl_data_device_manager) : wl_data_source;
function wl_data_device_manager_get_data_device(o : wl_data_device_manager; seat : wl_seat) : wl_data_device;
function wl_shell_get_shell_surface(o : wl_shell; surface : wl_surface) : wl_shell_surface;
procedure wl_shell_surface_pong(o : wl_shell_surface; serial : dword);
procedure wl_shell_surface_move(o : wl_shell_surface; seat : wl_seat; serial : dword);
procedure wl_shell_surface_resize(o : wl_shell_surface; seat : wl_seat; serial, edges : dword);
procedure wl_shell_surface_set_toplevel(o : wl_shell_surface);
procedure wl_shell_surface_set_transient(o : wl_shell_surface; parent : wl_surface; x, y : longint; flags : dword);
procedure wl_shell_surface_set_fullscreen(o : wl_shell_surface; method, framerate : dword; output : wl_output);
procedure wl_shell_surface_set_popup(o : wl_shell_surface; seat : wl_seat; serial : dword; parent : wl_surface; x, y : longint; flags : dword);
procedure wl_shell_surface_set_maximized(o : wl_shell_surface; output : wl_output);
procedure wl_shell_surface_set_title(o : wl_shell_surface; title : Pchar);
procedure wl_shell_surface_set_class(o : wl_shell_surface; class_ : Pchar);
procedure wl_surface_destroy(o : wl_surface);
procedure wl_surface_attach(o : wl_surface; buffer : wl_buffer; x, y : longint);
procedure wl_surface_damage(o : wl_surface; x, y, width, height : longint);
function wl_surface_frame(o : wl_surface) : wl_callback;
procedure wl_surface_set_opaque_region(o : wl_surface; region : wl_region);
procedure wl_surface_set_input_region(o : wl_surface; region : wl_region);
procedure wl_surface_commit(o : wl_surface);
procedure wl_surface_set_buffer_transform(o : wl_surface; transform : longint);
procedure wl_surface_set_buffer_scale(o : wl_surface; scale : longint);
procedure wl_surface_damage_buffer(o : wl_surface; x, y, width, height : longint);
procedure wl_surface_offset(o : wl_surface; x, y : longint);
function wl_seat_get_pointer(o : wl_seat) : wl_pointer;
function wl_seat_get_keyboard(o : wl_seat) : wl_keyboard;
function wl_seat_get_touch(o : wl_seat) : wl_touch;
procedure wl_seat_release(o : wl_seat);
procedure wl_pointer_set_cursor(o : wl_pointer; serial : dword; surface : wl_surface; hotspot_x, hotspot_y : longint);
procedure wl_pointer_release(o : wl_pointer);
procedure wl_keyboard_release(o : wl_keyboard);
procedure wl_touch_release(o : wl_touch);
procedure wl_output_release(o : wl_output);
procedure wl_region_destroy(o : wl_region);
procedure wl_region_add(o : wl_region; x, y, width, height : longint);
procedure wl_region_subtract(o : wl_region; x, y, width, height : longint);
procedure wl_subcompositor_destroy(o : wl_subcompositor);
function wl_subcompositor_get_subsurface(o : wl_subcompositor; surface, parent : wl_surface) : wl_subsurface;
procedure wl_subsurface_destroy(o : wl_subsurface);
procedure wl_subsurface_set_position(o : wl_subsurface; x, y : longint);
procedure wl_subsurface_place_above(o : wl_subsurface; sibling : wl_surface);
procedure wl_subsurface_place_below(o : wl_subsurface; sibling : wl_surface);
procedure wl_subsurface_set_sync(o : wl_subsurface);
procedure wl_subsurface_set_desync(o : wl_subsurface);

type
wl_display_listener = record
  error : procedure(dat:pointer; o:wl_display; object_id : wl_object; code : dword; message : Pchar); cdecl;
  delete_id : procedure(dat:pointer; o:wl_display; id : dword); cdecl;
end;

wl_registry_listener = record
  global : procedure(dat:pointer; o:wl_registry; name : dword; iface : Pchar; version : dword); cdecl;
  global_remove : procedure(dat:pointer; o:wl_registry; name : dword); cdecl;
end;

wl_callback_listener = record
  done : procedure(dat:pointer; o:wl_callback; callback_data : dword); cdecl;
end;

wl_compositor_listener = record
end;

wl_shm_pool_listener = record
end;

wl_shm_listener = record
  format : procedure(dat:pointer; o:wl_shm; format : dword); cdecl;
end;

wl_buffer_listener = record
  release : procedure(dat:pointer; o:wl_buffer); cdecl;
end;

wl_data_offer_listener = record
  offer : procedure(dat:pointer; o:wl_data_offer; mime_type : Pchar); cdecl;
  source_actions : procedure(dat:pointer; o:wl_data_offer; source_actions : dword); cdecl;
  action : procedure(dat:pointer; o:wl_data_offer; dnd_action : dword); cdecl;
end;

wl_data_source_listener = record
  target : procedure(dat:pointer; o:wl_data_source; mime_type : Pchar); cdecl;
  send : procedure(dat:pointer; o:wl_data_source; mime_type : Pchar; fd : Tcint); cdecl;
  cancelled : procedure(dat:pointer; o:wl_data_source); cdecl;
  dnd_drop_performed : procedure(dat:pointer; o:wl_data_source); cdecl;
  dnd_finished : procedure(dat:pointer; o:wl_data_source); cdecl;
  action : procedure(dat:pointer; o:wl_data_source; dnd_action : dword); cdecl;
end;

wl_data_device_listener = record
  data_offer : procedure(dat:pointer; o:wl_data_device; id : wl_data_offer); cdecl;
  enter : procedure(dat:pointer; o:wl_data_device; serial : dword; surface : wl_surface; x, y : Twlfixed; id : wl_data_offer); cdecl;
  leave : procedure(dat:pointer; o:wl_data_device); cdecl;
  motion : procedure(dat:pointer; o:wl_data_device; time : dword; x, y : Twlfixed); cdecl;
  drop : procedure(dat:pointer; o:wl_data_device); cdecl;
  selection : procedure(dat:pointer; o:wl_data_device; id : wl_data_offer); cdecl;
end;

wl_data_device_manager_listener = record
end;

wl_shell_listener = record
end;

wl_shell_surface_listener = record
  ping : procedure(dat:pointer; o:wl_shell_surface; serial : dword); cdecl;
  configure : procedure(dat:pointer; o:wl_shell_surface; edges : dword; width, height : longint); cdecl;
  popup_done : procedure(dat:pointer; o:wl_shell_surface); cdecl;
end;

wl_surface_listener = record
  enter : procedure(dat:pointer; o:wl_surface; output : wl_output); cdecl;
  leave : procedure(dat:pointer; o:wl_surface; output : wl_output); cdecl;
  preferred_buffer_scale : procedure(dat:pointer; o:wl_surface; factor : longint); cdecl;
  preferred_buffer_transform : procedure(dat:pointer; o:wl_surface; transform : dword); cdecl;
end;

wl_seat_listener = record
  capabilities : procedure(dat:pointer; o:wl_seat; capabilities : dword); cdecl;
  name : procedure(dat:pointer; o:wl_seat; name : Pchar); cdecl;
end;

wl_pointer_listener = record
  enter : procedure(dat:pointer; o:wl_pointer; serial : dword; surface : wl_surface; surface_x, surface_y : Twlfixed); cdecl;
  leave : procedure(dat:pointer; o:wl_pointer; serial : dword; surface : wl_surface); cdecl;
  motion : procedure(dat:pointer; o:wl_pointer; time : dword; surface_x, surface_y : Twlfixed); cdecl;
  button : procedure(dat:pointer; o:wl_pointer; serial, time, button, state : dword); cdecl;
  axis : procedure(dat:pointer; o:wl_pointer; time, axis : dword; value : Twlfixed); cdecl;
  frame : procedure(dat:pointer; o:wl_pointer); cdecl;
  axis_source : procedure(dat:pointer; o:wl_pointer; axis_source : dword); cdecl;
  axis_stop : procedure(dat:pointer; o:wl_pointer; time, axis : dword); cdecl;
  axis_discrete : procedure(dat:pointer; o:wl_pointer; axis : dword; discrete : longint); cdecl;
  axis_value120 : procedure(dat:pointer; o:wl_pointer; axis : dword; value120 : longint); cdecl;
  axis_relative_direction : procedure(dat:pointer; o:wl_pointer; axis, direction : dword); cdecl;
end;

wl_keyboard_listener = record
  keymap : procedure(dat:pointer; o:wl_keyboard; format : dword; fd : Tcint; size : dword); cdecl;
  enter : procedure(dat:pointer; o:wl_keyboard; serial : dword; surface : wl_surface; keys : wl_array); cdecl;
  leave : procedure(dat:pointer; o:wl_keyboard; serial : dword; surface : wl_surface); cdecl;
  key : procedure(dat:pointer; o:wl_keyboard; serial, time, key, state : dword); cdecl;
  modifiers : procedure(dat:pointer; o:wl_keyboard; serial, mods_depressed, mods_latched, mods_locked, group : dword); cdecl;
  repeat_info : procedure(dat:pointer; o:wl_keyboard; rate, delay : longint); cdecl;
end;

wl_touch_listener = record
  down : procedure(dat:pointer; o:wl_touch; serial, time : dword; surface : wl_surface; id : longint; x, y : Twlfixed); cdecl;
  up : procedure(dat:pointer; o:wl_touch; serial, time : dword; id : longint); cdecl;
  motion : procedure(dat:pointer; o:wl_touch; time : dword; id : longint; x, y : Twlfixed); cdecl;
  frame : procedure(dat:pointer; o:wl_touch); cdecl;
  cancel : procedure(dat:pointer; o:wl_touch); cdecl;
  shape : procedure(dat:pointer; o:wl_touch; id : longint; major, minor : Twlfixed); cdecl;
  orientation : procedure(dat:pointer; o:wl_touch; id : longint; orientation : Twlfixed); cdecl;
end;

wl_output_listener = record
  geometry : procedure(dat:pointer; o:wl_output; x, y, physical_width, physical_height, subpixel : longint; make, model : Pchar; transform : longint); cdecl;
  mode : procedure(dat:pointer; o:wl_output; flags : dword; width, height, refresh : longint); cdecl;
  done : procedure(dat:pointer; o:wl_output); cdecl;
  scale : procedure(dat:pointer; o:wl_output; factor : longint); cdecl;
  name : procedure(dat:pointer; o:wl_output; name : Pchar); cdecl;
  description : procedure(dat:pointer; o:wl_output; description : Pchar); cdecl;
end;

wl_region_listener = record
end;

wl_subcompositor_listener = record
end;

wl_subsurface_listener = record
end;



procedure wl_display_add_listener(o:wl_display; var l:wl_display_listener; dat:pointer);
procedure wl_registry_add_listener(o:wl_registry; var l:wl_registry_listener; dat:pointer);
procedure wl_callback_add_listener(o:wl_callback; var l:wl_callback_listener; dat:pointer);
procedure wl_compositor_add_listener(o:wl_compositor; var l:wl_compositor_listener; dat:pointer);
procedure wl_shm_pool_add_listener(o:wl_shm_pool; var l:wl_shm_pool_listener; dat:pointer);
procedure wl_shm_add_listener(o:wl_shm; var l:wl_shm_listener; dat:pointer);
procedure wl_buffer_add_listener(o:wl_buffer; var l:wl_buffer_listener; dat:pointer);
procedure wl_data_offer_add_listener(o:wl_data_offer; var l:wl_data_offer_listener; dat:pointer);
procedure wl_data_source_add_listener(o:wl_data_source; var l:wl_data_source_listener; dat:pointer);
procedure wl_data_device_add_listener(o:wl_data_device; var l:wl_data_device_listener; dat:pointer);
procedure wl_data_device_manager_add_listener(o:wl_data_device_manager; var l:wl_data_device_manager_listener; dat:pointer);
procedure wl_shell_add_listener(o:wl_shell; var l:wl_shell_listener; dat:pointer);
procedure wl_shell_surface_add_listener(o:wl_shell_surface; var l:wl_shell_surface_listener; dat:pointer);
procedure wl_surface_add_listener(o:wl_surface; var l:wl_surface_listener; dat:pointer);
procedure wl_seat_add_listener(o:wl_seat; var l:wl_seat_listener; dat:pointer);
procedure wl_pointer_add_listener(o:wl_pointer; var l:wl_pointer_listener; dat:pointer);
procedure wl_keyboard_add_listener(o:wl_keyboard; var l:wl_keyboard_listener; dat:pointer);
procedure wl_touch_add_listener(o:wl_touch; var l:wl_touch_listener; dat:pointer);
procedure wl_output_add_listener(o:wl_output; var l:wl_output_listener; dat:pointer);
procedure wl_region_add_listener(o:wl_region; var l:wl_region_listener; dat:pointer);
procedure wl_subcompositor_add_listener(o:wl_subcompositor; var l:wl_subcompositor_listener; dat:pointer);
procedure wl_subsurface_add_listener(o:wl_subsurface; var l:wl_subsurface_listener; dat:pointer);


const
wl_display_error_invalid_object = $00000000;
wl_display_error_invalid_method = $00000001;
wl_display_error_no_memory = $00000002;
wl_display_error_implementation = $00000003;
wl_shm_error_invalid_format = $00000000;
wl_shm_error_invalid_stride = $00000001;
wl_shm_error_invalid_fd = $00000002;
wl_shm_format_argb8888 = $00000000;
wl_shm_format_xrgb8888 = $00000001;
wl_shm_format_c8 = $20203843;
wl_shm_format_rgb332 = $38424752;
wl_shm_format_bgr233 = $38524742;
wl_shm_format_xrgb4444 = $32315258;
wl_shm_format_xbgr4444 = $32314258;
wl_shm_format_rgbx4444 = $32315852;
wl_shm_format_bgrx4444 = $32315842;
wl_shm_format_argb4444 = $32315241;
wl_shm_format_abgr4444 = $32314241;
wl_shm_format_rgba4444 = $32314152;
wl_shm_format_bgra4444 = $32314142;
wl_shm_format_xrgb1555 = $35315258;
wl_shm_format_xbgr1555 = $35314258;
wl_shm_format_rgbx5551 = $35315852;
wl_shm_format_bgrx5551 = $35315842;
wl_shm_format_argb1555 = $35315241;
wl_shm_format_abgr1555 = $35314241;
wl_shm_format_rgba5551 = $35314152;
wl_shm_format_bgra5551 = $35314142;
wl_shm_format_rgb565 = $36314752;
wl_shm_format_bgr565 = $36314742;
wl_shm_format_rgb888 = $34324752;
wl_shm_format_bgr888 = $34324742;
wl_shm_format_xbgr8888 = $34324258;
wl_shm_format_rgbx8888 = $34325852;
wl_shm_format_bgrx8888 = $34325842;
wl_shm_format_abgr8888 = $34324241;
wl_shm_format_rgba8888 = $34324152;
wl_shm_format_bgra8888 = $34324142;
wl_shm_format_xrgb2101010 = $30335258;
wl_shm_format_xbgr2101010 = $30334258;
wl_shm_format_rgbx1010102 = $30335852;
wl_shm_format_bgrx1010102 = $30335842;
wl_shm_format_argb2101010 = $30335241;
wl_shm_format_abgr2101010 = $30334241;
wl_shm_format_rgba1010102 = $30334152;
wl_shm_format_bgra1010102 = $30334142;
wl_shm_format_yuyv = $56595559;
wl_shm_format_yvyu = $55595659;
wl_shm_format_uyvy = $59565955;
wl_shm_format_vyuy = $59555956;
wl_shm_format_ayuv = $56555941;
wl_shm_format_nv12 = $3231564E;
wl_shm_format_nv21 = $3132564E;
wl_shm_format_nv16 = $3631564E;
wl_shm_format_nv61 = $3136564E;
wl_shm_format_yuv410 = $39565559;
wl_shm_format_yvu410 = $39555659;
wl_shm_format_yuv411 = $31315559;
wl_shm_format_yvu411 = $31315659;
wl_shm_format_yuv420 = $32315559;
wl_shm_format_yvu420 = $32315659;
wl_shm_format_yuv422 = $36315559;
wl_shm_format_yvu422 = $36315659;
wl_shm_format_yuv444 = $34325559;
wl_shm_format_yvu444 = $34325659;
wl_shm_format_r8 = $20203852;
wl_shm_format_r16 = $20363152;
wl_shm_format_rg88 = $38384752;
wl_shm_format_gr88 = $38385247;
wl_shm_format_rg1616 = $32334752;
wl_shm_format_gr1616 = $32335247;
wl_shm_format_xrgb16161616f = $48345258;
wl_shm_format_xbgr16161616f = $48344258;
wl_shm_format_argb16161616f = $48345241;
wl_shm_format_abgr16161616f = $48344241;
wl_shm_format_xyuv8888 = $56555958;
wl_shm_format_vuy888 = $34325556;
wl_shm_format_vuy101010 = $30335556;
wl_shm_format_y210 = $30313259;
wl_shm_format_y212 = $32313259;
wl_shm_format_y216 = $36313259;
wl_shm_format_y410 = $30313459;
wl_shm_format_y412 = $32313459;
wl_shm_format_y416 = $36313459;
wl_shm_format_xvyu2101010 = $30335658;
wl_shm_format_xvyu12_16161616 = $36335658;
wl_shm_format_xvyu16161616 = $38345658;
wl_shm_format_y0l0 = $304C3059;
wl_shm_format_x0l0 = $304C3058;
wl_shm_format_y0l2 = $324C3059;
wl_shm_format_x0l2 = $324C3058;
wl_shm_format_yuv420_8bit = $38305559;
wl_shm_format_yuv420_10bit = $30315559;
wl_shm_format_xrgb8888_a8 = $38415258;
wl_shm_format_xbgr8888_a8 = $38414258;
wl_shm_format_rgbx8888_a8 = $38415852;
wl_shm_format_bgrx8888_a8 = $38415842;
wl_shm_format_rgb888_a8 = $38413852;
wl_shm_format_bgr888_a8 = $38413842;
wl_shm_format_rgb565_a8 = $38413552;
wl_shm_format_bgr565_a8 = $38413542;
wl_shm_format_nv24 = $3432564E;
wl_shm_format_nv42 = $3234564E;
wl_shm_format_p210 = $30313250;
wl_shm_format_p010 = $30313050;
wl_shm_format_p012 = $32313050;
wl_shm_format_p016 = $36313050;
wl_shm_format_axbxgxrx106106106106 = $30314241;
wl_shm_format_nv15 = $3531564E;
wl_shm_format_q410 = $30313451;
wl_shm_format_q401 = $31303451;
wl_shm_format_xrgb16161616 = $38345258;
wl_shm_format_xbgr16161616 = $38344258;
wl_shm_format_argb16161616 = $38345241;
wl_shm_format_abgr16161616 = $38344241;
wl_shm_format_c1 = $20203143;
wl_shm_format_c2 = $20203243;
wl_shm_format_c4 = $20203443;
wl_shm_format_d1 = $20203144;
wl_shm_format_d2 = $20203244;
wl_shm_format_d4 = $20203444;
wl_shm_format_d8 = $20203844;
wl_shm_format_r1 = $20203152;
wl_shm_format_r2 = $20203252;
wl_shm_format_r4 = $20203452;
wl_shm_format_r10 = $20303152;
wl_shm_format_r12 = $20323152;
wl_shm_format_avuy8888 = $59555641;
wl_shm_format_xvuy8888 = $59555658;
wl_shm_format_p030 = $30333050;
wl_data_offer_error_invalid_finish = $00000000;
wl_data_offer_error_invalid_action_mask = $00000001;
wl_data_offer_error_invalid_action = $00000002;
wl_data_offer_error_invalid_offer = $00000003;
wl_data_source_error_invalid_action_mask = $00000000;
wl_data_source_error_invalid_source = $00000001;
wl_data_device_error_role = $00000000;
wl_data_device_error_used_source = $00000001;
wl_data_device_manager_dnd_action_none = $00000000;
wl_data_device_manager_dnd_action_copy = $00000001;
wl_data_device_manager_dnd_action_move = $00000002;
wl_data_device_manager_dnd_action_ask = $00000004;
wl_shell_error_role = $00000000;
wl_shell_surface_resize_none = $00000000;
wl_shell_surface_resize_top = $00000001;
wl_shell_surface_resize_bottom = $00000002;
wl_shell_surface_resize_left = $00000004;
wl_shell_surface_resize_top_left = $00000005;
wl_shell_surface_resize_bottom_left = $00000006;
wl_shell_surface_resize_right = $00000008;
wl_shell_surface_resize_top_right = $00000009;
wl_shell_surface_resize_bottom_right = $0000000A;
wl_shell_surface_transient_inactive = $00000001;
wl_shell_surface_fullscreen_method_default = $00000000;
wl_shell_surface_fullscreen_method_scale = $00000001;
wl_shell_surface_fullscreen_method_driver = $00000002;
wl_shell_surface_fullscreen_method_fill = $00000003;
wl_surface_error_invalid_scale = $00000000;
wl_surface_error_invalid_transform = $00000001;
wl_surface_error_invalid_size = $00000002;
wl_surface_error_invalid_offset = $00000003;
wl_surface_error_defunct_role_object = $00000004;
wl_seat_capability_pointer = $00000001;
wl_seat_capability_keyboard = $00000002;
wl_seat_capability_touch = $00000004;
wl_seat_error_missing_capability = $00000000;
wl_pointer_error_role = $00000000;
wl_pointer_button_state_released = $00000000;
wl_pointer_button_state_pressed = $00000001;
wl_pointer_axis_vertical_scroll = $00000000;
wl_pointer_axis_horizontal_scroll = $00000001;
wl_pointer_axis_source_wheel = $00000000;
wl_pointer_axis_source_finger = $00000001;
wl_pointer_axis_source_continuous = $00000002;
wl_pointer_axis_source_wheel_tilt = $00000003;
wl_pointer_axis_relative_direction_identical = $00000000;
wl_pointer_axis_relative_direction_inverted = $00000001;
wl_keyboard_keymap_format_no_keymap = $00000000;
wl_keyboard_keymap_format_xkb_v1 = $00000001;
wl_keyboard_key_state_released = $00000000;
wl_keyboard_key_state_pressed = $00000001;
wl_output_subpixel_unknown = $00000000;
wl_output_subpixel_none = $00000001;
wl_output_subpixel_horizontal_rgb = $00000002;
wl_output_subpixel_horizontal_bgr = $00000003;
wl_output_subpixel_vertical_rgb = $00000004;
wl_output_subpixel_vertical_bgr = $00000005;
wl_output_transform_normal = $00000000;
wl_output_transform_90 = $00000001;
wl_output_transform_180 = $00000002;
wl_output_transform_270 = $00000003;
wl_output_transform_flipped = $00000004;
wl_output_transform_flipped_90 = $00000005;
wl_output_transform_flipped_180 = $00000006;
wl_output_transform_flipped_270 = $00000007;
wl_output_mode_current = $00000001;
wl_output_mode_preferred = $00000002;
wl_subcompositor_error_bad_surface = $00000000;
wl_subcompositor_error_bad_parent = $00000001;
wl_subsurface_error_bad_surface = $00000000;


var
wl_display_ifacedat : Twl_interface = (name:'wl_display'; version:1; method_count:2; methods:nil; event_count:2; events:nil); 
wl_registry_ifacedat : Twl_interface = (name:'wl_registry'; version:1; method_count:1; methods:nil; event_count:2; events:nil); 
wl_callback_ifacedat : Twl_interface = (name:'wl_callback'; version:1; method_count:0; methods:nil; event_count:1; events:nil); 
wl_compositor_ifacedat : Twl_interface = (name:'wl_compositor'; version:6; method_count:2; methods:nil; event_count:0; events:nil); 
wl_shm_pool_ifacedat : Twl_interface = (name:'wl_shm_pool'; version:1; method_count:3; methods:nil; event_count:0; events:nil); 
wl_shm_ifacedat : Twl_interface = (name:'wl_shm'; version:1; method_count:1; methods:nil; event_count:1; events:nil); 
wl_buffer_ifacedat : Twl_interface = (name:'wl_buffer'; version:1; method_count:1; methods:nil; event_count:1; events:nil); 
wl_data_offer_ifacedat : Twl_interface = (name:'wl_data_offer'; version:3; method_count:5; methods:nil; event_count:3; events:nil); 
wl_data_source_ifacedat : Twl_interface = (name:'wl_data_source'; version:3; method_count:3; methods:nil; event_count:6; events:nil); 
wl_data_device_ifacedat : Twl_interface = (name:'wl_data_device'; version:3; method_count:3; methods:nil; event_count:6; events:nil); 
wl_data_device_manager_ifacedat : Twl_interface = (name:'wl_data_device_manager'; version:3; method_count:2; methods:nil; event_count:0; events:nil); 
wl_shell_ifacedat : Twl_interface = (name:'wl_shell'; version:1; method_count:1; methods:nil; event_count:0; events:nil); 
wl_shell_surface_ifacedat : Twl_interface = (name:'wl_shell_surface'; version:1; method_count:10; methods:nil; event_count:3; events:nil); 
wl_surface_ifacedat : Twl_interface = (name:'wl_surface'; version:6; method_count:11; methods:nil; event_count:4; events:nil); 
wl_seat_ifacedat : Twl_interface = (name:'wl_seat'; version:9; method_count:4; methods:nil; event_count:2; events:nil); 
wl_pointer_ifacedat : Twl_interface = (name:'wl_pointer'; version:9; method_count:2; methods:nil; event_count:11; events:nil); 
wl_keyboard_ifacedat : Twl_interface = (name:'wl_keyboard'; version:9; method_count:1; methods:nil; event_count:6; events:nil); 
wl_touch_ifacedat : Twl_interface = (name:'wl_touch'; version:9; method_count:1; methods:nil; event_count:7; events:nil); 
wl_output_ifacedat : Twl_interface = (name:'wl_output'; version:4; method_count:1; methods:nil; event_count:6; events:nil); 
wl_region_ifacedat : Twl_interface = (name:'wl_region'; version:1; method_count:3; methods:nil; event_count:0; events:nil); 
wl_subcompositor_ifacedat : Twl_interface = (name:'wl_subcompositor'; version:1; method_count:2; methods:nil; event_count:0; events:nil); 
wl_subsurface_ifacedat : Twl_interface = (name:'wl_subsurface'; version:1; method_count:6; methods:nil; event_count:0; events:nil); 


implementation



procedure wl_display_add_listener(o:wl_display; var l:wl_display_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_registry_add_listener(o:wl_registry; var l:wl_registry_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_callback_add_listener(o:wl_callback; var l:wl_callback_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_compositor_add_listener(o:wl_compositor; var l:wl_compositor_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_shm_pool_add_listener(o:wl_shm_pool; var l:wl_shm_pool_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_shm_add_listener(o:wl_shm; var l:wl_shm_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_buffer_add_listener(o:wl_buffer; var l:wl_buffer_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_data_offer_add_listener(o:wl_data_offer; var l:wl_data_offer_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_data_source_add_listener(o:wl_data_source; var l:wl_data_source_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_data_device_add_listener(o:wl_data_device; var l:wl_data_device_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_data_device_manager_add_listener(o:wl_data_device_manager; var l:wl_data_device_manager_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_shell_add_listener(o:wl_shell; var l:wl_shell_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_shell_surface_add_listener(o:wl_shell_surface; var l:wl_shell_surface_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_surface_add_listener(o:wl_surface; var l:wl_surface_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_seat_add_listener(o:wl_seat; var l:wl_seat_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_pointer_add_listener(o:wl_pointer; var l:wl_pointer_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_keyboard_add_listener(o:wl_keyboard; var l:wl_keyboard_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_touch_add_listener(o:wl_touch; var l:wl_touch_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_output_add_listener(o:wl_output; var l:wl_output_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_region_add_listener(o:wl_region; var l:wl_region_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_subcompositor_add_listener(o:wl_subcompositor; var l:wl_subcompositor_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wl_subsurface_add_listener(o:wl_subsurface; var l:wl_subsurface_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;



function wl_display_sync(o : wl_display) : wl_callback;
var parm : Tparamarr1;
begin
  parm[0].o := nil;
  wl_display_sync := wl_callback(wl_proxy_marshal_array_constructor(o,0,@parm[0],wl_callback_interface));
end;

function wl_display_get_registry(o : wl_display) : wl_registry;
var parm : Tparamarr1;
begin
  parm[0].o := nil;
  wl_display_get_registry := wl_registry(wl_proxy_marshal_array_constructor(o,1,@parm[0],wl_registry_interface));
end;

function wl_registry_bind(o : wl_registry; intf : Pwl_interface; version : dword; name : dword) : wl_object;
var parm : Tparamarr4;
begin
  parm[0].u := name;
  parm[1].s := intf^.name;
  parm[2].u := version;
  parm[3].o := nil;
  wl_registry_bind := wl_object(wl_proxy_marshal_array_constructor_versioned(o,0,@parm[0],intf,version));
end;

function wl_compositor_create_surface(o : wl_compositor) : wl_surface;
var parm : Tparamarr1;
begin
  parm[0].o := nil;
  wl_compositor_create_surface := wl_surface(wl_proxy_marshal_array_constructor(o,0,@parm[0],wl_surface_interface));
end;

function wl_compositor_create_region(o : wl_compositor) : wl_region;
var parm : Tparamarr1;
begin
  parm[0].o := nil;
  wl_compositor_create_region := wl_region(wl_proxy_marshal_array_constructor(o,1,@parm[0],wl_region_interface));
end;

function wl_shm_pool_create_buffer(o : wl_shm_pool; offset, width, height, stride : longint; format : dword) : wl_buffer;
var parm : Tparamarr6;
begin
  parm[0].o := nil;
  parm[1].i := offset;
  parm[2].i := width;
  parm[3].i := height;
  parm[4].i := stride;
  parm[5].u := format;
  wl_shm_pool_create_buffer := wl_buffer(wl_proxy_marshal_array_constructor(o,0,@parm[0],wl_buffer_interface));
end;

procedure wl_shm_pool_destroy(o : wl_shm_pool);
begin
  wl_proxy_marshal_array(o,1,nil);
end;

procedure wl_shm_pool_resize(o : wl_shm_pool; size : longint);
var parm : Tparamarr1;
begin
  parm[0].i := size;
  wl_proxy_marshal_array(o,2,@parm[0]);
end;

function wl_shm_create_pool(o : wl_shm; fd : Tcint; size : longint) : wl_shm_pool;
var parm : Tparamarr3;
begin
  parm[0].o := nil;
  parm[1].d := fd;
  parm[2].i := size;
  wl_shm_create_pool := wl_shm_pool(wl_proxy_marshal_array_constructor(o,0,@parm[0],wl_shm_pool_interface));
end;

procedure wl_buffer_destroy(o : wl_buffer);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

procedure wl_data_offer_accept(o : wl_data_offer; serial : dword; mime_type : Pchar);
var parm : Tparamarr2;
begin
  parm[0].u := serial;
  parm[1].s := mime_type;
  wl_proxy_marshal_array(o,0,@parm[0]);
end;

procedure wl_data_offer_receive(o : wl_data_offer; mime_type : Pchar; fd : Tcint);
var parm : Tparamarr2;
begin
  parm[0].s := mime_type;
  parm[1].d := fd;
  wl_proxy_marshal_array(o,1,@parm[0]);
end;

procedure wl_data_offer_destroy(o : wl_data_offer);
begin
  wl_proxy_marshal_array(o,2,nil);
end;

procedure wl_data_offer_finish(o : wl_data_offer);
begin
  wl_proxy_marshal_array(o,3,nil);
end;

procedure wl_data_offer_set_actions(o : wl_data_offer; dnd_actions, preferred_action : dword);
var parm : Tparamarr2;
begin
  parm[0].u := dnd_actions;
  parm[1].u := preferred_action;
  wl_proxy_marshal_array(o,4,@parm[0]);
end;

procedure wl_data_source_offer(o : wl_data_source; mime_type : Pchar);
var parm : Tparamarr1;
begin
  parm[0].s := mime_type;
  wl_proxy_marshal_array(o,0,@parm[0]);
end;

procedure wl_data_source_destroy(o : wl_data_source);
begin
  wl_proxy_marshal_array(o,1,nil);
end;

procedure wl_data_source_set_actions(o : wl_data_source; dnd_actions : dword);
var parm : Tparamarr1;
begin
  parm[0].u := dnd_actions;
  wl_proxy_marshal_array(o,2,@parm[0]);
end;

procedure wl_data_device_start_drag(o : wl_data_device; source : wl_data_source; origin, icon : wl_surface; serial : dword);
var parm : Tparamarr4;
begin
  parm[0].o := source;
  parm[1].o := origin;
  parm[2].o := icon;
  parm[3].u := serial;
  wl_proxy_marshal_array(o,0,@parm[0]);
end;

procedure wl_data_device_set_selection(o : wl_data_device; source : wl_data_source; serial : dword);
var parm : Tparamarr2;
begin
  parm[0].o := source;
  parm[1].u := serial;
  wl_proxy_marshal_array(o,1,@parm[0]);
end;

procedure wl_data_device_release(o : wl_data_device);
begin
  wl_proxy_marshal_array(o,2,nil);
end;

function wl_data_device_manager_create_data_source(o : wl_data_device_manager) : wl_data_source;
var parm : Tparamarr1;
begin
  parm[0].o := nil;
  wl_data_device_manager_create_data_source := wl_data_source(wl_proxy_marshal_array_constructor(o,0,@parm[0],wl_data_source_interface));
end;

function wl_data_device_manager_get_data_device(o : wl_data_device_manager; seat : wl_seat) : wl_data_device;
var parm : Tparamarr2;
begin
  parm[0].o := nil;
  parm[1].o := seat;
  wl_data_device_manager_get_data_device := wl_data_device(wl_proxy_marshal_array_constructor(o,1,@parm[0],wl_data_device_interface));
end;

function wl_shell_get_shell_surface(o : wl_shell; surface : wl_surface) : wl_shell_surface;
var parm : Tparamarr2;
begin
  parm[0].o := nil;
  parm[1].o := surface;
  wl_shell_get_shell_surface := wl_shell_surface(wl_proxy_marshal_array_constructor(o,0,@parm[0],wl_shell_surface_interface));
end;

procedure wl_shell_surface_pong(o : wl_shell_surface; serial : dword);
var parm : Tparamarr1;
begin
  parm[0].u := serial;
  wl_proxy_marshal_array(o,0,@parm[0]);
end;

procedure wl_shell_surface_move(o : wl_shell_surface; seat : wl_seat; serial : dword);
var parm : Tparamarr2;
begin
  parm[0].o := seat;
  parm[1].u := serial;
  wl_proxy_marshal_array(o,1,@parm[0]);
end;

procedure wl_shell_surface_resize(o : wl_shell_surface; seat : wl_seat; serial, edges : dword);
var parm : Tparamarr3;
begin
  parm[0].o := seat;
  parm[1].u := serial;
  parm[2].u := edges;
  wl_proxy_marshal_array(o,2,@parm[0]);
end;

procedure wl_shell_surface_set_toplevel(o : wl_shell_surface);
begin
  wl_proxy_marshal_array(o,3,nil);
end;

procedure wl_shell_surface_set_transient(o : wl_shell_surface; parent : wl_surface; x, y : longint; flags : dword);
var parm : Tparamarr4;
begin
  parm[0].o := parent;
  parm[1].i := x;
  parm[2].i := y;
  parm[3].u := flags;
  wl_proxy_marshal_array(o,4,@parm[0]);
end;

procedure wl_shell_surface_set_fullscreen(o : wl_shell_surface; method, framerate : dword; output : wl_output);
var parm : Tparamarr3;
begin
  parm[0].u := method;
  parm[1].u := framerate;
  parm[2].o := output;
  wl_proxy_marshal_array(o,5,@parm[0]);
end;

procedure wl_shell_surface_set_popup(o : wl_shell_surface; seat : wl_seat; serial : dword; parent : wl_surface; x, y : longint; flags : dword);
var parm : Tparamarr6;
begin
  parm[0].o := seat;
  parm[1].u := serial;
  parm[2].o := parent;
  parm[3].i := x;
  parm[4].i := y;
  parm[5].u := flags;
  wl_proxy_marshal_array(o,6,@parm[0]);
end;

procedure wl_shell_surface_set_maximized(o : wl_shell_surface; output : wl_output);
var parm : Tparamarr1;
begin
  parm[0].o := output;
  wl_proxy_marshal_array(o,7,@parm[0]);
end;

procedure wl_shell_surface_set_title(o : wl_shell_surface; title : Pchar);
var parm : Tparamarr1;
begin
  parm[0].s := title;
  wl_proxy_marshal_array(o,8,@parm[0]);
end;

procedure wl_shell_surface_set_class(o : wl_shell_surface; class_ : Pchar);
var parm : Tparamarr1;
begin
  parm[0].s := class_;
  wl_proxy_marshal_array(o,9,@parm[0]);
end;

procedure wl_surface_destroy(o : wl_surface);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

procedure wl_surface_attach(o : wl_surface; buffer : wl_buffer; x, y : longint);
var parm : Tparamarr3;
begin
  parm[0].o := buffer;
  parm[1].i := x;
  parm[2].i := y;
  wl_proxy_marshal_array(o,1,@parm[0]);
end;

procedure wl_surface_damage(o : wl_surface; x, y, width, height : longint);
var parm : Tparamarr4;
begin
  parm[0].i := x;
  parm[1].i := y;
  parm[2].i := width;
  parm[3].i := height;
  wl_proxy_marshal_array(o,2,@parm[0]);
end;

function wl_surface_frame(o : wl_surface) : wl_callback;
var parm : Tparamarr1;
begin
  parm[0].o := nil;
  wl_surface_frame := wl_callback(wl_proxy_marshal_array_constructor(o,3,@parm[0],wl_callback_interface));
end;

procedure wl_surface_set_opaque_region(o : wl_surface; region : wl_region);
var parm : Tparamarr1;
begin
  parm[0].o := region;
  wl_proxy_marshal_array(o,4,@parm[0]);
end;

procedure wl_surface_set_input_region(o : wl_surface; region : wl_region);
var parm : Tparamarr1;
begin
  parm[0].o := region;
  wl_proxy_marshal_array(o,5,@parm[0]);
end;

procedure wl_surface_commit(o : wl_surface);
begin
  wl_proxy_marshal_array(o,6,nil);
end;

procedure wl_surface_set_buffer_transform(o : wl_surface; transform : longint);
var parm : Tparamarr1;
begin
  parm[0].i := transform;
  wl_proxy_marshal_array(o,7,@parm[0]);
end;

procedure wl_surface_set_buffer_scale(o : wl_surface; scale : longint);
var parm : Tparamarr1;
begin
  parm[0].i := scale;
  wl_proxy_marshal_array(o,8,@parm[0]);
end;

procedure wl_surface_damage_buffer(o : wl_surface; x, y, width, height : longint);
var parm : Tparamarr4;
begin
  parm[0].i := x;
  parm[1].i := y;
  parm[2].i := width;
  parm[3].i := height;
  wl_proxy_marshal_array(o,9,@parm[0]);
end;

procedure wl_surface_offset(o : wl_surface; x, y : longint);
var parm : Tparamarr2;
begin
  parm[0].i := x;
  parm[1].i := y;
  wl_proxy_marshal_array(o,10,@parm[0]);
end;

function wl_seat_get_pointer(o : wl_seat) : wl_pointer;
var parm : Tparamarr1;
begin
  parm[0].o := nil;
  wl_seat_get_pointer := wl_pointer(wl_proxy_marshal_array_constructor(o,0,@parm[0],wl_pointer_interface));
end;

function wl_seat_get_keyboard(o : wl_seat) : wl_keyboard;
var parm : Tparamarr1;
begin
  parm[0].o := nil;
  wl_seat_get_keyboard := wl_keyboard(wl_proxy_marshal_array_constructor(o,1,@parm[0],wl_keyboard_interface));
end;

function wl_seat_get_touch(o : wl_seat) : wl_touch;
var parm : Tparamarr1;
begin
  parm[0].o := nil;
  wl_seat_get_touch := wl_touch(wl_proxy_marshal_array_constructor(o,2,@parm[0],wl_touch_interface));
end;

procedure wl_seat_release(o : wl_seat);
begin
  wl_proxy_marshal_array(o,3,nil);
end;

procedure wl_pointer_set_cursor(o : wl_pointer; serial : dword; surface : wl_surface; hotspot_x, hotspot_y : longint);
var parm : Tparamarr4;
begin
  parm[0].u := serial;
  parm[1].o := surface;
  parm[2].i := hotspot_x;
  parm[3].i := hotspot_y;
  wl_proxy_marshal_array(o,0,@parm[0]);
end;

procedure wl_pointer_release(o : wl_pointer);
begin
  wl_proxy_marshal_array(o,1,nil);
end;

procedure wl_keyboard_release(o : wl_keyboard);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

procedure wl_touch_release(o : wl_touch);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

procedure wl_output_release(o : wl_output);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

procedure wl_region_destroy(o : wl_region);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

procedure wl_region_add(o : wl_region; x, y, width, height : longint);
var parm : Tparamarr4;
begin
  parm[0].i := x;
  parm[1].i := y;
  parm[2].i := width;
  parm[3].i := height;
  wl_proxy_marshal_array(o,1,@parm[0]);
end;

procedure wl_region_subtract(o : wl_region; x, y, width, height : longint);
var parm : Tparamarr4;
begin
  parm[0].i := x;
  parm[1].i := y;
  parm[2].i := width;
  parm[3].i := height;
  wl_proxy_marshal_array(o,2,@parm[0]);
end;

procedure wl_subcompositor_destroy(o : wl_subcompositor);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

function wl_subcompositor_get_subsurface(o : wl_subcompositor; surface, parent : wl_surface) : wl_subsurface;
var parm : Tparamarr3;
begin
  parm[0].o := nil;
  parm[1].o := surface;
  parm[2].o := parent;
  wl_subcompositor_get_subsurface := wl_subsurface(wl_proxy_marshal_array_constructor(o,1,@parm[0],wl_subsurface_interface));
end;

procedure wl_subsurface_destroy(o : wl_subsurface);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

procedure wl_subsurface_set_position(o : wl_subsurface; x, y : longint);
var parm : Tparamarr2;
begin
  parm[0].i := x;
  parm[1].i := y;
  wl_proxy_marshal_array(o,1,@parm[0]);
end;

procedure wl_subsurface_place_above(o : wl_subsurface; sibling : wl_surface);
var parm : Tparamarr1;
begin
  parm[0].o := sibling;
  wl_proxy_marshal_array(o,2,@parm[0]);
end;

procedure wl_subsurface_place_below(o : wl_subsurface; sibling : wl_surface);
var parm : Tparamarr1;
begin
  parm[0].o := sibling;
  wl_proxy_marshal_array(o,3,@parm[0]);
end;

procedure wl_subsurface_set_sync(o : wl_subsurface);
begin
  wl_proxy_marshal_array(o,4,nil);
end;

procedure wl_subsurface_set_desync(o : wl_subsurface);
begin
  wl_proxy_marshal_array(o,5,nil);
end;


var
reqtypes : array[0..96] of Pwl_interface = (
  @wl_callback_ifacedat, // 0:wl_display.sync
  @wl_registry_ifacedat, // 1:wl_display.get_registry
  nil, nil, // 2:wl_registry.bind
  @wl_surface_ifacedat, // 4:wl_compositor.create_surface
  @wl_region_ifacedat, // 5:wl_compositor.create_region
  @wl_buffer_ifacedat, nil, nil, nil, nil, nil, // 6:wl_shm_pool.create_buffer
  // 12:wl_shm_pool.destroy
  nil, // 12:wl_shm_pool.resize
  @wl_shm_pool_ifacedat, nil, nil, // 13:wl_shm.create_pool
  // 16:wl_buffer.destroy
  nil, nil, // 16:wl_data_offer.accept
  nil, nil, // 18:wl_data_offer.receive
  // 20:wl_data_offer.destroy
  // 20:wl_data_offer.finish
  nil, nil, // 20:wl_data_offer.set_actions
  nil, // 22:wl_data_source.offer
  // 23:wl_data_source.destroy
  nil, // 23:wl_data_source.set_actions
  @wl_data_source_ifacedat, @wl_surface_ifacedat, @wl_surface_ifacedat, nil, // 24:wl_data_device.start_drag
  @wl_data_source_ifacedat, nil, // 28:wl_data_device.set_selection
  // 30:wl_data_device.release
  @wl_data_source_ifacedat, // 30:wl_data_device_manager.create_data_source
  @wl_data_device_ifacedat, @wl_seat_ifacedat, // 31:wl_data_device_manager.get_data_device
  @wl_shell_surface_ifacedat, @wl_surface_ifacedat, // 33:wl_shell.get_shell_surface
  nil, // 35:wl_shell_surface.pong
  @wl_seat_ifacedat, nil, // 36:wl_shell_surface.move
  @wl_seat_ifacedat, nil, nil, // 38:wl_shell_surface.resize
  // 41:wl_shell_surface.set_toplevel
  @wl_surface_ifacedat, nil, nil, nil, // 41:wl_shell_surface.set_transient
  nil, nil, @wl_output_ifacedat, // 45:wl_shell_surface.set_fullscreen
  @wl_seat_ifacedat, nil, @wl_surface_ifacedat, nil, nil, nil, // 48:wl_shell_surface.set_popup
  @wl_output_ifacedat, // 54:wl_shell_surface.set_maximized
  nil, // 55:wl_shell_surface.set_title
  nil, // 56:wl_shell_surface.set_class
  // 57:wl_surface.destroy
  @wl_buffer_ifacedat, nil, nil, // 57:wl_surface.attach
  nil, nil, nil, nil, // 60:wl_surface.damage
  @wl_callback_ifacedat, // 64:wl_surface.frame
  @wl_region_ifacedat, // 65:wl_surface.set_opaque_region
  @wl_region_ifacedat, // 66:wl_surface.set_input_region
  // 67:wl_surface.commit
  nil, // 67:wl_surface.set_buffer_transform
  nil, // 68:wl_surface.set_buffer_scale
  nil, nil, nil, nil, // 69:wl_surface.damage_buffer
  nil, nil, // 73:wl_surface.offset
  @wl_pointer_ifacedat, // 75:wl_seat.get_pointer
  @wl_keyboard_ifacedat, // 76:wl_seat.get_keyboard
  @wl_touch_ifacedat, // 77:wl_seat.get_touch
  // 78:wl_seat.release
  nil, @wl_surface_ifacedat, nil, nil, // 78:wl_pointer.set_cursor
  // 82:wl_pointer.release
  // 82:wl_keyboard.release
  // 82:wl_touch.release
  // 82:wl_output.release
  // 82:wl_region.destroy
  nil, nil, nil, nil, // 82:wl_region.add
  nil, nil, nil, nil, // 86:wl_region.subtract
  // 90:wl_subcompositor.destroy
  @wl_subsurface_ifacedat, @wl_surface_ifacedat, @wl_surface_ifacedat, // 90:wl_subcompositor.get_subsurface
  // 93:wl_subsurface.destroy
  nil, nil, // 93:wl_subsurface.set_position
  @wl_surface_ifacedat, // 95:wl_subsurface.place_above
  @wl_surface_ifacedat// 96:wl_subsurface.place_below
  // 97:wl_subsurface.set_sync
  // 97:wl_subsurface.set_desync
);

argtypes : array[0..113] of Pwl_interface = (
  nil, nil, nil, // 0:wl_display.error
  nil, // 3:wl_display.delete_id
  nil, nil, nil, // 4:wl_registry.global
  nil, // 7:wl_registry.global_remove
  nil, // 8:wl_callback.done
  nil, // 9:wl_shm.format
  // 10:wl_buffer.release
  nil, // 10:wl_data_offer.offer
  nil, // 11:wl_data_offer.source_actions
  nil, // 12:wl_data_offer.action
  nil, // 13:wl_data_source.target
  nil, nil, // 14:wl_data_source.send
  // 16:wl_data_source.cancelled
  // 16:wl_data_source.dnd_drop_performed
  // 16:wl_data_source.dnd_finished
  nil, // 16:wl_data_source.action
  @wl_data_offer_ifacedat, // 17:wl_data_device.data_offer
  nil, @wl_surface_ifacedat, nil, nil, @wl_data_offer_ifacedat, // 18:wl_data_device.enter
  // 23:wl_data_device.leave
  nil, nil, nil, // 23:wl_data_device.motion
  // 26:wl_data_device.drop
  @wl_data_offer_ifacedat, // 26:wl_data_device.selection
  nil, // 27:wl_shell_surface.ping
  nil, nil, nil, // 28:wl_shell_surface.configure
  // 31:wl_shell_surface.popup_done
  @wl_output_ifacedat, // 31:wl_surface.enter
  @wl_output_ifacedat, // 32:wl_surface.leave
  nil, // 33:wl_surface.preferred_buffer_scale
  nil, // 34:wl_surface.preferred_buffer_transform
  nil, // 35:wl_seat.capabilities
  nil, // 36:wl_seat.name
  nil, @wl_surface_ifacedat, nil, nil, // 37:wl_pointer.enter
  nil, @wl_surface_ifacedat, // 41:wl_pointer.leave
  nil, nil, nil, // 43:wl_pointer.motion
  nil, nil, nil, nil, // 46:wl_pointer.button
  nil, nil, nil, // 50:wl_pointer.axis
  // 53:wl_pointer.frame
  nil, // 53:wl_pointer.axis_source
  nil, nil, // 54:wl_pointer.axis_stop
  nil, nil, // 56:wl_pointer.axis_discrete
  nil, nil, // 58:wl_pointer.axis_value120
  nil, nil, // 60:wl_pointer.axis_relative_direction
  nil, nil, nil, // 62:wl_keyboard.keymap
  nil, @wl_surface_ifacedat, nil, // 65:wl_keyboard.enter
  nil, @wl_surface_ifacedat, // 68:wl_keyboard.leave
  nil, nil, nil, nil, // 70:wl_keyboard.key
  nil, nil, nil, nil, nil, // 74:wl_keyboard.modifiers
  nil, nil, // 79:wl_keyboard.repeat_info
  nil, nil, @wl_surface_ifacedat, nil, nil, nil, // 81:wl_touch.down
  nil, nil, nil, // 87:wl_touch.up
  nil, nil, nil, nil, // 90:wl_touch.motion
  // 94:wl_touch.frame
  // 94:wl_touch.cancel
  nil, nil, nil, // 94:wl_touch.shape
  nil, nil, // 97:wl_touch.orientation
  nil, nil, nil, nil, nil, nil, nil, nil, // 99:wl_output.geometry
  nil, nil, nil, nil, // 107:wl_output.mode
  // 111:wl_output.done
  nil, // 111:wl_output.scale
  nil, // 112:wl_output.name
  nil// 113:wl_output.description
);

requests : array[0..64] of Twl_message = (
  (name:'sync'; signature:'n'; types:@reqtypes[0]),// wl_display
  (name:'get_registry'; signature:'n'; types:@reqtypes[1]),// wl_display
  (name:'bind'; signature:'usun'; types:@reqtypes[2]),// wl_registry
  (name:'create_surface'; signature:'n'; types:@reqtypes[4]),// wl_compositor
  (name:'create_region'; signature:'n'; types:@reqtypes[5]),// wl_compositor
  (name:'create_buffer'; signature:'niiiiu'; types:@reqtypes[6]),// wl_shm_pool
  (name:'destroy'; signature:''; types:nil),// wl_shm_pool
  (name:'resize'; signature:'i'; types:@reqtypes[12]),// wl_shm_pool
  (name:'create_pool'; signature:'nhi'; types:@reqtypes[13]),// wl_shm
  (name:'destroy'; signature:''; types:nil),// wl_buffer
  (name:'accept'; signature:'u?s'; types:@reqtypes[16]),// wl_data_offer
  (name:'receive'; signature:'sh'; types:@reqtypes[18]),// wl_data_offer
  (name:'destroy'; signature:''; types:nil),// wl_data_offer
  (name:'finish'; signature:''; types:nil),// wl_data_offer
  (name:'set_actions'; signature:'uu'; types:@reqtypes[20]),// wl_data_offer
  (name:'offer'; signature:'s'; types:@reqtypes[22]),// wl_data_source
  (name:'destroy'; signature:''; types:nil),// wl_data_source
  (name:'set_actions'; signature:'u'; types:@reqtypes[23]),// wl_data_source
  (name:'start_drag'; signature:'?oo?ou'; types:@reqtypes[24]),// wl_data_device
  (name:'set_selection'; signature:'?ou'; types:@reqtypes[28]),// wl_data_device
  (name:'release'; signature:''; types:nil),// wl_data_device
  (name:'create_data_source'; signature:'n'; types:@reqtypes[30]),// wl_data_device_manager
  (name:'get_data_device'; signature:'no'; types:@reqtypes[31]),// wl_data_device_manager
  (name:'get_shell_surface'; signature:'no'; types:@reqtypes[33]),// wl_shell
  (name:'pong'; signature:'u'; types:@reqtypes[35]),// wl_shell_surface
  (name:'move'; signature:'ou'; types:@reqtypes[36]),// wl_shell_surface
  (name:'resize'; signature:'ouu'; types:@reqtypes[38]),// wl_shell_surface
  (name:'set_toplevel'; signature:''; types:nil),// wl_shell_surface
  (name:'set_transient'; signature:'oiiu'; types:@reqtypes[41]),// wl_shell_surface
  (name:'set_fullscreen'; signature:'uu?o'; types:@reqtypes[45]),// wl_shell_surface
  (name:'set_popup'; signature:'ouoiiu'; types:@reqtypes[48]),// wl_shell_surface
  (name:'set_maximized'; signature:'?o'; types:@reqtypes[54]),// wl_shell_surface
  (name:'set_title'; signature:'s'; types:@reqtypes[55]),// wl_shell_surface
  (name:'set_class'; signature:'s'; types:@reqtypes[56]),// wl_shell_surface
  (name:'destroy'; signature:''; types:nil),// wl_surface
  (name:'attach'; signature:'?oii'; types:@reqtypes[57]),// wl_surface
  (name:'damage'; signature:'iiii'; types:@reqtypes[60]),// wl_surface
  (name:'frame'; signature:'n'; types:@reqtypes[64]),// wl_surface
  (name:'set_opaque_region'; signature:'?o'; types:@reqtypes[65]),// wl_surface
  (name:'set_input_region'; signature:'?o'; types:@reqtypes[66]),// wl_surface
  (name:'commit'; signature:''; types:nil),// wl_surface
  (name:'set_buffer_transform'; signature:'i'; types:@reqtypes[67]),// wl_surface
  (name:'set_buffer_scale'; signature:'i'; types:@reqtypes[68]),// wl_surface
  (name:'damage_buffer'; signature:'iiii'; types:@reqtypes[69]),// wl_surface
  (name:'offset'; signature:'ii'; types:@reqtypes[73]),// wl_surface
  (name:'get_pointer'; signature:'n'; types:@reqtypes[75]),// wl_seat
  (name:'get_keyboard'; signature:'n'; types:@reqtypes[76]),// wl_seat
  (name:'get_touch'; signature:'n'; types:@reqtypes[77]),// wl_seat
  (name:'release'; signature:''; types:nil),// wl_seat
  (name:'set_cursor'; signature:'u?oii'; types:@reqtypes[78]),// wl_pointer
  (name:'release'; signature:''; types:nil),// wl_pointer
  (name:'release'; signature:''; types:nil),// wl_keyboard
  (name:'release'; signature:''; types:nil),// wl_touch
  (name:'release'; signature:''; types:nil),// wl_output
  (name:'destroy'; signature:''; types:nil),// wl_region
  (name:'add'; signature:'iiii'; types:@reqtypes[82]),// wl_region
  (name:'subtract'; signature:'iiii'; types:@reqtypes[86]),// wl_region
  (name:'destroy'; signature:''; types:nil),// wl_subcompositor
  (name:'get_subsurface'; signature:'noo'; types:@reqtypes[90]),// wl_subcompositor
  (name:'destroy'; signature:''; types:nil),// wl_subsurface
  (name:'set_position'; signature:'ii'; types:@reqtypes[93]),// wl_subsurface
  (name:'place_above'; signature:'o'; types:@reqtypes[95]),// wl_subsurface
  (name:'place_below'; signature:'o'; types:@reqtypes[96]),// wl_subsurface
  (name:'set_sync'; signature:''; types:nil),// wl_subsurface
  (name:'set_desync'; signature:''; types:nil)// wl_subsurface
);

events : array[0..60] of Twl_message = (
  (name:'error'; signature:'ous'; types:@argtypes[0]),// wl_display
  (name:'delete_id'; signature:'u'; types:@argtypes[3]),// wl_display
  (name:'global'; signature:'usu'; types:@argtypes[4]),// wl_registry
  (name:'global_remove'; signature:'u'; types:@argtypes[7]),// wl_registry
  (name:'done'; signature:'u'; types:@argtypes[8]),// wl_callback
  (name:'format'; signature:'u'; types:@argtypes[9]),// wl_shm
  (name:'release'; signature:''; types:nil),// wl_buffer
  (name:'offer'; signature:'s'; types:@argtypes[10]),// wl_data_offer
  (name:'source_actions'; signature:'u'; types:@argtypes[11]),// wl_data_offer
  (name:'action'; signature:'u'; types:@argtypes[12]),// wl_data_offer
  (name:'target'; signature:'?s'; types:@argtypes[13]),// wl_data_source
  (name:'send'; signature:'sh'; types:@argtypes[14]),// wl_data_source
  (name:'cancelled'; signature:''; types:nil),// wl_data_source
  (name:'dnd_drop_performed'; signature:''; types:nil),// wl_data_source
  (name:'dnd_finished'; signature:''; types:nil),// wl_data_source
  (name:'action'; signature:'u'; types:@argtypes[16]),// wl_data_source
  (name:'data_offer'; signature:'n'; types:@argtypes[17]),// wl_data_device
  (name:'enter'; signature:'uoff?o'; types:@argtypes[18]),// wl_data_device
  (name:'leave'; signature:''; types:nil),// wl_data_device
  (name:'motion'; signature:'uff'; types:@argtypes[23]),// wl_data_device
  (name:'drop'; signature:''; types:nil),// wl_data_device
  (name:'selection'; signature:'?o'; types:@argtypes[26]),// wl_data_device
  (name:'ping'; signature:'u'; types:@argtypes[27]),// wl_shell_surface
  (name:'configure'; signature:'uii'; types:@argtypes[28]),// wl_shell_surface
  (name:'popup_done'; signature:''; types:nil),// wl_shell_surface
  (name:'enter'; signature:'o'; types:@argtypes[31]),// wl_surface
  (name:'leave'; signature:'o'; types:@argtypes[32]),// wl_surface
  (name:'preferred_buffer_scale'; signature:'i'; types:@argtypes[33]),// wl_surface
  (name:'preferred_buffer_transform'; signature:'u'; types:@argtypes[34]),// wl_surface
  (name:'capabilities'; signature:'u'; types:@argtypes[35]),// wl_seat
  (name:'name'; signature:'s'; types:@argtypes[36]),// wl_seat
  (name:'enter'; signature:'uoff'; types:@argtypes[37]),// wl_pointer
  (name:'leave'; signature:'uo'; types:@argtypes[41]),// wl_pointer
  (name:'motion'; signature:'uff'; types:@argtypes[43]),// wl_pointer
  (name:'button'; signature:'uuuu'; types:@argtypes[46]),// wl_pointer
  (name:'axis'; signature:'uuf'; types:@argtypes[50]),// wl_pointer
  (name:'frame'; signature:''; types:nil),// wl_pointer
  (name:'axis_source'; signature:'u'; types:@argtypes[53]),// wl_pointer
  (name:'axis_stop'; signature:'uu'; types:@argtypes[54]),// wl_pointer
  (name:'axis_discrete'; signature:'ui'; types:@argtypes[56]),// wl_pointer
  (name:'axis_value120'; signature:'ui'; types:@argtypes[58]),// wl_pointer
  (name:'axis_relative_direction'; signature:'uu'; types:@argtypes[60]),// wl_pointer
  (name:'keymap'; signature:'uhu'; types:@argtypes[62]),// wl_keyboard
  (name:'enter'; signature:'uoa'; types:@argtypes[65]),// wl_keyboard
  (name:'leave'; signature:'uo'; types:@argtypes[68]),// wl_keyboard
  (name:'key'; signature:'uuuu'; types:@argtypes[70]),// wl_keyboard
  (name:'modifiers'; signature:'uuuuu'; types:@argtypes[74]),// wl_keyboard
  (name:'repeat_info'; signature:'ii'; types:@argtypes[79]),// wl_keyboard
  (name:'down'; signature:'uuoiff'; types:@argtypes[81]),// wl_touch
  (name:'up'; signature:'uui'; types:@argtypes[87]),// wl_touch
  (name:'motion'; signature:'uiff'; types:@argtypes[90]),// wl_touch
  (name:'frame'; signature:''; types:nil),// wl_touch
  (name:'cancel'; signature:''; types:nil),// wl_touch
  (name:'shape'; signature:'iff'; types:@argtypes[94]),// wl_touch
  (name:'orientation'; signature:'if'; types:@argtypes[97]),// wl_touch
  (name:'geometry'; signature:'iiiiissi'; types:@argtypes[99]),// wl_output
  (name:'mode'; signature:'uiii'; types:@argtypes[107]),// wl_output
  (name:'done'; signature:''; types:nil),// wl_output
  (name:'scale'; signature:'i'; types:@argtypes[111]),// wl_output
  (name:'name'; signature:'s'; types:@argtypes[112]),// wl_output
  (name:'description'; signature:'s'; types:@argtypes[113])// wl_output
);

procedure initifs();
begin
  wl_display_interface := @wl_display_ifacedat;
  wl_registry_interface := @wl_registry_ifacedat;
  wl_callback_interface := @wl_callback_ifacedat;
  wl_compositor_interface := @wl_compositor_ifacedat;
  wl_shm_pool_interface := @wl_shm_pool_ifacedat;
  wl_shm_interface := @wl_shm_ifacedat;
  wl_buffer_interface := @wl_buffer_ifacedat;
  wl_data_offer_interface := @wl_data_offer_ifacedat;
  wl_data_source_interface := @wl_data_source_ifacedat;
  wl_data_device_interface := @wl_data_device_ifacedat;
  wl_data_device_manager_interface := @wl_data_device_manager_ifacedat;
  wl_shell_interface := @wl_shell_ifacedat;
  wl_shell_surface_interface := @wl_shell_surface_ifacedat;
  wl_surface_interface := @wl_surface_ifacedat;
  wl_seat_interface := @wl_seat_ifacedat;
  wl_pointer_interface := @wl_pointer_ifacedat;
  wl_keyboard_interface := @wl_keyboard_ifacedat;
  wl_touch_interface := @wl_touch_ifacedat;
  wl_output_interface := @wl_output_ifacedat;
  wl_region_interface := @wl_region_ifacedat;
  wl_subcompositor_interface := @wl_subcompositor_ifacedat;
  wl_subsurface_interface := @wl_subsurface_ifacedat;
  wl_display_ifacedat.methods := @requests[0];
  wl_registry_ifacedat.methods := @requests[2];
  wl_callback_ifacedat.methods := nil;
  wl_compositor_ifacedat.methods := @requests[3];
  wl_shm_pool_ifacedat.methods := @requests[5];
  wl_shm_ifacedat.methods := @requests[8];
  wl_buffer_ifacedat.methods := @requests[9];
  wl_data_offer_ifacedat.methods := @requests[10];
  wl_data_source_ifacedat.methods := @requests[15];
  wl_data_device_ifacedat.methods := @requests[18];
  wl_data_device_manager_ifacedat.methods := @requests[21];
  wl_shell_ifacedat.methods := @requests[23];
  wl_shell_surface_ifacedat.methods := @requests[24];
  wl_surface_ifacedat.methods := @requests[34];
  wl_seat_ifacedat.methods := @requests[45];
  wl_pointer_ifacedat.methods := @requests[49];
  wl_keyboard_ifacedat.methods := @requests[51];
  wl_touch_ifacedat.methods := @requests[52];
  wl_output_ifacedat.methods := @requests[53];
  wl_region_ifacedat.methods := @requests[54];
  wl_subcompositor_ifacedat.methods := @requests[57];
  wl_subsurface_ifacedat.methods := @requests[59];
  wl_display_ifacedat.events := @events[0];
  wl_registry_ifacedat.events := @events[2];
  wl_callback_ifacedat.events := @events[4];
  wl_compositor_ifacedat.events := nil;
  wl_shm_pool_ifacedat.events := nil;
  wl_shm_ifacedat.events := @events[5];
  wl_buffer_ifacedat.events := @events[6];
  wl_data_offer_ifacedat.events := @events[7];
  wl_data_source_ifacedat.events := @events[10];
  wl_data_device_ifacedat.events := @events[16];
  wl_data_device_manager_ifacedat.events := nil;
  wl_shell_ifacedat.events := nil;
  wl_shell_surface_ifacedat.events := @events[22];
  wl_surface_ifacedat.events := @events[25];
  wl_seat_ifacedat.events := @events[29];
  wl_pointer_ifacedat.events := @events[31];
  wl_keyboard_ifacedat.events := @events[42];
  wl_touch_ifacedat.events := @events[48];
  wl_output_ifacedat.events := @events[55];
  wl_region_ifacedat.events := nil;
  wl_subcompositor_ifacedat.events := nil;
  wl_subsurface_ifacedat.events := nil;
end;


begin
  initifs();
end.

