{$mode objfpc}
unit paswl_bindable;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_basebindable,paswl_basecollection;


type
Tpaswl_bindable=class;
Tpaswl_bindable_cls = class of Tpaswl_bindable;
Tpaswl_bindable = class(Tpaswl_basebindable)
  public
  constructor create(); override;
  function make_child(c : Tpaswl_bindable_cls; newwlo : wl_object; accept_nowlo : boolean = false) : Tpaswl_bindable;
  
  function onRegistryRemove() : boolean; virtual; // true: "destroyme"
  
  public
  collection : Tpaswl_basecollection;
end;





implementation


constructor Tpaswl_bindable.create();
begin
end;

function Tpaswl_bindable.make_child(c : Tpaswl_bindable_cls; newwlo : wl_object; accept_nowlo : boolean = false) : Tpaswl_bindable;
var o : Tpaswl_bindable;
begin
//writeln('make child');
  o := nil;
  if (accept_nowlo)or(newwlo<>nil) then begin
    o := c.create();
    //writeln('made child of tpye ',o.classname);
    if o<>nil then begin
      o.display := display;
      o.wlo := newwlo;
      o.wlversion := wlversion; // inherit version from creator
      o.collection := collection;
      if o.wlo<>nil then o.onBind();
    end;
  end;
  make_child := o;
end;

function Tpaswl_bindable.onRegistryRemove() : boolean;
begin
  writeln(classname,' received onRegistryRemove()');
  collection.alldone := true;
  onRegistryRemove := false;
end;

initialization
begin
end;

finalization
begin
end;

end.



