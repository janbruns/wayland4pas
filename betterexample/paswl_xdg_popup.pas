{$mode objfpc}
unit paswl_xdg_popup;

interface
uses 
  wl_basic_types,
  wl_core,
  wl_libwl,
  wl_xdgshell,
  paswl_bindable,
  paswl_seat,
  paswl_xdg_positioner;


type

Tpaswl_xdg_popup=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  procedure grab(seat : Tpaswl_seat; serial : dword);
  procedure reposition(positioner : Tpaswl_xdg_positioner; token : dword);


  protected
  procedure cb_xdg_popup_configure(x, y, width, height : longint); virtual;
  procedure cb_xdg_popup_popup_done(); virtual;
  procedure cb_xdg_popup_repositioned(token : dword); virtual;
  

  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_xdg_popup_cls = class of Tpaswl_xdg_popup;






implementation




procedure xdg_popup_configure(dat:pointer; o:xdg_popup; x, y, width, height : longint); cdecl;
var d : Tpaswl_xdg_popup;
begin
  if dat<>nil then begin
    d := Tpaswl_xdg_popup(dat);
    if d.wlo=o then d.cb_xdg_popup_configure(x, y, width, height)
    else ;//
  end;
end;

procedure xdg_popup_popup_done(dat:pointer; o:xdg_popup); cdecl;
var d : Tpaswl_xdg_popup;
begin
  if dat<>nil then begin
    d := Tpaswl_xdg_popup(dat);
    if d.wlo=o then d.cb_xdg_popup_popup_done()
    else ;//
  end;
end;

procedure xdg_popup_repositioned(dat:pointer; o:xdg_popup; token : dword); cdecl;
var d : Tpaswl_xdg_popup;
begin
  if dat<>nil then begin
    d := Tpaswl_xdg_popup(dat);
    if d.wlo=o then d.cb_xdg_popup_repositioned(token)
    else ;//
  end;
end;


var
imp_if_xdg_popup_listener : xdg_popup_listener = (
  configure : @xdg_popup_configure;
  popup_done : @xdg_popup_popup_done;
  repositioned : @xdg_popup_repositioned;
);





procedure Tpaswl_xdg_popup.cb_xdg_popup_configure(x, y, width, height : longint);
begin
end;
procedure Tpaswl_xdg_popup.cb_xdg_popup_popup_done();
begin
end;
procedure Tpaswl_xdg_popup.cb_xdg_popup_repositioned(token : dword);
begin
end;


procedure Tpaswl_xdg_popup.grab(seat : Tpaswl_seat; serial : dword);
begin
  xdg_popup_grab(xdg_popup(wlo),wl_seat(seat.wlo),serial);
end;

procedure Tpaswl_xdg_popup.reposition(positioner : Tpaswl_xdg_positioner; token : dword);
begin
  xdg_popup_reposition(xdg_popup(wlo),xdg_positioner(positioner.wlo),token);
end;



constructor Tpaswl_xdg_popup.create();
begin
  inherited create();
end;

destructor Tpaswl_xdg_popup.destroy();
begin
  xdg_popup_destroy(xdg_popup(wlo));
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_xdg_popup.onBind();
begin
  inherited;
  xdg_popup_add_listener(xdg_popup(wlo),imp_if_xdg_popup_listener,self);
end;

procedure Tpaswl_xdg_popup.onUnbind();
begin
  inherited;
end;

class function Tpaswl_xdg_popup.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := xdg_popup_interface;
end;

class function Tpaswl_xdg_popup.wl_ifver() : dword;
begin
  wl_ifver := 6;
end;


initialization
begin
end;

finalization
begin
end;

end.



