{$mode objfpc}
unit memfdmgr;

interface
uses baseunix;



type
Tfd=cint;
Ttmpfiledescr=class
  public
  destructor destroy(); override; // doesn't call fpclose on fd
  
  function memFdByTempdir(s : TSize) : Tfd;
  function memFdByTempFile(s : TSize) : Tfd;
  
  procedure grow(s : TSize);
  
  public
  fn : ansistring;
  fd : Tfd;
  sz : TSize;
end;

function getMemFd(s : TSize) : Ttmpfiledescr;



implementation
uses sysutils;

const
{$IFDEF LINUX}
initial_allow_tmpdir = true;
{$ELSE}
initial_allow_tmpdir = false;
{$ENDIF}

const
__O_TMPFILE = &020000000; // check if depends on cpu
O_TMPFILE = __O_TMPFILE or O_DIRECTORY;


function Ttmpfiledescr.memFdByTempdir(s : TSize) : Tfd;
begin
  fn := GetTempDir();
  fd := fpOpen(fn,O_RdWr or O_TMPFILE);
  if fd>0 then begin
    sz := s;
    FpFtruncate(fd,sz);
  end;
  fn := '';
  memFdByTempdir := fd;
end;


function Ttmpfiledescr.memFdByTempFile(s : TSize) : Tfd;
begin
  fn := getTempfilename();
  fd := fpOpen(fn,O_RdWr or O_Creat);
  if fd>=0 then begin
    sz := s;
    FpFtruncate(fd,sz);
  end else fn := '';
  memFdByTempFile := fd;
end;

destructor Ttmpfiledescr.destroy();
begin
  if fn<>'' then FpUnlink(fn);
end;

procedure Ttmpfiledescr.grow(s : TSize);
begin
  if s>sz then begin
    FpFtruncate(fd,s);
    sz := s;
  end;
end;






var
allow_tmpdir : boolean;

function getMemFd(s : TSize) : Ttmpfiledescr;
var o : Ttmpfiledescr;
begin
  o := Ttmpfiledescr.create();
  o.fd := -1;
  if allow_tmpdir then begin
    o.memFdByTempdir(s);
  end;
  if o.fd<0 then begin
    allow_tmpdir := false;
    o.memFdByTempFile(s);
  end;
  if o.fd<0 then begin
    o.fn := '';
    o.destroy();
    o := nil;
  end;
  getMemFd := o;
end;






initialization
begin
  allow_tmpdir := initial_allow_tmpdir;
end;

finalization
begin
end;

end.



