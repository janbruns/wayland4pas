unit wl_iflist;

interface
uses wl_basic_types;


{ The wayland protocol registry interface exposes objects
  that can be instantiated by the application through
  an event called "global". The application can then
  instaciate those objects by sending wl_registry.bind()
  requests. In order to do that, libwayland-client expects
  the application to tell about interface-pointers for
  the libwayland-clent implementation of wl_registry.bind().

  These interface-pointers come to the application by means
  of linking (or as done here: by dynlib-loading), even if
  such interfaces are implemented in libwayland-client.

  So here's some code to manage known interfaces inorder to 
  not have fresh dynloading-code in any application we write,
  and have a mechanism to lookup interfaces from string names.
}

function wl_getImportedInterface(n : Pchar) : Pwl_interface;

procedure wl_addImportedInterface(n : Pchar; i : Pwl_interface; v : dword);


implementation
uses strings;

type
Pknown_wl_interface = ^Tknown_wl_interface;
Tknown_wl_interface = record
  name : Pchar;
  wli : Pwl_interface;
  next : Pknown_wl_interface;
  v : dword;
end;

var
known_wl_interfaces : Pknown_wl_interface = nil;


function wl_getImportedInterface(n : Pchar) : Pwl_interface;
var p : Pknown_wl_interface;
begin
  p := known_wl_interfaces;
  while p<>nil do begin
    if strcomp(n,p^.name)=0 then begin
      wl_getImportedInterface := p^.wli;
      exit;
    end;
    p := p^.next;
  end;
  wl_getImportedInterface := nil;
end;

procedure wl_addImportedInterface(n : Pchar; i : Pwl_interface; v : dword);
var p : Pknown_wl_interface;
begin
writeln('add interface for ',n,' test=',ppchar(i)^);
  new(p);
  p^.name := n;
  p^.wli := i;
  p^.v := v;
  p^.next := known_wl_interfaces;
  known_wl_interfaces := p;
end;

procedure cleanup();
var p : Pknown_wl_interface;
begin
  while known_wl_interfaces<>nil do begin
    p := known_wl_interfaces^.next;
    dispose(known_wl_interfaces);
    known_wl_interfaces := p;
  end;
end;


finalization cleanup();

end.

