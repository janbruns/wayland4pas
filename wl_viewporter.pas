{$mode objfpc}
unit wl_viewporter; //auto-generated


{$PACKRECORDS C}


interface
uses wl_basic_types,wl_libwl,wl_core;


type
wp_viewporter = class(wl_object) end;
wp_viewport = class(wl_object) end;



var
wp_viewporter_interface : Pwl_interface;
wp_viewport_interface : Pwl_interface;

procedure wp_viewporter_destroy(o : wp_viewporter);
function wp_viewporter_get_viewport(o : wp_viewporter; surface : wl_surface) : wp_viewport;
procedure wp_viewport_destroy(o : wp_viewport);
procedure wp_viewport_set_source(o : wp_viewport; x, y, width, height : Twlfixed);
procedure wp_viewport_set_destination(o : wp_viewport; width, height : longint);

type
wp_viewporter_listener = record
end;

wp_viewport_listener = record
end;



procedure wp_viewporter_add_listener(o:wp_viewporter; var l:wp_viewporter_listener; dat:pointer);
procedure wp_viewport_add_listener(o:wp_viewport; var l:wp_viewport_listener; dat:pointer);


const
wp_viewporter_error_viewport_exists = $00000000;
wp_viewport_error_bad_value = $00000000;
wp_viewport_error_bad_size = $00000001;
wp_viewport_error_out_of_buffer = $00000002;
wp_viewport_error_no_surface = $00000003;


var
wp_viewporter_ifacedat : Twl_interface = (name:'wp_viewporter'; version:1; method_count:2; methods:nil; event_count:0; events:nil); 
wp_viewport_ifacedat : Twl_interface = (name:'wp_viewport'; version:1; method_count:3; methods:nil; event_count:0; events:nil); 


implementation



procedure wp_viewporter_add_listener(o:wp_viewporter; var l:wp_viewporter_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;

procedure wp_viewport_add_listener(o:wp_viewport; var l:wp_viewport_listener; dat:pointer);
begin
  wl_proxy_add_listener(o,@l,dat);
end;



procedure wp_viewporter_destroy(o : wp_viewporter);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

function wp_viewporter_get_viewport(o : wp_viewporter; surface : wl_surface) : wp_viewport;
var parm : Tparamarr2;
begin
  parm[0].o := nil;
  parm[1].o := surface;
  wp_viewporter_get_viewport := wp_viewport(wl_proxy_marshal_array_constructor(o,1,@parm[0],wp_viewport_interface));
end;

procedure wp_viewport_destroy(o : wp_viewport);
begin
  wl_proxy_marshal_array(o,0,nil);
end;

procedure wp_viewport_set_source(o : wp_viewport; x, y, width, height : Twlfixed);
var parm : Tparamarr4;
begin
  parm[0].f := x;
  parm[1].f := y;
  parm[2].f := width;
  parm[3].f := height;
  wl_proxy_marshal_array(o,1,@parm[0]);
end;

procedure wp_viewport_set_destination(o : wp_viewport; width, height : longint);
var parm : Tparamarr2;
begin
  parm[0].i := width;
  parm[1].i := height;
  wl_proxy_marshal_array(o,2,@parm[0]);
end;


var
reqtypes : array[0..7] of Pwl_interface = (
  // 0:wp_viewporter.destroy
  @wp_viewport_ifacedat, @wl_surface_ifacedat, // 0:wp_viewporter.get_viewport
  // 2:wp_viewport.destroy
  nil, nil, nil, nil, // 2:wp_viewport.set_source
  nil, nil// 6:wp_viewport.set_destination
);



requests : array[0..4] of Twl_message = (
  (name:'destroy'; signature:''; types:nil),// wp_viewporter
  (name:'get_viewport'; signature:'no'; types:@reqtypes[0]),// wp_viewporter
  (name:'destroy'; signature:''; types:nil),// wp_viewport
  (name:'set_source'; signature:'ffff'; types:@reqtypes[2]),// wp_viewport
  (name:'set_destination'; signature:'ii'; types:@reqtypes[6])// wp_viewport
);



procedure initifs();
begin
  wp_viewporter_interface := @wp_viewporter_ifacedat;
  wp_viewport_interface := @wp_viewport_ifacedat;
  wp_viewporter_ifacedat.methods := @requests[0];
  wp_viewport_ifacedat.methods := @requests[2];
  wp_viewporter_ifacedat.events := nil;
  wp_viewport_ifacedat.events := nil;
end;


begin
  initifs();
end.

