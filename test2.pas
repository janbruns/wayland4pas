{$mode objfpc}
uses xdgshell,wl_basic_types,wl_core,wl_libwl,strings,math, baseunix;


const
aWIDTH = 600;
aHEIGHT = 400;
CURSOR_WIDTH = 80;
CURSOR_HEIGHT = 80;
CURSOR_HOT_SPOT_X=40;
CURSOR_HOT_SPOT_Y=40;

var
display : wl_display;
registry : wl_registry;
compositor : wl_compositor;
mouseptr : wl_pointer;
seat : wl_seat;
shell : wl_shell;
xdg : xdg_wm_base;
shm : wl_shm;

alldone : boolean;


type
Ppool_data = ^Tpool_data;
Tpool_data = record
  m : Pdword;
  fd : Tcint;
  capacity, size : dword;
end;
Tpixel = dword;


function hello_create_memory_pool(f : Tcint) : wl_shm_pool;
var dat : Ppool_data; pool : wl_shm_pool; tat : stat;
begin
  hello_create_memory_pool := nil;
  if (FPfstat(f,tat) <> 0) then begin
    writeln('stat failed');
    exit;
  end;
  new(dat);
  dat^.capacity := tat.st_size;
  dat^.size := 0;
  dat^.fd := f;
  dat^.m := Fpmmap(nil, dat^.capacity, PROT_READ, MAP_SHARED, dat^.fd, 0);
  if (dat^.m = MAP_FAILED) then begin
    writeln('map failed');
    dispose(dat);
    exit;
  end else begin
    pool := wl_shm_create_pool(shm, dat^.fd, dat^.capacity);
    if (pool=nil) then begin
      writeln('wl_shm_create_pool failed');
      Fpmunmap(dat^.m, dat^.capacity);
      dispose(dat);
      exit;
    end;
    wl_proxy_set_user_data(pool, dat);
    hello_create_memory_pool := pool;
  end;
end;

procedure hello_free_memory_pool(pool : wl_shm_pool);
var dat : Ppool_data;
begin
  dat := wl_proxy_get_user_data(pool);
  wl_shm_pool_destroy(pool);
  Fpmunmap(dat^.m, dat^.capacity);
  dispose(dat);
end;









function hello_create_buffer(pool : wl_shm_pool; width, height : dword) : wl_buffer;
var pd : Ppool_data; b : wl_buffer;
begin
  hello_create_buffer := nil;
  pd := wl_proxy_get_user_data(pool);
  b  := wl_shm_pool_create_buffer(pool, pd^.size, width, height, width*sizeof(Tpixel), wl_shm_format_argb8888);
  if (b=nil) then exit;
  pd^.size += width*height*sizeof(Tpixel);
  hello_create_buffer := b;
end;

procedure hello_free_buffer(b: wl_buffer);
begin
  wl_buffer_destroy(b);
end;

procedure shell_surface_ping(dat:pointer; shs : wl_shell_surface; serial : dword); cdecl;
begin
  wl_shell_surface_pong(shs, serial);
end;

procedure shell_surface_configure(dat:pointer; shs : wl_shell_surface; edges:dword; width,height : longint); cdecl;
begin
end;

procedure shell_popup_done(dat:pointer; o:wl_shell_surface); cdecl;
begin
end;

var sh_surf_listnr : wl_shell_surface_listener = (
  ping:@shell_surface_ping;
  configure:@shell_surface_configure;
  popup_done:@shell_popup_done
);


function hello_create_surface() : wl_shell_surface;
var surf : wl_surface; shs : wl_shell_surface;
begin
  hello_create_surface := nil;
  surf := wl_compositor_create_surface(compositor);
  if (surf=nil) then exit;
  shs := wl_shell_get_shell_surface(shell, surf);
  if (shs=nil) then begin
    wl_surface_destroy(surf);
    exit;
  end;
  wl_shell_surface_add_listener(shs,sh_surf_listnr,nil);
  wl_shell_surface_set_toplevel(shs);
  wl_proxy_set_user_data(shs, surf);
  wl_proxy_set_user_data(surf, nil);
  hello_create_surface := shs;
end;

procedure xdg_surface_config(dat:pointer; o:xdg_surface; serial : dword); cdecl;
begin
  writeln('xdg_surface_config');
  xdg_surface_ack_configure(o,serial);  
end;

procedure xdgbaseping(dat:pointer; o:xdg_wm_base; serial : dword); cdecl;
begin
  writeln('xdg_base_ping');
  xdg_wm_base_pong(xdg, serial);
end;

var xdg_surf_listnr : xdg_surface_listener = (
  configure:@xdg_surface_config;
);

var xdg_base_listnr : xdg_wm_base_listener = (
  ping:@xdgbaseping;
);


function hello_create_xdgsurface() : xdg_surface;
var surf : wl_surface; xdgs : xdg_surface;
begin
  hello_create_xdgsurface := nil;
  surf := wl_compositor_create_surface(compositor);
  if (surf=nil) then exit;
  xdgs := xdg_wm_base_get_xdg_surface(xdg, surf);
  if (xdgs=nil) then begin
    wl_surface_destroy(surf);
    exit;
  end;
  xdg_surface_add_listener(xdgs,xdg_surf_listnr,nil);
  wl_display_roundtrip(display);
  writeln('roundtrip after xdg_surface_add_listener done'); 

//  wl_shell_surface_set_toplevel(shs);
  wl_proxy_set_user_data(xdgs, surf);
  wl_proxy_set_user_data(surf, nil);
  
  hello_create_xdgsurface := xdgs;
end;



procedure hello_free_surface(shs : wl_shell_surface);
var surf : wl_surface;
begin
  surf := wl_surface( wl_proxy_get_user_data(shs) );
  wl_surface_destroy(surf);
end;


procedure hello_free_surface_xdg(xdgs : xdg_surface);
var surf : wl_surface;
begin
  surf := wl_surface( wl_proxy_get_user_data(xdgs) );
  wl_surface_destroy(surf);
end;




procedure xdgtl_conf(dat:pointer; o:xdg_toplevel; width, height : longint; states : wl_array); cdecl;
var i : longint;
begin
  write('xdtl_conf(',width,',',height,'): [ ');
  for i := 0 to states^.size-1 do write(hexstr(states^.data[i],2),' ');
  writeln(']');
end;

procedure xdgtl_close(dat:pointer; o:xdg_toplevel); cdecl;
begin
  writeln('xdtl_close');
  alldone := true;
end;

procedure xdgtl_confbounds(dat:pointer; o:xdg_toplevel; width, height : longint); cdecl;
begin
  writeln('xdtl_confbounds');
end;

procedure xdgtl_wmcaps(dat:pointer; o:xdg_toplevel; capabilities : wl_array); cdecl;
begin
  writeln('xdtl_wmcaps');
end;


var
xdgtllistnr : xdg_toplevel_listener = (
  configure : @xdgtl_conf;
  close : @xdgtl_close;
  configure_bounds : @xdgtl_confbounds;
  wm_capabilities : @xdgtl_wmcaps
);











procedure hello_bind_buffer(b: wl_buffer; shs : wl_shell_surface);
var surf : wl_surface;
begin
  surf := wl_surface( wl_proxy_get_user_data(shs) );
  wl_surface_attach(surf, b, 0, 0);
  wl_surface_commit(surf);
end;

procedure hello_bind_buffer_xdg(b: wl_buffer; shs : xdg_surface);
var surf : wl_surface;
begin
  surf := wl_surface( wl_proxy_get_user_data(shs) );
  wl_surface_attach(surf, b, 0, 0);
  wl_surface_commit(surf);
end;

procedure hello_set_button_callback(shs : wl_shell_surface;  cb : pointer );
var surf : wl_surface;
begin
  surf := wl_surface( wl_proxy_get_user_data(shs) );
  wl_proxy_set_user_data(surf, cb);
end;

procedure hello_set_button_callback_xdg(shs : xdg_surface;  cb : pointer );
var surf : wl_surface;
begin
  surf := wl_surface( wl_proxy_get_user_data(shs) );
  wl_proxy_set_user_data(surf, cb);
end;



type pointer_data = record
  surf : wl_surface;
  buff : wl_buffer;
  hot_spot_x, hot_spot_y : longint;
  target_surface : wl_surface;
end;

procedure hello_set_cursor_from_pool(pool : wl_shm_pool; width, height : dword; spot_x,spot_y : longint);
var dat : ^pointer_data;
begin
  new(dat);
  dat^.hot_spot_x := spot_x;
  dat^.hot_spot_y := spot_y;
  dat^.surf := wl_compositor_create_surface(compositor);
  if (dat^.surf=nil) then begin
    dispose(dat);
  end else begin
    dat^.buff := hello_create_buffer(pool, width, height);
    if (dat^.buff=nil) then begin
      wl_surface_destroy(dat^.surf);
    end else begin
      wl_proxy_set_user_data(mouseptr, dat);
      exit;
    end;
    dispose(dat);
  end;
end;

procedure hello_free_cursor();
var dat : ^pointer_data;
begin 
  dat := wl_proxy_get_user_data(mouseptr);
  wl_buffer_destroy(dat^.buff);
  wl_surface_destroy(dat^.surf);
  dispose(dat);
  wl_proxy_set_user_data(mouseptr, nil);
end;


procedure mptr_enter(dat:pointer; o:wl_pointer; serial : dword; surface : wl_surface; surface_x, surface_y : Twlfixed); cdecl;
var pd : ^pointer_data;
begin
  pd := wl_proxy_get_user_data(o);
  pd^.target_surface := wl_surface( surface );
  wl_surface_attach(pd^.surf, pd^.buff, 0, 0);
  wl_surface_commit(pd^.surf);
  wl_pointer_set_cursor(o, serial, pd^.surf, pd^.hot_spot_x, pd^.hot_spot_y);
end;

procedure mptr_leave(dat:pointer; o:wl_pointer; serial : dword; surface : wl_surface); cdecl;
begin
end;

procedure mptr_motion(dat:pointer; o:wl_pointer; time : dword; surface_x, surface_y : Twlfixed); cdecl;
begin
end;

type Tcb1=procedure(d : dword);

procedure mptr_button(dat:pointer; o:wl_pointer; serial, time, button, state : dword); cdecl;
var pd : ^pointer_data; cb : Tcb1;
begin
  pd := wl_proxy_get_user_data(o);
  cb := Tcb1( wl_proxy_get_user_data(pd^.target_surface) );
  if (cb<>nil) then cb(button);
end;

procedure mptr_axis(dat:pointer; o:wl_pointer; time, axis : dword; value : Twlfixed); cdecl;
begin
end;

procedure mptr_frame(dat:pointer; o:wl_pointer); cdecl;
begin
end;

procedure mptr_axis_source(dat:pointer; o:wl_pointer; axis_source : dword); cdecl;
begin
end;

procedure mptr_axis_stop(dat:pointer; o:wl_pointer; time, axis : dword); cdecl;
begin
end;

procedure mptr_axis_discrete(dat:pointer; o:wl_pointer; axis : dword; discrete : longint); cdecl;
begin
end;

procedure mptr_axis_value120(dat:pointer; o:wl_pointer; axis : dword; value120 : longint); cdecl;
begin
end;

procedure mptr_axis_relative_direction(dat:pointer; o:wl_pointer; axis, direction : dword); cdecl;
begin
end;


var mouseptrcbs : wl_pointer_listener = (
  enter:@mptr_enter;
  leave:@mptr_leave;
  motion:@mptr_motion;
  button:@mptr_button;
  axis:@mptr_axis;
  frame:@mptr_frame;
  axis_source:@mptr_axis_source;
  axis_stop:@mptr_axis_stop;
  axis_discrete:@mptr_axis_discrete;
  axis_value120 : @mptr_axis_value120;
  axis_relative_direction : @mptr_axis_relative_direction;
);


procedure regglobal(dat:pointer; o:wl_registry; name : dword; iface : Pchar; version : dword); cdecl;
begin
  writeln('Global: ',hexstr(name,8), ' data=',hexstr(ptruint(dat),16),' version: ',version,' inf=',iface);
  if 0=strcomp(iface,wl_compositor_interface^.name) then begin
    compositor := wl_compositor( wl_registry_bind(registry,  wl_compositor_interface, min(wl_compositor_interface^.version,version), name) );
  end;
  if 0=strcomp(iface,wl_shm_interface^.name) then begin
    shm := wl_shm( wl_registry_bind(registry,  wl_shm_interface, min(wl_shm_interface^.version,version), name) );
  end;
  if 0=strcomp(iface,xdg_wm_base_interface^.name) then begin
    xdg := xdg_wm_base( wl_registry_bind(registry,  xdg_wm_base_interface, min(xdg_wm_base_interface^.version,version), name) );
    if (xdg<>nil) then begin
      xdg_wm_base_add_listener(xdg,xdg_base_listnr,nil);
    end;
  end;
  if 0=strcomp(iface,wl_shell_interface^.name) then begin
    shell := wl_shell( wl_registry_bind(registry,  wl_shell_interface, min(wl_shell_interface^.version,version), name) );
  end;
  if 0=strcomp(iface,wl_seat_interface^.name) then begin
    seat := wl_seat( wl_registry_bind(registry,  wl_seat_interface, min(wl_seat_interface^.version,version), name) );
    mouseptr := wl_seat_get_pointer(seat);
    wl_pointer_add_listener(mouseptr, mouseptrcbs, nil);
  end;
end;

procedure regglobalrm(dat:pointer; o:wl_registry; name : dword); cdecl;
begin
end;

var regcbs : wl_registry_listener = (global:@regglobal; global_remove:@regglobalrm);




procedure on_button(d : dword);
begin
  alldone := true;
end;

procedure dotest();
var pool : wl_shm_pool; buffer : wl_buffer; xsurf : xdg_surface; surface : wl_shell_surface; image : Tcint; xdgtl : xdg_toplevel;
begin
  display := wl_display_connect(nil);
  if display<>nil then begin
    registry := wl_display_get_registry(display);
    writeln('reg=',hexstr(ptruint(registry),16));
    wl_registry_add_listener(registry,regcbs,pointer($1234123456567878));
    wl_display_roundtrip(display);
writeln('roundtrip done'); 

    //image := FpOpen('./images.bin',O_RDWR);
    image := FpOpen('./bilder.bin',O_RDWR);
    pool := hello_create_memory_pool(image);
writeln('image pool created.');    
    wl_display_roundtrip(display);
writeln('roundtrip done'); 

//    surface := hello_create_surface();
    //wl_shell_surface_set_title(surface,'jans test prog');
    
    xsurf := hello_create_xdgsurface();
    xdgtl := xdg_surface_get_toplevel(xsurf);
    xdg_toplevel_add_listener(xdgtl,xdgtllistnr,nil);
    wl_surface_commit(wl_surface(wl_proxy_get_user_data(xsurf)));
    wl_display_roundtrip(display);
    

writeln('roundtrip done'); 
 
writeln('xsurf created:',hexstr(ptruint(xsurf),16));   
    wl_display_roundtrip(display);
writeln('roundtrip done'); 

     
    buffer := hello_create_buffer(pool, aWIDTH, aHEIGHT);
writeln('buffer created:',hexstr(ptruint(buffer),16));        
    hello_bind_buffer_xdg(buffer,xsurf);
writeln('buffer bound');
   

    xdg_surface_set_window_geometry(xsurf,0,0,aWIDTH, aHEIGHT);
writeln('set win geo');
    
writeln('toplevel created:',hexstr(ptruint(xdgtl),16));        
    
    wl_display_roundtrip(display);
writeln('roundtrip done'); 


    
    
    xdg_toplevel_set_title(xdgtl,'jans test prog');
    xdg_toplevel_set_max_size(xdgtl,aWIDTH, aHEIGHT);



    hello_set_cursor_from_pool(pool, CURSOR_WIDTH, CURSOR_HEIGHT, CURSOR_HOT_SPOT_X, CURSOR_HOT_SPOT_Y);
    hello_set_button_callback_xdg(xsurf, @on_button);

writeln('doing roundtrip before mainloop...');
    wl_display_roundtrip(display);
writeln('roundtrip done'); 

    writeln('enter main loop: ',not(alldone),' press enter');
//    readln;

    while not(alldone) do begin
      if (wl_display_dispatch(display) < 0) then begin
        writeln('"Main loop error"');
        alldone := true;
      end;
    end;

  end else writeln('error: no dosplay. wayland probably not running');
end;

begin
  import_libwlc();
  if libwayland_client_avail then begin
    dotest();
  end else writeln('missing libwayland');
end.
