
Header-units for usng core wayland protocol with freepascal.

The following units are included:

wl_basic_types.pas  // real basic types
wl_iflist.pas       // probably not useful to anyone
wl_libwl.pas        // dynloader for libwayland.client.so
wl_core.pas         // the main thing, generated from wayland.xml using the scanner.pas tool 

And the following additional content:

scanner.pas         // tool to generate things like wl_core.pas

test.pas            // remake of some Hello-World Tutorial
bilder.bin          // Image content for test.pas
screenshot.png      // screenshot from test.pas

readme.txt          // this file


Copyright: From my part, too minimal to mention anything here.

Note: from protocol updates (included using the scanner tool), the API might change in a way that requires application code updates (specificly for the event-listener records), so it's probably best to copy the actual units to the app-src.  

Gruss

Jan Bruns

code@abnuto.de

