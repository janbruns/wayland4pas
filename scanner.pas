{$mode objfpc}
uses XMLRead,dom;

type
Twlarg = class
  next : Twlarg;
  name : UnicodeString;
  typ  : UnicodeString;
  aif  : UnicodeString;
  allownull : UnicodeString;
  description, descr : UnicodeString;
  node : TDOMNode;
  v : int64;
  idx : longint;
end;

Twlarglist = class
  public
  procedure read_arglist(what : UnicodeString; n : TDOMNode);
  function arglist_to_string() : UnicodeString;
  procedure check_arglist_needs_constructor();

  public
  firstarg, curarg : Twlarg;

  description, descr : UnicodeString;
  next : Twlarglist;
  name : UnicodeString;
  bitfield : UnicodeString;
  node : TDOMNode;
  since : longint;
  idx : longint;

  num_args : longint;
  needs_constructor_if,
  needs_constructor   : boolean;
  arg_with_new_id : Twlarg;
  ifpart,imppart : UnicodeString;

  eidx,ridx : longint;

end;


Twlinterf = class
  constructor create();
  procedure read_eventreqests(what : UnicodeString; var fr,cr : Twlarglist);
  procedure print_req(var f : text; withimp,implocals : boolean);
  procedure print_ev(var f : text);
  procedure print_evset(var f : text; withimp : boolean);
  procedure print_enums(var f : text);

  public
  next : Twlinterf;
  name : UnicodeString;
  node : TDOMNode;
  description, descr : UnicodeString;
  
  first_req, cur_req : Twlarglist;
  first_ev,  cur_ev  : Twlarglist;
  first_en,  cur_en  : Twlarglist;
  version : longint;
  firstreqidx,firstevidx : longint;
end;


var
descrwhat,protocolname,protocoldescription,protocoldescr,protocolcopyright : UnicodeString;


procedure getdescr(n : TDOMNode; var l,s : UnicodeString);
var n2 : TDOMNode;
begin
  s := '';
  l := '';
  n := n.firstChild;
  while n<>nil do begin
    if n.nodeName=descrwhat then begin
      l := n.TextContent;
      n2 := n.Attributes.GetNamedItem('summary');
      if n2<>nil then s := n2.NodeValue;
    end;
    n := n.NextSibling;
  end;
end;

constructor Twlinterf.create();
begin
  first_req := nil;
  cur_req := nil;
  first_ev := nil;
  cur_ev := nil;
end;


procedure Twlarglist.read_arglist(what : UnicodeString; n : TDOMNode);
var a : Twlarg; x : longint; n2 : TDOMNode;
begin
  firstarg := nil;
  curarg := nil;
  x := 0;
  getdescr(n,description,descr);
  n := n.firstChild;
  while (n<>nil) do begin
    if n.nodeName=what then begin
      a := Twlarg.create();
      a.node := n;
      n2 := n.Attributes.GetNamedItem('name');
      if (n2<>nil) then a.name := n2.NodeValue;
      if a.name='interface' then a.name := 'iface';
      n2 := n.Attributes.GetNamedItem('type');
      if (n2<>nil) then a.typ  := n2.NodeValue;
      n2 := n.Attributes.GetNamedItem('interface');
      if (n2<>nil) then a.aif  := n2.NodeValue;
      a.v := 0;
      n2 := n.Attributes.GetNamedItem('value');
      if (n2<>nil) then val(n2.NodeValue,a.v);

      n2 := n.Attributes.GetNamedItem('allow-null');
      if (n2<>nil) then a.allownull := n2.NodeValue else a.allownull:='';

      a.idx := x;
      inc(x);
      if (curarg<>nil) then curarg.next := a;
      curarg := a;
      if firstarg=nil then firstarg := a;
      getdescr(n,a.description,a.descr);
      if a.descr='' then begin
        n2 := n.Attributes.GetNamedItem('summary');
        if (n2<>nil) then a.descr := n2.NodeValue;
      end;
    end;
    n := n.NextSibling;
  end;
end;

function Twlarglist.arglist_to_string() : UnicodeString;
var s : UnicodeString;
begin
  s := '(';
  curarg := firstarg;
  while curarg<>nil do begin
    s += curarg.name + ' : ' + curarg.typ + ' = ' +UnicodeString(hexstr(curarg.v,8));
    if (curarg.next<>nil) then s += ', ';
    curarg := curarg.next; 
  end;
  s += ')';
  arglist_to_string := s;
end;



procedure Twlarglist.check_arglist_needs_constructor();
var a : Twlarg;
begin
  needs_constructor_if := false;
  needs_constructor := false;
  arg_with_new_id := nil;
  num_args := 0;
  a := firstarg;
  while a<>nil do begin
    if a.typ='new_id' then begin
      arg_with_new_id := a;
      if needs_constructor then begin
        writeln('multiple new-id args unsuppoted: ',name);
        halt(0);
      end;
      needs_constructor := true;
      if a.aif='' then needs_constructor_if := true;
    end;
    inc(num_args);
    a := a.next;
  end;
end;


procedure Twlinterf.read_eventreqests(what : UnicodeString; var fr,cr : Twlarglist);
var idx : longint; n,n2 : TDOMNode; r : Twlarglist;
begin
  idx := 0;
  fr := nil;
  cr := nil;
  n := node.firstChild;
  while (n<>nil) do begin
    if n.nodeName=what then begin
      r := Twlarglist.create();
      r.node := n;
      n2 := n.Attributes.GetNamedItem('name');
      if n2<>nil then r.name := n2.NodeValue else r.name := '';
      n2 := n.Attributes.GetNamedItem('bitfield');
      if n2<>nil then r.bitfield := n2.NodeValue else r.bitfield := '';
      n2 := n.Attributes.GetNamedItem('since');
      if n2<>nil then val(n2.NodeValue,r.since) else r.since := 0;
      if cr<>nil then cr.next := r;
      cr := r;
      if fr=nil then fr := r;
      r.idx := idx;
      inc(idx);
      if (what='enum')
      then r.read_arglist('entry',n)
      else begin
        r.read_arglist('arg',n);
        r.check_arglist_needs_constructor();
      end;
    end;
    n := n.NextSibling;
  end;
end;




var
current_wlif,first_wlif : Twlinterf;

procedure read_interface(ifnode : TDOMNode);
var wlif : Twlinterf; n2 : TDOMNode; 
begin
  wlif := Twlinterf.create();
  wlif.node := ifnode;
  n2 := ifnode.Attributes.GetNamedItem('name');
  if n2<>nil then wlif.name := n2.NodeValue;
  n2 := ifnode.Attributes.GetNamedItem('version');
  if n2<>nil then val(n2.NodeValue,wlif.version) else wlif.version := 0;
  getdescr(ifnode,wlif.description,wlif.descr);
  if current_wlif<>nil then current_wlif.next := wlif;
  current_wlif := wlif;
  if first_wlif=nil then first_wlif := wlif;
  wlif.read_eventreqests('request',wlif.first_req,wlif.cur_req);
  wlif.read_eventreqests('event'  ,wlif.first_ev ,wlif.cur_ev );
  wlif.read_eventreqests('enum'  ,wlif.first_en ,wlif.cur_en );

end;

procedure read_interfaces(protocolnode : TDOMNode);
var n : TDOMNode; 
begin
  descrwhat := 'copyright';
  getdescr(protocolnode,protocolcopyright,protocoldescr);
  descrwhat := 'description';
  getdescr(protocolnode,protocoldescription,protocoldescr);
  protocolname := '';
  n := protocolnode.Attributes.GetNamedItem('name');
  if n<>nil then protocolname := n.NodeValue;
  n := protocolnode.firstChild;
  while (n<>nil) do begin
    if n.nodeName='interface' then begin
      read_interface(n);
    end;
    n := n.NextSibling;
  end;
end;

procedure translate_protocolfile(fn : ansistring);
var doc : TXMLDocument;
begin
  ReadXMLFile(doc, fn);
  read_interfaces(doc.DocumentElement);
  doc.destroy();
end;



function count_msgs(x : Twlarglist) : longint;
var  n : longint;
begin
  n := 0;
  while x<>nil do begin
    inc(n);
    x := x.next;
  end; 
  count_msgs := n;
end;












function typ2pash(t,i : UnicodeString) : UnicodeString;
begin
  if (t='int') then typ2pash := 'longint'
  else if (t='uint') then typ2pash := 'dword'
  else if (t='object') then begin
    typ2pash := 'wl_object';
    if i<>'' then typ2pash := i;
  end
  else if (t='string') then typ2pash := 'Pchar'
  else if (t='fixed') then typ2pash := 'Twlfixed'
  else if (t='fd') then typ2pash := 'Tcint'
  else if (t='array') then typ2pash := 'wl_array'
  else if (t='new_id') then typ2pash := i
  else begin
    writeln('unsupported arg type: ',t);
    halt(0);
    //typ2pash := '****UNKNOWN***'+t;
  end;
end;

function typ2parm(t : UnicodeString) : UnicodeString;
begin
  if (t='int') then typ2parm := 'i'
  else if (t='uint') then typ2parm := 'u'
  else if (t='object') then typ2parm := 'o'
  else if (t='string') then typ2parm := 's'
  else if (t='fixed') then typ2parm := 'f'
  else if (t='fd') then typ2parm := 'd'
  else if (t='array') then typ2parm := 'a'
  else begin
    writeln('unsupported parm type: ',t);
    halt(0);
    //typ2pash := '****UNKNOWN***'+t;
  end;
end;

function sametype(t1,t2 : Twlarg) : boolean;
begin
  if t1.typ=t2.typ then begin
    if (t1.typ='object') then begin
      sametype := (t1.aif=t2.aif);
    end else sametype := true;
  end else sametype := false;
end;

function argl2pash(o : UnicodeString; i : Twlinterf; r : Twlarglist; allow_newid : boolean) : UnicodeString;
var s : UnicodeString; a : Twlarg;
begin
  s := o;
  a := r.firstarg;
  while a<>nil do begin
    if allow_newid or (a.typ<>'new_id') then begin
      s += a.name;
      if (a.next<>nil)and sametype(a.next,a) then s += ', '
      else s += ' : ' +typ2pash(a.typ,a.aif) + '; ';
    end;
    a := a.next;
  end;
  delete(s,length(s)-1,2);
  argl2pash := s;
end;

function req2pash(i : Twlinterf; r : Twlarglist) : UnicodeString;
var s,o : UnicodeString;
begin
  if r.needs_constructor then s := 'function ' else s := 'procedure ';
  s += i.name+'_'+r.name;
  o := 'o : '+i.name+'; ';
  if r.needs_constructor_if then o += 'intf : Pwl_interface; version : dword; ';
  s += '('+argl2pash(o,i,r,false)+')';
  if r.needs_constructor then begin
    s += ' : ';
    if r.needs_constructor_if then s += 'wl_object' else s+= r.arg_with_new_id.aif;
  end;
  s += ';';
  req2pash := s;
end;








procedure Twlinterf.print_req(var f : text; withimp,implocals : boolean);
var i,x : longint; a : Twlarg;
begin
  cur_req := first_req;
  while cur_req<>nil do begin
    writeln(f,req2pash(self,cur_req));
    if withimp then begin
      i := 0;
      x := 0;
      if cur_req.needs_constructor_if then x := 2;
      if implocals and (cur_req.num_args>0) then writeln(f,'var parm : Tparamarr',cur_req.num_args+x,';');
      writeln(f,'begin');
      a := cur_req.firstarg;
      while a<>nil do begin
        if a.typ<>'new_id' then begin
          writeln(f,'  parm[',i,'].',typ2parm(a.typ),' := ',a.name,';');
        end else begin
          if cur_req.needs_constructor_if then begin
            writeln(f,'  parm[',i,'].s := intf^.name;'); inc(i);
            writeln(f,'  parm[',i,'].u := version;'); inc(i);
          end;
          writeln(f,'  parm[',i,'].o := nil;');
        end;
        inc(i);
        a := a.next;
      end;
      if cur_req.needs_constructor then begin
        if cur_req.needs_constructor_if then begin
          write(f,'  ',name,'_',cur_req.name,' := wl_object(');
          writeln(f,'wl_proxy_marshal_array_constructor_versioned(o,',cur_req.idx,',@parm[0],intf,version));');
        end else begin
          write(f,'  ',name,'_',cur_req.name,' := ',cur_req.arg_with_new_id.aif,'(');
          writeln(f,'wl_proxy_marshal_array_constructor(o,',cur_req.idx,',@parm[0],',cur_req.arg_with_new_id.aif,'_interface));');
        end;
      end else begin
        if (cur_req.num_args>0) 
        then writeln(f,'  wl_proxy_marshal_array(o,',cur_req.idx,',@parm[0]);') 
        else writeln(f,'  wl_proxy_marshal_array(o,',cur_req.idx,',nil);')
      end;
      writeln(f,'end;');
      writeln(f);
    end;
    cur_req := cur_req.next;
  end;
  
end;



procedure Twlinterf.print_ev(var f : text);
var i : longint; a : Twlarg;
begin
  writeln(f,name,'_listener = record');
  cur_ev := first_ev;
  while cur_ev<>nil do begin
    write(f,'  ',cur_ev.name,' : procedure(dat:pointer; o:',name);
    if cur_ev.firstarg=nil 
    then writeln(f,'); cdecl;')
    else writeln(f,'; ',argl2pash('',self,cur_ev,true),'); cdecl;');
    cur_ev := cur_ev.next;
  end;
  writeln(f,'end;');
  writeln(f);
end;

procedure Twlinterf.print_evset(var f : text; withimp : boolean);
var i : longint; a : Twlarg;
begin
  writeln(f,'procedure ',name,'_add_listener(o:',name,'; var l:',name,'_listener; dat:pointer);');
  if withimp then begin
    writeln(f,'begin');
    writeln(f,'  wl_proxy_add_listener(o,@l,dat);');
    writeln(f,'end;');
    writeln(f);
  end;
end;


procedure Twlinterf.print_enums(var f : text);
var i : longint; a : Twlarg;
begin
  cur_en := first_en;
  while cur_en<>nil do begin
    a := cur_en.firstarg;
    while (a<>nil) do begin
      writeln(f,name,'_',cur_en.name,'_',a.name,' = $',hexstr(a.v,8),';' );
      a := a.next;
    end;
    cur_en := cur_en.next;
  end; 
end;



{

}


procedure make_req_unit();
var f : TEXT; 
begin
  writeln('writing evreq unit to: wl_evreq.pas');
  assign(f,'wl_evreq.pas');
  rewrite(f);

  writeln(f,'{$mode objfpc}');
  writeln(f,'unit wl_evreq; //auto-generated');
  writeln(f);
  writeln(f);
  writeln(f,'{$PACKRECORDS C}');
  writeln(f);
  writeln(f);
  writeln(f,'interface');
  writeln(f,'uses wl_basic_types,wl_libwl_ifs,wl_libwl;');

  writeln(f);

  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    current_wlif.print_req(f,false,false);
    current_wlif := current_wlif.next;
  end;
  writeln(f);
  writeln(f,'type');

  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    current_wlif.print_ev(f);
    current_wlif := current_wlif.next;
  end;
  writeln(f);
  writeln(f);

  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    current_wlif.print_evset(f,false);
    current_wlif := current_wlif.next;
  end;
  writeln(f);
  writeln(f);
  writeln(f,'const');
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    current_wlif.print_enums(f);
    current_wlif := current_wlif.next;
  end;

  writeln(f);
  writeln(f,'implementation');
  writeln(f);
  writeln(f);
  writeln(f);
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    current_wlif.print_evset(f,true);
    current_wlif := current_wlif.next;
  end;
  writeln(f);
  writeln(f);

  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    current_wlif.print_req(f,true,true);
    current_wlif := current_wlif.next;
  end;
  writeln(f);
  writeln(f,'begin');
  writeln(f,'end.');
  writeln(f);
  close(f);
end;





{ ======= generate interface-importer unit


}

procedure write_ifimporter_head(var f : TEXT);
begin
  writeln(f,'{$mode objfpc}');
  writeln(f,'unit wl_libwl_ifs; // auto-generated');
  writeln(f);
  writeln(f,'{$PACKRECORDS C}');
  writeln(f);
  writeln(f);
  writeln(f,'interface');
  writeln(f,'uses wl_basic_types,wl_iflist,dynlibs,math;');
  writeln(f);
  writeln(f,'function import_ifs(lib : TLibHandle) : boolean;');
  writeln(f);
end;

var ifcnt : longint;

procedure write_ifimporter_ifvars(var f : TEXT);
begin
  writeln(f,'var');
  ifcnt := 0;
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    writeln(f,current_wlif.name+'_interface : Pwl_interface;');
    inc(ifcnt);
    current_wlif := current_wlif.next;
  end;
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    writeln(f,current_wlif.name+'_version : dword = ',current_wlif.version,';');
    current_wlif := current_wlif.next;
  end;
  writeln(f);
end;

procedure write_ifimporter_iftyps(var f : TEXT);
begin
  writeln(f,'type');
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    if (current_wlif.name='wl_display') then write(f,'//'); // basic_types.pas
    writeln(f,current_wlif.name,' = class(wl_object) end;');
    current_wlif := current_wlif.next;
  end;
  writeln(f);
end;

procedure write_ifnames(var f : TEXT);
begin
  writeln(f);
  writeln(f,'implementation');
  writeln(f);
  writeln(f,'const');
  writeln(f,'namecnt = ',ifcnt,';');
  writeln(f,'ifnames : array[0..namecnt-1] of Pchar = (');
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    write(f,'  ''',current_wlif.name,'''');
    if current_wlif.next<>nil then writeln(f,',') else writeln(f);
    inc(ifcnt);
    current_wlif := current_wlif.next;
  end;
  writeln(f,');');
  writeln(f);
end;


procedure write_ifload(var f : TEXT);
begin
  writeln(f);
  writeln(f,'function import_if(var i : Pwl_interface; var cnt : longint; var v : dword; lib : TLibHandle) : boolean;');
  writeln(f,'var nif : ansistring;');
  writeln(f,'begin');
  writeln(f,'  inc(cnt);');
  writeln(f,'  nif := ifnames[cnt];');
  writeln(f,'  nif := nif + ''_interface'';');
  writeln(f,'  import_if := true;');
  writeln(f,'  i := GetProcedureAddress(lib, nif);');
  writeln(f,'  if i=nil then exit;');
  writeln(f,'  v := min(v,i^.version);');
  writeln(f,'  wl_addImportedInterface(ifnames[cnt], i, v);');
  writeln(f,'  import_if := false;');
  writeln(f,'end;');
  writeln(f,'');
  writeln(f,'function import_ifs(lib : TLibHandle) : boolean;');
  writeln(f,'var cnt : longint;');
  writeln(f,'begin');
  writeln(f,'  cnt := -1;');
  writeln(f,'  import_ifs := false;');
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    writeln(f,'  if import_if(',current_wlif.name,'_interface, cnt, ',current_wlif.name,'_version, lib) then exit;');
    current_wlif := current_wlif.next;
  end;
  writeln(f,'  import_ifs := true;');
  writeln(f,'end;');
  writeln(f);
  writeln(f,'begin');
  writeln(f,'end.');
  writeln(f);
end;


procedure make_ifimporter();
var f : TEXT;
begin
  writeln('writing interface importer unit to: wl_libwl_ifs.pas');
  assign(f,'wl_libwl_ifs.pas');
  rewrite(f);
  write_ifimporter_head(f);
  write_ifimporter_iftyps(f);
  write_ifimporter_ifvars(f);
  write_ifnames(f);
  write_ifload(f);
  close(f);
end;






procedure write_ifs_head(var f : TEXT);
begin
  writeln(f);
  writeln(f,'var');
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    writeln(f,current_wlif.name,'_interface : Pwl_interface;');
    current_wlif := current_wlif.next;
  end;
  writeln(f);
end;

procedure write_ifs_im(var f : TEXT);
begin
  writeln(f);
  writeln(f,'var');
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    write(f,current_wlif.name,'_ifacedat : Twl_interface = (');
    write(f,'name:''',current_wlif.name,'''; ');
    write(f,'version:',current_wlif.version,'; ');
    write(f,'method_count:',count_msgs(current_wlif.first_req),'; ');
    write(f,'methods:nil; ');
    write(f,'event_count:',count_msgs(current_wlif.first_ev),'; ');
    write(f,'events:nil); ');
    writeln(f);
    current_wlif := current_wlif.next;
  end;
  writeln(f);
end;

var
rcnt : longint = 0;
ecnt : longint = 0;

procedure write_ifs_alltypes(var f : TEXT; ev : boolean);
var l : Twlarglist; a : Twlarg; m,n,n0,x : longint;
begin
  x := 0;
  m := 0;
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    if ev then l := current_wlif.first_ev else l := current_wlif.first_req;
    while l<>nil do begin
      if ev then l.eidx := m else l.ridx := m;
      a := l.firstarg;
      while(a<>nil) do begin
        inc(m);
        a := a.next;
      end;
      if ev then inc(ecnt) else inc(rcnt);
      l := l.next;
    end;
    current_wlif := current_wlif.next;
  end;
  n := 0;
  if ev then write(f,'argtypes') else write(f,'reqtypes');
  writeln(f,' : array[0..',m-1,'] of Pwl_interface = (');
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    if ev then current_wlif.firstevidx := x else current_wlif.firstreqidx := x;
    if ev then l := current_wlif.first_ev else l := current_wlif.first_req;
    while l<>nil do begin
      write(f,'  ');
      n0 := n;
      a := l.firstarg;
      while(a<>nil) do begin
        if a.aif<>'' then write(f,'@',a.aif,'_ifacedat') else write(f,'nil');
        dec(m);
        inc(n);
        if (m>0) then write(f,', ');
        a := a.next;
      end;
      writeln(f,'// ',n0,':',current_wlif.name,'.',l.name);
      inc(x);
      l := l.next;
    end;
    current_wlif := current_wlif.next;
  end;
  writeln(f,');');
  writeln(f);
end;

function makeargsig(a : Twlarg) : UnicodeString;
var s : UnicodeString; var nt : boolean;
begin
  s := '';
  nt := false;
  if a.typ='int' then begin
    s += 'i';
  end else if a.typ='uint' then begin
    s += 'u';
  end else if a.typ='fixed' then begin
    s += 'f';
  end else if a.typ='string' then begin
    s += 's';
    nt := true;
  end else if a.typ='array' then begin
    s += 'a';
    nt := true;
  end else if a.typ='fd' then begin
    s += 'h';
  end else if a.typ='new_id' then begin
    if a.aif='' then s += 'su'; // weird libwayland-hack
    s += 'n';
    nt := true;
  end else if a.typ='object' then begin
    s += 'o';
    nt := true;
  end else begin
    writeln('unknown type:',a.typ,' in an arg named ',a.name);
    halt(0);
  end;
  if nt and (a.allownull='true') then s := '?'+s;
  makeargsig := s;
end;

function makesig(l : Twlarglist) : UnicodeString;
var s : UnicodeString; a : Twlarg;
begin
  s := '';
  a := l.firstarg;
  while a<>nil do begin
    s += makeargsig(a);
    a := a.next;
  end;
  makesig := s;
end;

procedure write_ifs_allmsg(var f : TEXT; ev : boolean);
var l : Twlarglist; a : Twlarg; m : longint;
begin
  if ev then m := ecnt else m := rcnt;
  if ev then write(f,'events') else write(f,'requests');
  writeln(f,' : array[0..',m-1,'] of Twl_message = (');
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    if ev then l := current_wlif.first_ev else l := current_wlif.first_req;
    while l<>nil do begin
      write(f,'  (name:''',l.name,'''; signature:''',makesig(l),'''; types:');
      if l.firstarg<>nil then begin   
        if ev then write(f,'@argtypes[',l.eidx,'])') else write(f,'@reqtypes[',l.ridx,'])');
      end else write(f,'nil)');
      dec(m);
      if (m>0) then write(f,',');
      writeln(f,'// ',current_wlif.name);
      l := l.next;
    end;
    current_wlif := current_wlif.next;
  end;
  writeln(f,');');
  writeln(f);
end;

procedure write_ifs_init(var f : TEXT);
begin
  writeln(f,'procedure initifs();');
  writeln(f,'begin');
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    writeln(f,'  ',current_wlif.name,'_interface := @',current_wlif.name,'_ifacedat;');
    current_wlif := current_wlif.next;
  end;
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    if current_wlif.first_req=nil then writeln(f,'  ',current_wlif.name,'_ifacedat.methods := nil;')
    else writeln(f,'  ',current_wlif.name,'_ifacedat.methods := @requests[',current_wlif.firstreqidx,'];');
    current_wlif := current_wlif.next;
  end;
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    if current_wlif.first_ev=nil then writeln(f,'  ',current_wlif.name,'_ifacedat.events := nil;')
    else writeln(f,'  ',current_wlif.name,'_ifacedat.events := @events[',current_wlif.firstevidx,'];');
    current_wlif := current_wlif.next;
  end;
  writeln(f,'end;');
  writeln(f);
end;


procedure make_ifgen();
var f : TEXT;
begin
  writeln('writing generated interfaces unit to: wl_ifs.pas');
  assign(f,'wl_ifs.pas');
  rewrite(f);
  write_ifs_head(f);
  write_ifs_im(f);
  write_ifs_alltypes(f,false);
  write_ifs_alltypes(f,true);
  write_ifs_allmsg(f,false);
  write_ifs_allmsg(f,true);
  write_ifs_init(f);
  close(f);
end;






procedure make_allinone(s : ansistring);
var f : TEXT;
begin
  writeln('writing generated allinone unit to: ',s,'.pas');
  assign(f,s+'.pas');
  rewrite(f);
  writeln(f,'{$mode objfpc}');
  writeln(f,'unit ',s,'; //auto-generated');
  writeln(f);
  writeln(f);
  writeln(f,'{$PACKRECORDS C}');
  writeln(f);
  writeln(f);
  writeln(f,'interface');
  write(f,'uses wl_basic_types,wl_libwl');
  if (s<>'wl_core') then writeln(f,',wl_core;') else writeln(f,';');
  writeln(f);
  writeln(f);
  write_ifimporter_iftyps(f);
  writeln(f);
  write_ifs_head(f);

  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    current_wlif.print_req(f,false,false);
    current_wlif := current_wlif.next;
  end;
  writeln(f);
  writeln(f,'type');

  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    current_wlif.print_ev(f);
    current_wlif := current_wlif.next;
  end;
  writeln(f);
  writeln(f);

  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    current_wlif.print_evset(f,false);
    current_wlif := current_wlif.next;
  end;
  writeln(f);
  writeln(f);
  writeln(f,'const');
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    current_wlif.print_enums(f);
    current_wlif := current_wlif.next;
  end;

  writeln(f);
  write_ifs_im(f);

  writeln(f);
  writeln(f,'implementation');
  writeln(f);
  writeln(f);
  writeln(f);
  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    current_wlif.print_evset(f,true);
    current_wlif := current_wlif.next;
  end;
  writeln(f);
  writeln(f);

  current_wlif := first_wlif;
  while current_wlif<>nil do begin
    current_wlif.print_req(f,true,true);
    current_wlif := current_wlif.next;
  end;
  writeln(f);
  writeln(f,'var');
//  write_ifs_im(f);
  write_ifs_alltypes(f,false);
  write_ifs_alltypes(f,true);
  write_ifs_allmsg(f,false);
  write_ifs_allmsg(f,true);
  write_ifs_init(f);
  writeln(f);
  writeln(f,'begin');
  writeln(f,'  initifs();');
  writeln(f,'end.');
  writeln(f);
  close(f);
end;







begin
  if paramcount<2 then begin
    writeln('usage: scanner protocolXMLfilename generatedUnitName');
  end;
  current_wlif := nil;
  first_wlif := nil;
  translate_protocolfile(paramstr(1));

  make_allinone(paramstr(2));
end.
